@extends('layouts.pages')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection
<style>
    img.explore-img {
        border-radius: 30px;
    }
</style>
@section('content')
            <div class="header-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="row vertical-center">
                            <div class="col-xs-12 col-md-6 transparent-mask entry-heading">
                                <h1 aria-live="assertive" class="inner-heading">Support any cause,<br> no matter how small</h1>
                                <p aria-live="assertive">Find causes you’ll love to donate to and make the world a better place in your own little way.</p>
                                <div class="btn-wrapper">
                                    <a href="{{ route('start_campaign')}}" class="btn btn-primary btn-md btn-light" aria-label="create a cause" role="button">create a cause</a>
                                    <a href="{{ route('campaigns') }}" class="btn btn-primary btn-md btn-orange" aria-label="explore campaigns" role="button">view all campaigns</a>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <img src="{{ asset('assets/img/heart-bg.svg') }}" class="header-heart" alt="little boy in a heart shape" role="img">
                                <div class="explore-entry-wrapper col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <a href="#">
                                        <img src="{{ asset('assets/img/explore-image-1.svg') }}">
                                        </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#">
                                        <img src="{{ asset('assets/img/explore-image-2.svg') }}">
                                    </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#">
                                        <img src="{{ asset('assets/img/explore-image-3.svg') }}">
                                    </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#">
                                        <img src="{{ asset('assets/img/explore-image-4.svg') }}">
                                    </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#">
                                        <img src="{{ asset('assets/img/explore-image-5.svg') }}">
                                    </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#">
                                        <img src="{{ asset('assets/img/explore-image-6.svg') }}">
                                    </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#">
                                        <img src="{{ asset('assets/img/explore-image-7.svg') }}">
                                    </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#">
                                        <img src="{{ asset('assets/img/explore-image-8.svg') }}">
                                    </a>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="#">
                                        <img src="{{ asset('assets/img/explore-image-9.svg') }}">
                                    </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section 1 -->
        <section class="category-bg-img">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <h3 class="text-center section-heading-text" aria-live="assertive">Featured Campaign Categories</h3>
                    </div>
                
                    
                    @foreach($categories->chunk(4) as $chunk)
                        <div class="row pt-4">
                            @foreach($chunk as $cat)
                            <div class="col-xs-4 col-md-3">
                                <a href="{{route('single_category', [$cat->id, $cat->category_slug])}}" role="link">
                                    <img style="width: 100%;" src="{{ $cat->get_image_url() }}" alt="humanitarian campaign" role="img">
                                </a>
                                <h5 class="category-title" aria-live="assertive">{{ $cat->category_name }}</h5>
                            </div>
                            @endforeach
                        <div>
                    @endforeach
                </div>
            </div>
        </section>
        <!-- section 2 -->
        <section class="campaign-bg-img transparent-mask">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <h3 class="text-center section-heading-text" aria-live="assertive">Popular Campaigns This Week</h3>
                    </div>
                </div>
                <div class="row pt-5">
                @foreach($popular as $fc)
                    <div class="col-xs-12 col-md-4">
                        <div class="card p-4">
                            <div class="like-container">
                                <div class="like-cnt unchecked" id="like-cnt">
                                    <svg height="62" viewBox="0 0 54 52" width="64" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <filter id="a" height="116.9%" width="124.7%" x="-12.3%" y="-6.2%">
                                            <feMorphology in="SourceAlpha" operator="dilate" radius="3" result="shadowSpreadOuter1"></feMorphology>
                                            <feOffset dy="12" in="shadowSpreadOuter1" result="shadowOffsetOuter1"></feOffset>
                                            <feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="10"></feGaussianBlur>
                                            <feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.05 0"></feColorMatrix>
                                            <feMerge>
                                                <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
                                                <feMergeNode in="SourceGraphic"></feMergeNode>
                                            </feMerge>
                                        </filter>
                                        <path d="m25.8488318 14.7377048c-1.6528355-1.6496691-4.3332467-1.6503936-5.9868084-.001449-.0007263.0003623-.0010894.0010868-.0014525.001449l-.8158878.8139672-.8158878-.8139672c-1.6539249-1.6496691-4.3346992-1.6496691-5.988624 0-1.6535617 1.6496691-1.6535617 4.3244949 0 5.974164l.8162509.8139672 5.9882609 5.974164 5.9882609-5.974164.8158878-.8139672c1.6535618-1.6489446 1.654288-4.3230459.0014524-5.972715z"
                                            fill="#fff" fill-rule="evenodd" filter="url(#a)" stroke="#125cd5" transform="translate(-2.5 -4.5)"></path>
                                    </svg>
                                </div>
                            </div>
                            @php
                                $percent_raised = $fc->percent_raised();
                            @endphp
                            <img class="card-img-top" src="{{ $fc->feature_img_url()}}" alt="campaign image" role="img">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: {{$percent_raised}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <di class="card-body" style="padding-bottom: 0rem;">
                                <div class="progress-title-wrapper">
                                    <h5 class="progress-title" aria-live="assertive">raised
                                        <span class="primary-color"> {{get_amount($fc->success_payments->sum('amount'))}}</span> / ${{ $fc->goal}}
                                    </h5>
                                    
                                    <h5 class="progress-title" aria-live="assertive">
                                        <span class="primary-color">{{$percent_raised}}%</span> funded
                                    </h5>
                                </div>
                            </di>
                            <div class="card-body">
                                <h4 class="card-title" aria-live="assertive"><a href="{{route('campaign_single', [$fc->id, $fc->slug])}}">{{$fc->title}}</a></h4>

                                <p class="card-text" aria-live="assertive">{{ str_limit($fc->short_description, $limit = 70, $end = '...') }}</p>
                            </div>
                            <div class="card-detail-wrapper">
                                <h5 aria-live="assertive" style="font-weight: 300;">{{$fc->success_payments->count()}} Donors</h5>


                                <h5 aria-live="assertive" style="font-weight: 300;">{{$fc->days_left()}} Days to go</h5>
                            </div>
                            <div class="card-btn-wrapper">
                                <a class="btn btn-secondary btn-xs btn-light" href="{{route('campaign_single', [$fc->id, $fc->slug])}}" role="button" aria-label="view campaign">View Campaign</a>
                                <a class="btn btn-primary btn-xs btn-orange" href="#" role="button" aria-label="support this cause">Support</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="section-btn-wrapper">
                    <a href="{{ route('campaigns') }}" class="btn btn-primary btn-md btn-orange" role="button" aria-label="view all campaigns">View all Campaigns</a>
                </div>
            </div>
        </section>
        <!-- section 3 -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <h3 class="text-center section-heading-text" aria-live="assertive">Recent Campaigns</h3>
                    </div>
                </div>
                <div class="row pt-5">
                @foreach($funded_campaigns as $fc)
                    <div class="col-xs-12 col-md-4">
                        <div class="card p-4">
                            <div class="like-container">
                                <div class="like-cnt unchecked" id="like-cnt">
                                    <svg height="62" viewBox="0 0 54 52" width="64" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <filter id="a" height="116.9%" width="124.7%" x="-12.3%" y="-6.2%">
                                            <feMorphology in="SourceAlpha" operator="dilate" radius="3" result="shadowSpreadOuter1"></feMorphology>
                                            <feOffset dy="12" in="shadowSpreadOuter1" result="shadowOffsetOuter1"></feOffset>
                                            <feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="10"></feGaussianBlur>
                                            <feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.05 0"></feColorMatrix>
                                            <feMerge>
                                                <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
                                                <feMergeNode in="SourceGraphic"></feMergeNode>
                                            </feMerge>
                                        </filter>
                                        <path d="m25.8488318 14.7377048c-1.6528355-1.6496691-4.3332467-1.6503936-5.9868084-.001449-.0007263.0003623-.0010894.0010868-.0014525.001449l-.8158878.8139672-.8158878-.8139672c-1.6539249-1.6496691-4.3346992-1.6496691-5.988624 0-1.6535617 1.6496691-1.6535617 4.3244949 0 5.974164l.8162509.8139672 5.9882609 5.974164 5.9882609-5.974164.8158878-.8139672c1.6535618-1.6489446 1.654288-4.3230459.0014524-5.972715z"
                                            fill="#fff" fill-rule="evenodd" filter="url(#a)" stroke="#125cd5" transform="translate(-2.5 -4.5)"></path>
                                    </svg>
                                </div>
                            </div>
                            @php
                                $percent_raised = $fc->percent_raised();
                            @endphp
                            <img class="card-img-top" src="{{ $fc->feature_img_url()}}" alt="campaign image" role="img">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: {{$percent_raised}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <di class="card-body" style="padding-bottom: 0rem;">
                                <div class="progress-title-wrapper">
                                    <h5 class="progress-title" aria-live="assertive">raised
                                        <span class="primary-color"> {{get_amount($fc->success_payments->sum('amount'))}}</span> / ${{ $fc->goal}}
                                    </h5>
                                    
                                    <h5 class="progress-title" aria-live="assertive">
                                        <span class="primary-color">{{$percent_raised}}%</span> funded
                                    </h5>
                                </div>
                            </di>
                            <div class="card-body">
                                <h4 class="card-title" aria-live="assertive"><a href="{{route('campaign_single', [$fc->id, $fc->slug])}}">{{$fc->title}}</a></h4>

                                <p class="card-text" aria-live="assertive">{{ str_limit($fc->short_description, $limit = 70, $end = '...') }}</p>
                            </div>
                            <div class="card-detail-wrapper">
                                <h5 aria-live="assertive" style="font-weight: 300;">{{$fc->success_payments->count()}} Donors</h5>


                                <h5 aria-live="assertive" style="font-weight: 300;">{{$fc->days_left()}} Days to go</h5>
                            </div>
                            <div class="card-btn-wrapper">
                                <a class="btn btn-secondary btn-xs btn-light" href="{{route('campaign_single', [$fc->id, $fc->slug])}}" role="button" aria-label="view campaign">View Campaign</a>
                                <a class="btn btn-primary btn-xs btn-orange" href="#" role="button" aria-label="support this cause">Support</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    
                </div>
                <div class="section-btn-wrapper">
                    <a href="{{ route('campaigns') }}" class="btn btn-primary btn-md btn-orange" role="button" aria-label="view all campaigns">View all Campaigns</a>
                </div>
            </div>
        </section>
        <!-- CTA section -->
        <section class="cta-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-4 mx-auto">
                        <h2 class="cta-text text-center" aria-live="assertive">Get Rewarded for Helping Others</h2>
                        <div class="btn-wrapper">
                            <a href="#" class="btn btn-primary btn-md btn-white" role="button" aria-label="create a cause">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

@endsection
