@extends('layouts.pages')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')
	<div>
					<section class="section_bg">
						<div class="container">
							<h1 class="text-white">Frequently Asked Questions</h1>
						</div>
					</section>
        	<div class="container">
                
                <div class="row mt-5">
                        <div class="col-md-4">
                        	<div class="accordion" id="accordionExample">
							  <div class="faq_accordion">
							    <div class="accordion__title" id="headingOne">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							          What is causevest donation platform?
							        </p>
							      </h5>
							    </div>

							    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
							      <div class="card-body">
							        Causevest donation platform lets users support good causes around the world. 
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="headingTwo">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							          What is a campaign?

							        </p>
							      </h5>
							    </div>
							    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
							      <div class="card-body">
							        A campaign is a fund raising activity set up by operators or causes to solicit support and donations for the causes they love 
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="headingThree">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							          How is a cause different from a campaign?
							        </p>
							      </h5>
							    </div>
							    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
							      <div class="card-body">
							        A cause is the end of a donation. A campaign is the activity set up by operators to solicit donations for a cause
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading4">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
							          Who can register a cause?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordionExample">
							      <div class="card-body">
							        The end beneficiary of a donation can register a cause in their name.
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading5">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="true" aria-controls="collapse5">
							          Who can create a campaign?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordionExample">
							      <div class="card-body">
							        Anybody can create a campaign for a cause. Including, the cause itself, and operators.
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading6">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="true" aria-controls="collapse6">
							          How can I track my donations?
							        </p>
							      </h5>
							    </div>

							    <div id="collapseOne" class="collapse" aria-labelledby="heading6" data-parent="#accordionExample">
							      <div class="card-body">
							        You can track your donations via your dash board. When you have donated to a cause, you will receive notifications as they execute and provide audits on their progress bar.
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading7">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="true" aria-controls="collapse7">
							          Who is an operator?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordionExample">
							      <div class="card-body">
							        An operator is anyone or institution who helps to support and maintain the platform. Internal operator maintains the integrity of the platform by carrying out due diligence on causes, campaigns and operators. A supporter operator (Individual or corporate) supports and solicit support for causes they love.
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading8">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="true" aria-controls="collapse8">
							         Who is a corporate operator?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#faq2">
							      <div class="card-body">
							        A corporator is a private business. A corporate donor who has set up a campaign to raise funds for a cause they love. They may do this by enticing their customers to donate to their chosen cause, through their campaigns for perks or rewards. 
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading9">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse9" aria-expanded="true" aria-controls="collapse9">
							          How can I get rewarded for helping others?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="faq2">
							      <div class="card-body">
							        You can get rewarded for helping others when you have found and helped register a cause or set up campaign for a cause worthy of donations. As the cause operator, you receive a proportion of the causes donations. 
							      </div>
							    </div>
							  </div>
							</div>
                        </div>
                        <div class="col-md-4">
                        	<div class="accordion" id="faq2">
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading10">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse10" aria-expanded="true" aria-controls="collapse10">
							          What is causevest donation?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordionExample">
							      <div class="card-body">
							        Causevest donation is an optional donation a donor makes when he choses to send donations to causevest foundation. Causevest foundation invests from this pull and redistributes profit to causes . However, the donor can vote on set which cause receives his donation as he wills
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading11">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse11" aria-expanded="true" aria-controls="collapse11">
							          What is donation in perpetuity?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#faq2">
							      <div class="card-body">
							        Donation in perpetuity is when an established cause (mostly charities) choses to receive donations continuosly, non-stop.
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading12">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse12" aria-expanded="true" aria-controls="collapse12">
							         What exactly is a cause?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#faq2">
							      <div class="card-body">
							        A cause is anything that you believe should be supported. It could be a charity, your favourite author, a petition, band, a volunteer or even yourself. The network coin holders, as a collective, decide which cases receive capital, but anyone can set up and create a cause that is eligible for donations.
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading13">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse13" aria-expanded="true" aria-controls="collapse13">
							          What problem does Causevest Network solve?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#faq2">
							      <div class="card-body">
							        The Network solves a wide range of problems but 3 core issues are as follows:
									1. We incentivize altruism – In a world focused on maximising short term profit, there is no reward for doing good. We stand out because our platform incentivises users for doing good
									2. Causes that go unsupported get spotted and funded. There are many charities out there and that attract a lot of funding, but we also support smaller, often unseen causes. We specialize in finding causes that are sometimes overlooked.
									3. Our technology provides clear audit trails – ensuring that funds don’t get siphoned off, while also reducing the burden on charities and causes to upload data

							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading14">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse14" aria-expanded="true" aria-controls="collapse14">
							          What is disruptive altruism?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#faq2">
							      <div class="card-body">
							        The idea that we as people can create new systems that quite literally are designed to incorporate altruistic activity in everyone’s lives, whether they realize it or not and whether they like it or not.
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading15">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse15" aria-expanded="true" aria-controls="collapse15">
							          How can I make money using causevest?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse15" class="collapse" aria-labelledby="heading15" data-parent="#faq2">
							      <div class="card-body">
							        Instead of rewarding miners, we reward operators who help support and maintain the network. We also reward users who hold full nodes. Ultimately we incentive actions that support the Causevest network and encourage altruistic activity automatically via the protocol and semi-automatically via our platform.
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading16">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse16" aria-expanded="true" aria-controls="collapse16">
							          How will donations work?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse16" class="collapse" aria-labelledby="heading16" data-parent="#faq2">
							      <div class="card-body">
							        Any cause listed on our network can raise money and receive donations. They can offer products in return for services or accept smart donations that have additional covenants attached to them.
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading17">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse17" aria-expanded="true" aria-controls="collapse14">
							         Who is behind this project?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse17" class="collapse" aria-labelledby="heading17" data-parent="#faq2">
							      <div class="card-body">
							        We have a passionate team of developers, operators and crypto enthusiasts. You can see them <a href = "https://causevest.io/#team">here</a>
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading18">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse18" aria-expanded="true" aria-controls="collapse14">
							          What is XCV Vote?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse18" class="collapse" aria-labelledby="heading18" data-parent="#faq2">
							      <div class="card-body">
							        It refers to an XCV holder voting for a cause they would like to get the causevest top up donations. Voting units of the XCV holder is counted against their XCV holding 
							      </div>
							    </div>
							  </div>
                        	</div>
                        </div>
                        <div class="col-md-4">
                        	<div class="accordion" id="faq3">
                        		<div class="faq_accordion">
							    <div class="accordion__title" id="heading19">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse19" aria-expanded="true" aria-controls="collapse19">
							          What is unique about XCV Votes?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse19" class="collapse" aria-labelledby="heading19" data-parent="#faq3">
							      <div class="card-body">
							        XCV Votes have greater influence on the distribution of budgeted funds for causevest top up donations
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading20">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse20" aria-expanded="true" aria-controls="collapse20">
							          How may I buy XCV
							        </p>
							      </h5>
							    </div>

							    <div id="collapse20" class="collapse" aria-labelledby="heading20" data-parent="#faq3">
							      <div class="card-body">
							        kIndly follow this link to buy XCV <a href="www.causevest.io">www.causevest.io</a>
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading21">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse21" aria-expanded="true" aria-controls="collapse21">
							          What are Covenants?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse21" class="collapse" aria-labelledby="heading21" data-parent="#faq3">
							      <div class="card-body">
							        Covenants are the conditions and standards are a donor stipulates a cause must fulfil before his donation is unlocked to the cause
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading22">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse22" aria-expanded="true" aria-controls="collapse22">
							          What kind of Covenant can I or cant I input?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse22" class="collapse" aria-labelledby="heading22" data-parent="#faq3">
							      <div class="card-body">
							        Covenants are completely at your prerogative as a donor
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading23">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse23" aria-expanded="true" aria-controls="collapse23">
							         What is causevest Participatory budgeting?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse23" class="collapse" aria-labelledby="heading23" data-parent="#faq3">
							      <div class="card-body">
							        Participatory budgeting, is decentralised budgeting where the community rather than the leadership only decides what should receive how much funds. Budgeting based on the wisdom of the crowd
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading24">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse24" aria-expanded="true" aria-controls="collapse24">
							          What is Causevest Top up Donation?
							        </p>
							      </h5>
							    </div>

							    <div id="collapse24" class="collapse" aria-labelledby="heading24" data-parent="#faq3">
							      <div class="card-body">
							        Causevest top up donation is the additional funds causevest distributes to causes as the donor community directs via their monthly votes
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading25">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse25" aria-expanded="true" aria-controls="collapse25">
							          What funds is budgeted in causevest participatory budgeting
							        </p>
							      </h5>
							    </div>

							    <div id="collapse25" class="collapse" aria-labelledby="heading25" data-parent="#faq3">
							      <div class="card-body">
							        Funds from causevest foundation investment profits and network profits
							      </div>
							    </div>
							  </div>
							  <div class="faq_accordion">
							    <div class="accordion__title" id="heading26">
							      <h5 class="mb-0">
							        <p class="collapsed" data-toggle="collapse" data-target="#collapse26" aria-expanded="true" aria-controls="collapse26">
							          Collapsible Group Item #1
							        </p>
							      </h5>
							    </div>

							    <div id="collapse26" class="collapse" aria-labelledby="heading26" data-parent="#faq3">
							      <div class="card-body">
							        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
							      </div>
							    </div>
							  </div>
                        	</div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

@endsection