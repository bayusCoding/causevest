<table style="width: 555px; margin: 0 auto;box-shadow: 0 10px 30px 3px rgba(85,85,85,0.08);">
    <thead>
        <tr>
            <th style="padding-top: 43px; padding-bottom: 30px"><img src="{{ asset('assets/img/logo-dark.svg') }}" alt="Causevest" style="margin: 0 auto; height: auto; width: 110px; display: block"></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="padding-left: 55px; padding-right: 55px; padding-bottom: 20px">
                <h2 style="text-align:center; color: #444444; font-family: Helvetica; line-height: 44px; margin: 0">Welcome to Causevest</h2>
            </td>
        </tr>
        <tr>
            <td style="text-align:center; padding-left: 55px; padding-right: 55px; padding-bottom: 20px">
                <p style="color: #444444; font-family: Helvetica; font-weight: 300;font-size: 15px; line-height: 23px; margin: 0">
                    You have been added as an {{$user_type}} on Causevest <br> 
                    Kindly follow this <a class="btn btn-light btn-md btn-block" style="color:FF5A00; text-decoration:none" href="{{url('/')}}/password/reset/{{$token}}">link</a> to reset your password
                </p>
            </td>
        </tr>
    </tbody>
</table>