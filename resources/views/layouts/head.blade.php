<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- fonts-->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Mono:300,400,500,700" rel="stylesheet">
    <!-- Stylesheet-->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="preload" href="{{ asset('assets/css/style.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">

    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/summernote/summernote.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/toastr/toastr.min.css') }}">

        <link rel="stylesheet" href="{{ asset('assets/folder/font-awesome-4.4.0/css/font-awesome.min.css') }}">


    <noscript>
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    </noscript>
    <script>
    ! function(t) {
        "use strict";
        t.loadCSS || (t.loadCSS = function() {});
        var e = loadCSS.relpreload = {};
        if (e.support = function() {
                var e;
                try {
                    e = t.document.createElement("link").relList.supports("preload")
                } catch (a) {
                    e = !1
                }
                return function() {
                    return e
                }
            }(), e.bindMediaToggle = function(t) {
                function e() {
                    t.media = a
                }
                var a = t.media || "all";
                t.addEventListener ? t.addEventListener("load", e) : t.attachEvent && t.attachEvent("onload", e),
                    setTimeout(function() {
                        t.rel = "stylesheet", t.media = "only x"
                    }), setTimeout(e, 3e3)
            }, e.poly = function() {
                if (!e.support())
                    for (var a = t.document.getElementsByTagName("link"), n = 0; n < a.length; n++) {
                        var o = a[n];
                        "preload" !== o.rel || "style" !== o.getAttribute("as") || o.getAttribute("data-loadcss") ||
                            (o.setAttribute("data-loadcss", !0), e.bindMediaToggle(o))
                    }
            }, !e.support()) {
            e.poly();
            var a = t.setInterval(e.poly, 500);
            t.addEventListener ? t.addEventListener("load", function() {
                e.poly(), t.clearInterval(a)
            }) : t.attachEvent && t.attachEvent("onload", function() {
                e.poly(), t.clearInterval(a)
            })
        }
        "undefined" != typeof exports ? exports.loadCSS = loadCSS : t.loadCSS = loadCSS
    }("undefined" != typeof global ? global : this);
    </script>
    <link rel="stylesheet" href="{{ asset('assets/css/media.css') }}">

    <!-- scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script src="{{ asset('assets/js/jquery-session.js') }}"></script>
    <script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- <script src="{{asset('assets/plugins/SocialShare/SocialShare.min.js')}}"></script> -->

    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script> -->    
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <title>Causevest | Get rewarded by boosting your donations.</title>

    <!-- Search Engine -->
    <meta name="description" content="Get rewarded when people make donations and boost those donations further with network capital by voting with your XCV.">
    <meta name="image" content="https://www.causevest.io/site-image.png">

    <!-- Schema.org for Google -->
    <meta itemprop="name" content="Causevest | Get rewarded by boosting your donations.">
    <meta itemprop="description" content="Get rewarded when people make donations and boost those donations further with network capital by voting with your XCV.">
    <meta itemprop="image" content="https://www.causevest.io/site-image.png">

    <!-- Open Graph general (Facebook, Pinterest & Google+) -->
    <meta name="og:title" content="Causevest | Get rewarded by boosting your donations.">
    <meta name="og:description" content="Get rewarded when people make donations and boost those donations further with network capital by voting with your XCV.">
    <meta name="og:image" content="https://www.causevest.io/opengraph.png">
    <meta name="og:url" content="https://www.causevest.io">
    <meta name="og:site_name" content="Causevest">
    <meta name="og:type" content="website">
    
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/img/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/img/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#125cd5">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="theme-color" content="#000000">
</head>

<body id="body" class=<?php if( !empty($_COOKIE['currentClass']) ) { echo "{$_COOKIE['currentClass']}"; } else { echo "light"; } ?> >
    
    <div class="header-bg-img transparent-mask" role="img"></div>
    <header role="banner" id="header-scroll" class="header">
        <div class="container">
            <nav class="navbar navbar-light bg-transparent navbar-fixed-top" role="navigation">
                <!-- site logo -->
                <a class="navbar-brand" href="{{ url('/') }}" role="link">
                    <img src="{{ asset('assets/img/causevest-logo.svg') }}" alt="causevest logo" class="logo-light-scroll">
                    <img src="{{ asset('assets/img/causevest-logo.svg') }}" alt="causevest logo" class="logo-dark-scroll">
                </a>
                <!-- background toggle -->
                <a class="nav-link ml-auto background-toggle" role="button">
                    <svg class="toggle transparent-mask moon" role="button" aria-live="assertive" aria-label="dark or light toggle" onclick="toggleDarkLight()" xmlns="http://www.w3.org/2000/svg" width="14" height="15">
                        <g fill="#FFF">
                            <path d="M13.902334 11.52873c-1.335674 2.083506-3.669727 3.46547-6.326924 3.471211-4.116592.008936-7.496719-3.320918-7.546494-7.437216C-.0179 3.68959 2.859668.47874 6.592207.00079c.079775-.010225.131748.081035.083262.145195-.89997 1.19124-1.433526 2.674395-1.433526 4.28253 0 3.927627 3.183868 7.111465 7.111465 7.111465.498164 0 .98414-.051094 1.45333-.148682.07878-.016377.139014.069697.095596.137431z" />
                            <path d="M13.806914 11.390586c.078633-.016347.138867.069522.095537.137168-1.338457 2.088047-3.679424 3.47165-6.343594 3.47165-4.15913 0-7.53038-3.371571-7.53038-7.53038 0-1.664297.539765-3.202324 1.45415-4.449024-.207598.681006-.321123 1.40297-.325664 2.150948-.0254 4.19039 3.340107 7.577197 7.530586 7.577021 1.518486-.000058 2.931914-.449472 4.114336-1.222851.342304-.021065.677783-.066446 1.00503-.134532z" />
                        </g>
                    </svg>
                    <svg class="toggle sun" role="button" aria-live="assertive" aria-label="dark or light toggle" onclick="toggleDarkLight()" width="15px" height="15px" viewBox="0 0 15 15" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id="Web-Platform" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Landing-Page-Dark-Mode" transform="translate(-772.000000, -21.000000)" fill="#FFFFFF" fill-rule="nonzero">
                                <g id="Group-19">
                                    <g id="sun" transform="translate(772.000000, 21.000000)">
                                        <circle id="Oval" cx="7.5" cy="7.5" r="3.62068359"></circle>
                                        <path d="M7.5,0 C7.92849609,0 8.27586914,0.347373047 8.27586914,0.775869141 L8.27586914,2.06897461 C8.27586914,2.4974707 7.92849609,2.84484375 7.5,2.84484375 C7.07150391,2.84484375 6.72413086,2.4974707 6.72413086,2.06897461 L6.72413086,0.775869141 C6.72413086,0.347373047 7.07150391,0 7.5,0 Z" id="Shape"></path>
                                        <path d="M7.5,12.1551855 C7.92849609,12.1551855 8.27586914,12.5025586 8.27586914,12.9310547 L8.27586914,14.2241602 C8.27586914,14.652627 7.92849609,15 7.5,15 C7.07150391,15 6.72413086,14.652627 6.72413086,14.2241309 L6.72413086,12.9310254 C6.72413086,12.5025293 7.07150391,12.1551855 7.5,12.1551855 Z" id="Shape"></path>
                                        <path d="M2.03777344,2.03765625 C2.34076172,1.73466797 2.83201172,1.73466797 3.135,2.03765625 L4.04935547,2.95201172 C4.35234375,3.255 4.35234375,3.74625 4.04935547,4.04923828 C3.74636719,4.35222656 3.25511719,4.35222656 2.95212891,4.04923828 L2.03777344,3.13488281 C1.73478516,2.83189453 1.73478516,2.34064453 2.03777344,2.03765625 Z" id="Shape"></path>
                                        <path d="M0,7.5 C0,7.07150391 0.347373047,6.72413086 0.775869141,6.72413086 L2.06897461,6.72413086 C2.4974707,6.72413086 2.84484375,7.07150391 2.84484375,7.5 C2.84484375,7.92849609 2.4974707,8.27586914 2.06897461,8.27586914 L0.775869141,8.27586914 C0.347373047,8.27586914 0,7.92849609 0,7.5 Z" id="Shape"></path>
                                        <path d="M2.03759766,12.9612305 C1.73460938,12.6582422 1.73460938,12.1669922 2.03759766,11.8640039 L2.95195313,10.9496484 C3.25494141,10.6466602 3.74619141,10.6466602 4.04917969,10.9496484 C4.35216797,11.2526367 4.35216797,11.7438867 4.04917969,12.046875 L3.13482422,12.9612305 C2.83183594,13.2642187 2.34058594,13.2642187 2.03759766,12.9612305 Z" id="Shape"></path>
                                        <path d="M12.9616406,12.9614648 C12.6586523,13.2644531 12.1674023,13.2644531 11.8644141,12.9614648 L10.9500586,12.0471094 C10.6470703,11.7441211 10.6470703,11.2528711 10.9500586,10.9498828 C11.2530469,10.6468945 11.7442969,10.6468945 12.0472852,10.9498828 L12.9616406,11.8642383 C13.2646289,12.1672266 13.2646289,12.6584766 12.9616406,12.9614648 Z" id="Shape"></path>
                                        <path d="M15,7.5 C15,7.92849609 14.652627,8.27586914 14.2241309,8.27586914 L12.9310254,8.27586914 C12.5025293,8.27586914 12.1551562,7.92849609 12.1551562,7.5 C12.1551562,7.07150391 12.5025293,6.72413086 12.9310254,6.72413086 L14.2241309,6.72413086 C14.652627,6.72413086 15,7.07150391 15,7.5 Z" id="Shape"></path>
                                        <path d="M12.9619336,2.03841797 C13.2649219,2.34140625 13.2649219,2.83265625 12.9619336,3.13564453 L12.0475781,4.05 C11.7445898,4.35298828 11.2533398,4.35298828 10.9503516,4.05 C10.6473633,3.74701172 10.6473633,3.25576172 10.9503516,2.95277344 L11.864707,2.03841797 C12.1676953,1.73542969 12.6589453,1.73542969 12.9619336,2.03841797 Z" id="Shape"></path>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </svg>
                </a>
                <div class="mobile ml-auto">
                    <div class="hamburger-menu">
                        <div class="bar"></div>
                    </div>
                </div>
                <ul class="nav justify-content-end" role="menubar">
                    <li class="nav-item" role="menuitem">
                        <a class="nav-link" href="{{route('browse_categories')}}" aria-live="assertive" aria-label="explore campaigns">Explore Campaigns</a>
                    </li>
                    <li class="nav-item" role="menuitem">
                        <a class="nav-link" href="{{route('start_campaign')}}" aria-live="assertive" aria-label="create a cause">Start a Cause</a>
                    </li>
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li class="nav-item" role="menuitem">
                            <a class="nav-link" href="{{ route('login') }}" role="button" aria-live="assertive" aria-label="log in">Log in</a>
                        </li>
                        <li class="nav-item" role="menuitem">
                            <a class="nav-link btn btn-primary btn-xs btn-orange" href="{{ route('register') }}" role="button" aria-live="assertive" aria-label="sign up">Sign Up</a>
                        </li>
                    @else
                        <li class="dropdown">
                            <a href="#" style="text-decoration: none" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <i class="fa fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li class="nav-link" style="line-height:1.3em">
                                    <a class="dashboard" href="{{route('dashboard')}}"> 
                                        <!-- <i class="fa fa-dashboard dashboard"></i>  -->
                                        @lang('app.dashboard') <br /><br />
                                    </a>

                                    <a href="{{route('change_password')}}" class="dashboard"> @lang('app.change_password')<br /><br /> </a>

                                    <a href="{{route('profile')}}" class="dashboard">Profile <br /><br /> </a>

                                    <a class="logout" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        <i style="display:none" class="nav-link fa fa-sign-out"></i> 
                                        @lang('app.logout')
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </nav>
            <div class="mobile-nav hide">
                <ul>
                    <li>
                        <a href="{{route('browse_categories')}}" aria-live="assertive">explore campaigns</a>
                    </li>
                    <li>
                        <a href="{{route('start_campaign')}}" aria-live="assertive">create a cause</a>
                    </li>
                    @if (Auth::guest())
                        <li>
                            <a href="{{ route('login') }}" aria-live="assertive">sign in</a>
                        </li>
                        <li>
                            <a href="{{ route('register') }}" aria-live="assertive">sign up</a>
                        </li>

                    @else
                        <li>
                            <a class="dashboard" href="{{route('dashboard')}}"> 
                                <!-- <i class="fa fa-dashboard dashboard"></i>  -->
                                @lang('app.dashboard') <br /><br />
                            </a>
                        </li>
                        <li>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                {{ csrf_field() }}
                            </form>

                            <a class="logout" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i style="display:none" class="nav-link fa fa-sign-out"></i> 
                                @lang('app.logout')
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </header>
    <!-- header content -->
    <!-- orange mask -->
    <main role="main">