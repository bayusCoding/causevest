</main>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-md-3">
                    <a href="index.html" role="link"><img src="{{ asset('assets/img/logo.svg') }}" class="footer-logo" alt="footer logo"></a>
                    <!-- Social -->
                    <ul class="social">
                        <li><a href="#" role="link"><img src="{{ asset('assets/img/twitter-icon.svg') }}" alt="twitter icon"></a></li>
                        <li>
                            <a href="#" role="link">
                                        <img src="{{ asset('assets/img/Instagram-icon.svg') }}" alt="instagram icon">
                                    </a>
                        </li>
                        <li>
                            <a href="#" role="link">
                                        <img src="{{ asset('assets/img/facebook-icon.svg') }}" alt="facebook icon">
                                    </a>
                        </li>
                    </ul> 
                </div>
                <div class="col-xs-6 col-md-3">
                    <h5 class="primary-color" aria-live="assertive">learn more</h5>
                    <a href="#" role="link" aria-label="about us">about us</a>
                </div>
                <div class="col-xs-6 col-md-3">
                    <h5 class="primary-color" aria-live="assertive">support</h5>
                    <a href="{{ url('/faq') }}" style="text-transform: uppercase" role="link" aria-label="frequently asked questions">faq</a>
                    <a href="{{ url('/privacy_policy') }}" role="link" aria-label="privacy policy">privacy policy</a>
                    <a href="{{ url('/terms_of_use') }}" role="link" aria-label="terms of use">terms of use</a>
                    <a href="{{ url('/transparency') }}" role="link" aria-label="terms of use">transparency</a>
                </div>
                <div class="col-xs-6 col-md-3">
                    <h5 class="primary-color" aria-live="assertive">community</h5>
                    <a href="#" role="link" aria-label="join us on telegram">telegram</a>
                    <a href="#" role="link" aria-label="join us on discord">discord</a>
                    <a href="#" role="link" aria-label="join us on forum">forum</a>
                </div>
                <div class="col-md-12">
                    <div class="copyright">
                        <p class="text-center small" aria-live="assertive">&copy; 2018 Causevest</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    
    <!-- scripts -->
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/mojs/latest/mo.min.js"></script> -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

    <script type="text/javascript" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/DrawSVGPlugin.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.0/TweenMax.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/summernote/summernote.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/toastr/toastr.min.js') }}"></script>

    <script>
        $(function () {
            $('#start_date, #end_date').datetimepicker({format: 'YYYY-MM-DD'});
        });

        $(document).ready(function() {
            $('.description').summernote({  height: 300});

            $('button.note-btn.btn.btn-default.btn-sm.btn-fullscreen').click(() => {
                $('#header-scroll').toggle();
                console.log('clicked');
            });

            // window.onscroll = function () {
            //     var header = document.querySelector("#header-scroll");
            //     var nav = document.querySelector("#headerMenuCollapse");

            //     if (window.pageYOffset >= 1) {
            //         header.classList.add("is-sticky");
            //         nav.classList.add("is-sticky");
            //     } else {
            //         header.classList.remove("is-sticky");
            //         nav.classList.remove("is-sticky");
            //     }
            // };
        });

    </script>
    
    <script>
        function toggleDarkLight() {
            var body = document.getElementById("body");
            var currentClass = body.className;
            body.className = currentClass == "dark" ? "light" : "dark";
        }
        var check_status = false;
        var like_cnt = $("#like-cnt");
        var like_parent = $(".like-container");

        // var burst = new mojs.Burst({
        //     parent: like_parent,
        //     radius: {
        //         20: 60
        //     },
        //     count: 15,
        //     angle: {
        //         0: 30
        //     },
        //     children: {
        //         delay: 250,
        //         duration: 700,
        //         radius: {
        //             10: 0
        //         },
        //         fill: ["#125cd5"],
        //         easing: mojs.easing.bezier(0.08, 0.69, 0.39, 0.97)
        //     }
        // });

        // $("#like-cnt").click(function () {
        //     var t1 = new TimelineLite();
        //     var t2 = new TimelineLite();
        //     if (!check_status) {
        //         t1.set(like_cnt, {
        //             scale: 0
        //         });
        //         t1.set(".like-btn", {
        //             scale: 0
        //         });
        //         t1.to(like_cnt, 0.6, {
        //             scale: 1,
        //             background: "#125cd5",
        //             ease: Expo.easeOut
        //         });
        //         t2.to(
        //             ".like-btn",
        //             0.65, {
        //                 scale: 1,
        //                 ease: Elastic.easeOut.config(1, 0.3)
        //             },
        //             "+=0.2"
        //         );
        //         //    t1.timeScale(5);
        //         check_status = true;
        //         //circleShape.replay();
        //         burst.replay();
        //     } else {
        //         t1
        //             .to(like_cnt, 1, {
        //                 scale: 1
        //             })
        //             .to(like_cnt, 1, {
        //                 scale: 1,
        //                 background: "rgba(255,255,255,0.3)",
        //                 ease: Power4.easeOut
        //             });
        //         t1.timeScale(7);
        //         check_status = false;
        //     }
        // });

        $('.btn[aria-label="explore campaigns"]').click(function () {
            $('html, body').animate({
                scrollTop: $("section.category-bg-img").offset().top
            }, 1000)
        });

        function getCookie(name) {
            var value = "; " + document.cookie;
            var parts = value.split("; " + name + "=");
            if (parts.length == 2) return parts.pop().split(";").shift();
        }
       
       $('.toggle').click(() => {
            document.cookie = "currentClass="+$('body').attr('class')+"; path=/"
            console.log(getCookie('currentClass'));
        });
    </script>
</body>

</html>
