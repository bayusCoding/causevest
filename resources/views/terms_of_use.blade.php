@extends('layouts.pages')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')

	<section class="section_bg">
		<div class="container">
			<h1>Terms of use</h1>
		</div>
	</section>
	<section class="body_text">
		<div class="container">
			<div class="t_inner_text">
				<h4>Welcome to causevest platform!</h4>
				<p>These are causevest terms of use (stick with us here). We know that most people skim through terms of use statements because they're boring and go right over our heads, but we have done everything we can to make this easy to get through. At the beginning of every section, you'll see the biggest takeaway of the following portion. By using causevest, you agree to these terms and we will always tell you if we change them. These summaries are not legally binding, but they give you a short version of the terms. Summaries appear highlighted at the start of every section.
				</p>
				<p>The Causevest platform sits on top of the causevest protocol and operates as a social enterprise based donation platform where good causes can be registered, supported and donated to by the causevest foundation and donors all around the world. These are the platforms terms of use and apply to all users of the causevest website and mobile applications. “We,” “our” or “us” refers to BIT4ME LIMITED. “ CAUSEVEST PLATFORM” refers to this website and the services offered by us.
				</p>
				<p>
				By using CAUSEVEST PLATFORM you agree to these terms. You acknowledge that you have read and understood these terms. You also agree to our privacy policy, community guidelines and any other policies we post on CAUSEVEST PLATFORM.
				CAUSEVEST PLATFORM is a platform. We are not responsible for the quality, timing or legality of content or rewards.
				</p>
				<p>
				We may sometimes make changes to these terms. If we make material changes that adversely affect your rights under these terms, then we will let you know by posting an announcement on the site or sending you an email prior to the changes coming into effect. Continuing to use CAUSEVST PLATFORM after a change to these terms means you accept the new terms.
				</p>
			</div>
			<div class="t_inner_text">
				<h4>Account Creation</h4>
				<p>You must be at least 13 years old to register for an account, and to pay or accept money there are other requirements. You are responsible for your account.
				When you register for an account you must provide us with accurate information. If you don’t provide us with accurate information then we may not be able to assist you with accessing your account.
				</p>
				<p>To register for an account you must be at least 13 years old. To give or receive pledges you must be at least 18 years old or have your parent’s permission. If you know a user is under the age of 13, please <a href="#">report</a> them to us.
				You are responsible for anything that occurs when anyone is signed in to your account, as well as the security of the account. If you believe your account is compromised, you must <a href="#">contact us</a> immediately.
				</p>
			</div>
			<div class="t_inner_text">
				<h4>User Conduct</h4>
				<p>Don’t do bad stuff. Be responsible and use common sense.You are responsible for all the activity on your account. If you do bad things we may terminate your account. For starters:</p>
				<ul>
					<li> Illegal Activities - Don’t break the law or encourage others to break the law.</li>
					<li>Abuse - Don’t harass or bully others, or promote violence or hatred towards others.</li>
					<li>Personal Information - Don’t distribute others’ personal information or otherwise abuse it. CAUSES/OPERATORS with access to their DONORS personal information should not use it for anything unrelated to CAUSEVEST.</li>
					<li> Fraud - Don’t post information that is false or otherwise misleading.</li>
					<li>Impersonation - Don’t impersonate anyone. Don’t use another’s account, or allow others to use your account.</li>
					<li>Username Squatting - Don’t create an account to prevent others from using the name or to sell the name.</li>
					<li>Intellectual Property - Don’t infringe on others’ intellectual property rights.</li>
					<li>Spam - Don’t spam others or distribute unsolicited advertising material.</li>
					<li>Malware -  Don’t use CAUSEVEST PLATFORM to host or distribute, malicious or destructive software</li>
					<li>Endorsement - Don’t claim endorsement by CAUSEVEST PLATFORM without our prior written approval.</li>
					<li>Service Degradation - Don’t degrade others’ use of CAUSEVESTPLATFORM.</li>
					<li>Data Mining - Don’t crawl, scrape or otherwise index information on CAUSEVEST PLATFORM. If you are doing this to create a useful feature then we may allow it, but you must check with us first.</li>
					<li>Reward Sharing - Don’t support a CAUSE/CAMPAIGN and then share their DONOR-only content without permission from the OPERATOR/CAUSE.</li>
					<li>Reward Sharing - Don’t support a CAUSE/CAMPAIGN and then share their DONOR-only content without permission from the OPERATOR/CAUSE.</li>
				</ul>
				<p>Please also see our community guidelines for specific guidance in some of these areas. These guidelines are not meant to be exhaustive. If you find a new and creative way to hurt CAUSEVEST PLATFORM or our users we may take action to prevent it.</p>
			</div>
			<div class="t_inner_text">
				<h4>All About Being a DONOR</h4>
				<p>DONORS pledge money to CAUSES for doing good. Depending on the operators /causes/campaigns preference, you will be charged either a set amount one-off, periodically or in perpetutity. CAUSEVEST PLATFORM is not responsible for the quality, timing or legality of content or rewards.</p>
				<p>To become a DONOR simply create an account, update your preferred payment method and pledge to any CAUSE!
				There are THREE types of DONATIONS. We have the direct, periodic and smart donations.
				If you donate to a cause periodically, you are charged on behalf of the cause based on the specified frequency of donations.
				Smart donations involve creating a smart contract that only releases funds to the cause if certain criteria are met. There are three types of smart donations; Strict smart donations, flexible smart donations and vested smart donations.
				Strict smart donations have a list of criteria, created by the donor, to be met before the donation goes through. Flexible smart donations are an open donation type with softer guidelines. Flexible donations are monitored by the community, with user collaboration ensuring that causes behave according to the donor’s original intentions. A Vested smart donation is a special donation that is designed to pay out in perpetuity or until a cause’s objectives are completely satisfied. This is done by taking the initial donation from the donor and gifting it to the Causevest foundation. These funds are then invested by the foundation to generate a steady stream of cash flow, with the profits paid out to the donor’s selected cause.</p>
				<p>Should you prefer to choose the smart donation
				For all kinds of donations, you may cancel your pledge at any time.
				If you are located in the EU, then VAT is added to the total charge. This is shown when you set up the initial donation amount.
				We may grant you recovery of refunds at our sole discretion in exceptional circumstances.
				We have no control over the quality, timing or legality of content or audits. We do not grant refunds for failure to deliver rewards. We do attempt to screen for fraudulent CAUSE pages, but cannot guarantee the identity of CAUSES/OPERATORS or the validity of any claims they make. We appreciate your help <a href="#">reporting suspicious creator pages</a> so we can improve CAUSEVEST PLATFORM.
				</p>
			</div>
			<div class="t_inner_text">
				<h4>All About Being a CAUSE/OPERATOR</h4>
				<p>CAUSES/OPERATORS launch a CAMPAIGN that allows DONORS to support them.  You are responsible for any taxes you owe based on the donations you receive. You must also follow our rules about content and audits.</p>
				<p>To become a cause/operator, you may convert your regular account to a cause/operator account or directly make a cause/operator account, and launch the page/campaign.
				You may choose to withdraw donations at the start of the month, or per campaign period, in either case most payments are processed at the start of each month.  In connection with your donations, we are your limited agent for the sole purpose of receiving, holding, and settling payments to you. As your agent, our receipt of funds from a donor on your behalf is the same as receipt of funds by you directly, and you will only have recourse against us, and not against any donor, for any failure by us to settle funds to you. We try to provide timely access to your funds, but you may occasionally experience delays in accessing your funds. We may also block or hold payments for violations of the terms or compliance reasons, including collecting tax reporting information. When payments are delayed or blocked we try to communicate the reasons to you promptly. In order to protect causes, we may block patrons’ payments if we believe them to be fraudulent.
				</p>
				<p>Our fee to process transactions on your behalf is 5% of processed donations. Separate payment processing fees vary based on the donation amount, but are typically under 5% of the total amount we process for you process across all donors.
				We do not handle most tax payments, but we collect tax identification information and report this to tax authorities as legally required. You are responsible for reporting any taxes.
				</p>
				<p>The one area of taxes where we handle payments on your behalf is VAT payments for electronically supplied services to donors in the EU. For the purpose of electronically supplied services, creators make a supply of that content to us, and then we supply it to the donor. To learn more about how we handle VAT, please check our VAT guide.
				We restrict some types of contents, You cannot:
				<ul>
					<li>Create any content or rewards with real people engaging in sexual or violent acts.</li>
					<li>Create any content or reward with nudity, or realistic depictions of sexual or violent acts, unless you flag the page as Adult Content.</li>
					<li>Create content or rewards using others' intellectual property, unless you have written permission to use it, or your use is protected by fair use.</li>
				</ul>
				</p>
				<p>Please also see our community guidelines for specific guidance in some of these areas.
					If your potential donors are under the age of 18, please remind them that they need permission to donate. Anyone under the age of 13 cannot have an account.
				</p>
			</div>
			<div class="t_inner_text">
				<h4>CAUSEVEST PLATFORM ROLE</h4>
				<p>While CAUSEVEST supports CAUSES REGISTRATION, we do not legally endorse any CAUSE on our site. If a cause  is reported we may remove it, but we do not take that action lightly.</p>
				<p>We only provide a platform for CAUSES, OPERATORS AND DONORS  to interact. We do not screen or endorse any CAUSE on CAUSEVEST PLATFORM. If you see content that violates these terms, then let us know and we may remove it.
				Removing content or terminating accounts is not an action we take lightly and we may take a while to investigate or open the issue up to further discussion with the community.
				We are constantly testing out new features with the goal of making CAUSEVEST PLATFORM better. We may add or remove features, and often test features with a random subset of our users. If we believe a feature is significantly different from these terms, then we explain those differences in the test.
				With your permission, we may give other websites or services the ability to verify information about your CAUSEVEST PLATFORM account or perform actions on your behalf. This permission is asked for when you connect your CAUSEVEST PLATFORM account to these other websites or services. Information can include the existence of your account, DONATION amounts and length of support. Actions can include creating, editing or deleting pledges and any other interaction with CAUSES such as posting messages or liking posts.
				</p>
			</div>
			<div class="t_inner_text">
				<h4>Account Disabling</h4>
				<p>You can disable your account by request. We can disable it at our discretion.
				You can permanently disable your account at any time by sending an email to disable@causevest.com. You can see what information is deleted and what we continue to store after the account is disabled in our privacy policy.
				We can terminate or suspend your account at any time at our discretion. We can also cancel any donations and remove any content or rewards at our discretion.
				These terms remain in effect after your account is disabled.
				</p>
			</div>
			<div class="t_inner_text">
				<h4>Your Content</h4>
				<p>You keep complete ownership of all content, but give us permission to use it on CAUSEVEST PLATFORM. Make sure you have permission to use content that you post on CAUSEVEST PLATFORM.
				You keep full ownership of all content that you post on CAUSEVEST PLATFORM, but to operate we need licenses from you.
				By posting content to CAUSEVEST PLATFORM you grant us a royalty-free, perpetual, irrevocable, non-exclusive, sublicensable, worldwide license to use, reproduce, distribute, perform, publicly display or prepare derivative works of your content. The purpose of this license is to allow us to operate CAUSEVEST PLATFORM, promote CAUSEVEST PLATFORM and promote your content on CAUSEVEST PLATFORM. We are not trying to steal your content or use it in an exploitative way.
				You may not post content that infringes on others' intellectual property or proprietary rights.
				CAUSEVEST PLATFORM may not use content posted by creators in any way not authorized by the USER.
				</p>
			</div>
			<div class="t_inner_text">
				<h4>CAUSEVEST PLATFORM Content</h4>
				<p>You can use our content as part of CAUSEVEST, but can’t use it for anything else without our permission.
				Content we create is protected by copyright, trademark and trade secret laws. Some examples of our content are the text on the site, our logo, and our codebase. We grant you a license to use our logo and other copyrights or trademarks to promote your CAUSEVEST PLATFORM page & CAMPAIGN.
				You may not otherwise use, reproduce, distribute, perform, publicly display or prepare derivative works of our content unless we give you permission in writing. Please ask if you have any questions.
				</p>
			</div>
			<div class="t_inner_text">
				<h4>Copyright Infringement</h4>
				<p>If someone on CAUSEVEST PLATFORM is using your copyright without permission, then please send an email to copyright@causevest.com. This is complicated, so make sure to read the long version before you send us an email.
				If you believe that any content on causevest platform  infringes your copyrights, please send written notice to:
				Copyright Agent 
				Bit4me, ltd. 
				230 9th Street 
				San Francisco, CA 94103 
				copyright@causevest.com
				This notice should include the following information:
				</p>
				<ul>
					<li>The electronic or physical signature of the copyright owner, or a person authorized to act on their behalf.</li>
					<li> A description of the copyrighted work that you claim has been infringed.</li>
					<li>A description of the exact location on causevest platform of the content that you claim is infringing. This description must allow us to find and identify the content.</li>
					<li>Your name, address, telephone number and email address.</li>
					<li>A statement by you that: a) you believe in good faith that the use of the content that you claim to infringe your copyright is not authorized by law, the copyright owner, or the owner’s agent, b) all information contained in your copyright notice is accurate, and c) under penalty of perjury, you are either the copyright owner, or authorized to act on their behalf.
					If your content has been removed because of a DMCA notice, but you believe the content was not infringing on another’s copyrights, then you may send a written counter-notice to have the content restored. Your counter-notice should include the following information:
					</li>
					<li>Your electronic or physical signature.</li>
					<li>A description of the content that was removed and the exact location of the content on causevest before it was removed.</li>
					<li>Your name, address, telephone number and email address.</li>
					<li>A statement under penalty of perjury that you believe in good faith that the content was removed by mistake or misidentification.</li>
					<li>A statement that you consent to the jurisdiction of the U.S. Federal District Court for the judicial district in which you are located, or if you are outside the U.S., the Northern District of California, and that you will accept service of process from the party that originally sent us the DMCA notice.</li>
					<p>In appropriate circumstances we may also terminate the accounts of repeat infringers.
					For more information about DMCA notices please see our DMCA guide.</p>
				</ul>
			</div>
			<div class="t_inner_text">
				<h4>Indemnity</h4>
				<p>If we are sued because of you, you have to help pay for it.
				You will indemnify us from all losses and liabilities, including legal fees, that arise from these terms or relate to your use of causevest platform. We reserve the right to exclusive control over the defense of a claim covered by this clause. If we use this right then you will help us in our defense.
				</p>
				<p>Your obligation to indemnify under this clause also applies to our affiliates, officers, directors, employees, agents and third party service providers.
				</p>
			</div>
			<div class="t_inner_text">
				<h4>Warranty Disclaimer</h4>
				<p>We do our best to make sure causevest platform works as expected, but stuff happens.
					Causevest platform is provided “as is” and without warranty of any kind. Any warranty of merchantability, fitness for a particular purpose, non-infringement, and any other warranty is excluded to the greatest extent permitted by law.
					The disclaimers of warranty under this clause also apply to our affiliates and third party service providers.
					</p>
			</div>
			<div class="t_inner_text">
				<h4>Limit of Liability</h4>
				<p>If you lose money as a result of using causevest platform , any payment to you is limited to how much you have paid us, and we don’t have to pay you if your loss is unexpected.</p>
				<p>To the extent permitted by law, we are not liable to you for any incidental, consequential or punitive damages arising out of these terms, or your use or attempted use of causevest platform. To the extent permitted by law, our liability for damages is limited to the amount of money we have earned through your use of Patreon. We are specifically not liable for loss associated with failure to deliver rewards and from losses caused by conflicting contractual agreements.</p>
				<p>For this clause “we” and “our” is defined to include our affiliates, officers, directors, employees, agents and third party service providers.</p>
			</div>
			<div class="t_inner_text">
				<h4>Dispute Resolution</h4>
				<p>If you have a problem please talk to us, but you are limited in how you can resolve disputes. You waive your right to trial by jury and your right to participate in a class action proceeding.
				We encourage you to contact us if you have an issue. If a dispute does arise out of these terms or related to your use of causevest platform, and it cannot be resolved after you talk with us, then it must be resolved by arbitration. This arbitration must be administered by JAMS under theJAMS Streamlined Arbitration Rules & Procedures. Judgment on the arbitration award may be entered in any court with jurisdiction. Arbitrations may only take place on an individual basis. No class arbitrations or other grouping of parties is allowed. By agreeing to these terms you are waiving your right to trial by jury or to participate in a class action or representative proceeding, we are also waiving these rights.
				We follow the JAMS Policy on Consumer Arbitrations Pursuant to Pre-Dispute Clauses Minimum Standards of Procedural Fairness for all arbitrations done under these terms. If any portion of these terms do not follow that standard, that portion is severed from these terms.
				This clause does not limit either party’s ability to seek injunctive or other equitable relief for disputes relating to intellectual property or proprietary data.
				</p>
			</div>
			<div class="t_inner_text">
				<h4>Governing Law</h4>
				<p>Any disputes with us must be resolved in San Francisco under California law.
				California law, excluding its conflict of law provisions, governs these terms and all other causevest platform policies. If a lawsuit does arise, both parties consent to the exclusive jurisdiction and venue of the courts located in San Francisco, California.</p>
			</div>
			<div class="t_inner_text">
				<h4>Everything Else</h4>
				<p>These terms are the final word on causevest platforms policies.
				These terms and any referenced policies are the entire agreement between you and us, and supersede all prior agreements. If any provision of these terms is held to be unenforceable, that provision is modified to the extent necessary to enforce it. If a provision cannot be modified, it is severed from these terms, and all other provisions remain in force. If either party fails to enforce a right provided by these terms, it does not waive the ability to enforce any rights in the future.
				If you have any questions, please email legal@causevest.com.</p>
			</div>

		</div>
	</section>
@endsection