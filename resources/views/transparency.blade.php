@extends('layouts.pages')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')

	<section class="section_bg">
		<div class="container">
			<h1>Transparency</h1>
		</div>
	</section>
	<section class="body_text">
		<div class="container">
			<div class="t_inner_text">
				<h4> How do Donors know if a project will follow through?</h4>
				<p>Launching a campaign is a very public act, and causes or operators put their reputations at risk when they do. Donors should look for campaigns with a clear plan for how the project will be executed and completed, and who have a history of bringing their projects and other projects to fruition. Causes /operators are encouraged to share links and as much background information as possible so donors can make informed decisions about the projects they support. If a cause doesn't share key information, donors should take that into consideration. Does the cause include links to any websites that show work related to the project, or past projects? Does the operator appear in the video? Have they connected via Facebook? Don't hesitate to request more information from an operator if necessary. You can always reach out before donating via the "Contact me" button on the operators's profile.</p>
			</div>
			<div class="t_inner_text">
				<h4>What is a cause obligated to do once their project is funded?</h4>
				<p>When a project is successfully funded, the cause is responsible for completing the project and fulfilling each reward (if promised). Their fundamental obligation to donors is to finish all the work that was promised, providing audit reports. Once a cause has done so, they’ve fulfilled their obligation to their donors. 
 				At the same time, backers must understand that causevest is not a store. When you support a project, you’re either giving a cause a life or helping an existing one sustain life. There’s a chance something could happen that prevents the cause from being able to finish the project as promised. If a cause is absolutely unable to complete the project and fulfill rewards, they must make every reasonable effort to find another way of bringing the project to a satisfying conclusion for their donors. In any case, proper audit is provided. Otherwise, donation is recycled. For more information, see <a hreh="#">Section 4 of our Terms of Use</a>.</p>
			</div>
			<div class="t_inner_text">
				<h4> Who is responsible for completing a project as promised?</h4>
				<p>It's the causes’ responsibility to complete their project. Causevest is not involved in the development of the projects themselves.
				On causevest, donors (you!) ultimately decide the validity and worthiness of a project by whether they decide to fund it.
				However, to provide extra layer of security causevest decentralized operators conduct due diligence and police the platform to further ensure that only worthy causes get funded (see operators guide). When a cause or its operator is discovered to be fraudulent, donations are locked and recycled (see our terms of use)
				</p>
			</div>
			<div class="t_inner_text">
				<h4>How do I know a cause is who they claim they are?</h4>
				<p>It is possible you are friends with the person or team, or you learnt about the project from a trusted source.
				Looking for more information? Read through the description and watch the video to get a better sense of the person or team behind the project.
				In the person or team bio section of each cause, you can find additional resources, like links to websites, relevant background information, and in some cases, connected social media accounts. For persons or teams who have built integrity with a history of experience and successful completion of past projects, you'll also find a verified name with a check mark next to it. This person is behind the project or a part of the company or team behind it, and they verified their identity through an automated process. 
				If you have more questions, you can send the cause a message to ask them.
				</p>
			</div>
			<div class="t_inner_text">
				<h4>What should causes do if they are having problems completing their projects?</h4>
				<p> If problems come up, the team is expected to post a project update explaining the situation. Sharing the story, speed bumps and all, is crucial. Most donors support projects because they want to see something happen and they'd like to be a part of it. Causes who are honest and transparent will usually find donors and the entire causevest community to be understanding. 
				It's not uncommon for things to take longer than expected. Sometimes the execution of the project proves more difficult than the cause had anticipated. If a cause is making a good faith effort to complete their project and is transparent about it, backers should do their best to be patient and understanding while demanding continued accountability from the cause. 
				If the problems are severe enough that the cause can't fulfill their project, causes need to find a resolution. Steps should include[d1]  , detailing exactly how funds were used, and other actions to satisfy the causevest community. For more information, see <a href="#">Section 4 of our Terms of Use</a>.
				</p>
			</div>
			<div class="t_inner_text">
				<h4>Can causevest refund the money if a project is unable to fulfill?</h4>
				<p>causevest does not issue refunds. At best we freeze the fund balance on the cause’s account and recycle to other donations.  </p>
			</div>
			<div class="t_inner_text">
				<h4>Why can’t causevest guarantee projects?</h4>
				<p>We started causevest to encourage disruptive altruism and participatory budgeting. It’s a new way for donors and causes to work together to create positive impact. Many traditional funding systems are risk-averse and profit-focused, and tons of great ideas never get a chance. Causevest gives a chance to many great ideas, accommodating more causes (charitable and non charitable) that may never have had an opportunity to make an impact due to their low profile or lack of history. In the causevest platform anyone can help decide which great idea or cause will become tomorrow's reality. In addition, we plough back a significant proportion of our network profits to registered causes popular with community members.</p>
				<p>Causevest is full of passionate, ambitious, innovative, and imaginative ideas. Many of the projects on causevest are in earlier stages of development and are looking for a community to bring them to life. We are happy to allow causes take risks and a good shot at creating positive impact.
				</p>
			</div>
			<div class="t_inner_text">
				<h4>What does causevest do to protect its community?</h4>
				<p>We have highly incentivized network of operators that police the system for suspicious activity. Operators earn rewards by conducting due diligence on causes and teams. Our internal operators screen reports that are sent to us by our community and they take action if they find something that does not align with our rules.
				It is important to note that in causevest, causes unlock additional funding as they successfully complete more stages of their projects and providing satisfactory audits on success. This reduces the risk of funding fraudulent causes and also gives our internal operators time to look into any concerns raised by donors and the rest of the causevest community.
				</p>
			</div>
			<p>For more information about how causevest works, visit our <a href="#">Trust & Safety page.</a></p>
		</div>
	</section>
@endsection