<style>
.hide{
    display: none;
}
.comment{
    margin:5px; cursor:pointer
}
</style>
<div class="left-holder font">
    <h3>Updates</h3>
    @if(!$user)
        <h4 style="color:red">Login to write comment</h4>
    @endif
    @if($updates->count() > 0)
        
        @foreach($updates as $update)
        <div class="wrap">
            <div class="col-xs-12 col-md-12 card">
                <div class="card-body">
                    <div class="update-field text-left">
                            <div class="image-profile pull-left d-flex">
                                    <img src="{{ $update->getUserByUpdate($update->user_id)->get_gravatar(100) }}">
                                    <div class="user-details">
                                    <p>{{$update->getUserByUpdate($update->user_id)->name}}</p>
                                    <p class="text-muted"> {{$update->created_at->format('j F Y')}} </p>
                                    </div>
                                </div>
                        {!! safe_output(nl2br($update->description)) !!}
                        <!-- <i style="font-size:30px; padding: 10px" class="fa fa-thumbs-up"></i> -->
                    </div>

                </div>
            </div>

            @php $comments = $update->getComment($update->id) @endphp
            <div class="comment">
                <h4>Comments</h4>
                @foreach($comments as $comment)

                    <div class="card">
                        <div class="card-body"> {{$comment->comment}} </div>
                    </div>
                @endforeach

                <div class="form">
                    @if($user)
                        {{Form::open([ 'route' => 'comment', 'class' => 'form-horizontal'])}}
                        <br>
                        <div class="form-group">
                            <textarea name="comment" class="form-control" cols="10" rows="3"></textarea><br>
                            <input type="hidden" name="campaign_id" value="{{$campaign->id}}">
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            <input type="hidden" name="update_id" value="{{$update->id}}">
                            <input type="submit" class="btn btn-light"  value="comment">
                        </div>
                        {{Form::close()}}
                    @endif
                </div>
            </div>
        </div>
        @endforeach
    @else
        <div class="col-xs-12 col-md-12 card">
            <div class="no-data">
                <i class="fa fa-bell-o"></i><h3>@lang('app.no_update')</h3>
            </div>
        </div> 
    @endif

</div>

<script>
    $('.comment').hide();
    $('.cm').click( (e) => {
        $(e.target).closest('.wrap').find('div:eq(4)').toggle();
    })
</script>