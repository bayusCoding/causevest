@extends('layouts.pages')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')

	<section class="section_bg">
		<div class="container">
			<h1>Privacy Policy</h1>
		</div>
	</section>
	<section class="body_text">
		<div class="container">
			<div class="p_inner_text">
				<h4>Welcome to Causevest!</h4>
				<p>Causevest is an innovative platform specifically designed for socially conscious investors as it operates with the ideology of encouraging investors to donate to good causes of their choice registered on our platform. The coins help the investors to gain profit and at the same time, require them to do good by proceeding the transaction fee to the cause that they believe in. We care about your privacy and want to help you understand how we collect, use and share your personal information. This privacy policy applies to causes , operators and donors, and is part of our <a href='/terms_of_use'>terms of use</a>.</p>
			</div>
			<div class="p_inner_text">
				<h4>Information You Provide</h4>
				<p>This is information that you provide to us through text fields, such as your name, payment information, goals and rewards. The information we collect is different depending on if you are an account holder, become a donor by supporting a cause, or become a cause by creating your own cause page.</p>
			</div>
			<div class="p_inner_text">
				<h4>To create an account on Patreon you have to provide your:</h4>
				<ul style="list-style-type: decimal;">
					<li>Email address</li>
					<li>Username</li>
					<li>Password</li>
				</ul>
				<p>You may also sign up using a Facebook account and email address. We will ask permission to access basic information from that account, such as your name, profile picture, and friend list. You can choose to stop sharing that information with us at any time by removing causevest access to that account.
				You also have the option to add more information to your profile, such as a location, social media links and an about section, which we store along with any comments or messages you submit through Patreon.</p>
			</div>
			<div class="p_inner_text">
				<h4>Patrons</h4>
				<p>A donor is someone who financially supports causes through Causevest. To become a donor you have to provide our payment processor (Stripe) with your payment information. You can see the privacy policy for these payment processor on the <a href="https://stripe.com/us/privacy/">Stripe</a> site. They provide us with a token that represents your account, your card’s expiration date, card type and the last four digits of your credit card. If you provide them with a name and email address then they also provide us with that information.
				We collect and process information about the causes you support, the level at which you support them, what rewards you receive and how often you support them.
				As a donor, if you select a reward tier with a physical reward, then you have to provide your shipping information so the creator can ship you the reward.
				</p>
			</div>
			<div class="p_inner_text">
				<h4>Causes</h4>
				<p>A cause is someone or a team who creates a cause page requesting financial support from donors. To become a cause, you must create a page that describes what your cause/project, any rewards you are offering and any financial goals you have. We collect and process this information along with any additional information you add to your profile. To receive payments you have to create an account with one of our payment partners: PayPal, Stripe or Payoneer. You must also provide us with additional information for tax purposes. Depending on your location you have to fill out a form with some combination of your:</p>
				<ul style="list-style-type: decimal;">
					<li>Legal name</li>
					<li>Address</li>
					<li>Social security number or employer identification number</li>
					<li>Country of citizenship</li>
					<li>Foreign tax identification number</li>
					<li>Date of birth</li>
					<li>Your nonprofit registration number, if you are a nonprofit business</li>
				</ul>
			</div>
			<div class="p_inner_text">
				<h4>Information We Collect</h4>
				<p>We collect information automatically as you navigate the site or through our third party analytics providers. We may store usage information such as the type of device you use to access Patreon, your operating system, browser type, IP address, and device ID, the pages you visit or request, links clicked, referring sites, user interactions and your search terms. We also derive your location from your IP address.</p>
			</div>
			<div class="p_inner_text">
				<h4>Do Not Track</h4>
				<p>Our service does not support Do Not Track requests at this time, which means our third party analytics providers may associate your usage on Patreon with your usage on other sites.</p>
			</div>
			<div class="p_inner_text">
				<h4>Cookies</h4>
				<p>We use third-party advertising cookies to present you with opportunities while on Cuasevest. We also use retargeting cookies to present you with Patreon advertising on other websites.</p>
				<ul style="list-style-type: decimal;">
					<li>Sift Science: The <a href='https://siftscience.com/'>Sift Science</a> page and <a href='https://siftscience.com/site-privacy'>privacy policy</a> explains their cookies and provides information on opt-out.</li>
					<li>Visual Website Optimiser (Wingify): You can read the <a href='https://wingify.com/'>Visual Website Optimiser</a> and its <a href='https://vwo.com/privacy-policy/'>privacy policy</a> to learn about its cookies and have the option to <a href='https://vwo.com/opt-out/'>opt-out</a>.</li>
					<li>Facebook Custom Audiences: You can adjust your Facebook advertising settings from within your Facebook account and opt-out of advertisements from within the Facebook application.</li>
					<li>Google Adwords: You can <a href='https://www.google.com/settings/u/0/ads/authenticated?hl=en'>adjust</a> the Google ads settings and <a href='https://support.google.com/ads/answer/2662922?hl=en'>opt-out</a> of this program.</li>
					<li>Twitter Tailored Audiences: You can <a href='https://support.twitter.com/articles/20170405'>opt-out</a> of seeing interest-based advertisingon Twitter.</li>
					<p>We use Google Analytics, Amplitude, and KissInsights analytics cookies to see how you use Patreon so we can improve in a variety of ways. We encourage you to read the <a href='https://www.google.com/policies/privacy/'>Google privacy policy</a>. If you prefer to not have data reported by Google Analytics, you can install the <a href='https://tools.google.com/dlpage/gaoptout'>Google Analytics Opt-out Browser Add-on</a>. We also encourage you to read more about <a href='https://amplitude.com/'>Amplitude</a> and their <a href='https://amplitude.com/privacy'>privacy policy</a>, and about <a href=''>KissInsights</a>, their <a href='https://www.kissmetrics.com/privacy/'>privacy policy</a> and their <a href='https://www.kissmetrics.com/user-privacy/'>opt-out instructions</a>.
					</p>
					<p>We also use third party vendors to provide delivery of rewards which you can read about in our “Information Shared with Third Parties” section below.</p>
				</ul>
			</div>
			<div class="p_inner_text">
				<h4>How We Use Your Information</h4>
				<p>We never sell your information to anyone. We use your information to:</p>
				<ul style="list-style-type: decimal;">
					<li>provide Causevest services to you.</li>
					<li>allow you to sign in to your account.</li>
					<li>allow you to support causes through Causevest.</li>
					<li>allow you to collect money as a Cause.</li>
					<li>send you emails relevant to your usage, as controlled by your email preferences.</li>
					<li>reply to your questions.</li>
					<li>understand how you use the service and market Donors to you, including suggesting Causes you may be interested in supporting.</li>
					<li>create better tools for Causes to serve Donors.</li>
					<li>conduct research and development to improve Causevest and develop future products that our users may be interested in by analyzing how you use it.</li>
					<li>prevent fraud and abuse on Causevest.</li>
				</ul>
			</div>
			<div class="p_inner_text">
				<h4>Information Shared with Creators</h4>
				<p>If you are a Donor, the following information is shared with the Causes you support:</p>
				<ul style="list-style-type: decimal;">
					<li>Your email address and other profile information.</li>
					<li>Any messages you send them through Causevest.</li>
					<li>Your physical address, city, state, country, and phone number, if you have signed up for a reward that requires shipping.</li>
					<li>All information about your Donation, including amount and start date, but not your full payment card information.</li>
					<li>Some aggregated and anonymized data about how you use Causevest that cannot be linked back to any individual user.</li>
					<li>Your stipulated covenants </li>
				</ul>
			</div>
			<div class="p_inner_text">
				<h4>Information Shared with Third Parties</h4>
				<p>Personally Identifiable Information (PII) is data that includes a personal identifier like your name, email or address, or data that could reasonably be linked back to you. We will only share this data under the following circumstances:</p>
				<ul style="list-style-type: decimal;">
					<li>With your permission, with service providers used by Causevest to deliver content and rewards.</li>
					<li>With companies that are contractually engaged in providing us with services, such as order fulfillment, email management, analyzing data trends, credit card processing and fraud detection and prevention. These companies may have access to personal information to perform their services and are obligated by contract to safeguard any PII they receive from us.</li>
					<li>To protect the security or integrity of Causevest, and to protect the rights, property, or safety of Causevest, its employees, users, or others, if we believe that disclosure is reasonably necessary to comply with a law, regulation, valid legal process (e.g., subpoenas or warrants served on us). If we are going to release your data, we will do our best to provide you with notice in advance by email, unless we are prohibited by law from doing so.</li>
					<li>In connection with the sale, merger, bankruptcy, sale of assets or reorganization of our company. We will notify you if a different company receives your PII. The promises in this privacy policy apply to any data transferred to a new entity.</li>
					<li>Our <a href='https://www.patreon.com/apps/featured'>apps directory</a> displays our most updated list of third party apps providers who help us deliver rewards. While we try to keep this list up to date, we often experiment with new third party apps providers, and this list may not be exhaustive.</li>
				</ul>
			</div>
			<div class="p_inner_text">
				<h4>Information Shared with the Public</h4>
				<p>The following information is publicly accessible:</p>
				<ul style="list-style-type: decimal;">
					<li>Your profile, and your social media links and location if you add that information.</li>
					<li>By default the Causes you support are publicly displayed. If you flag your account as private, we will not display the Causes you support.</li>
					<li>Any posts or comments you make on a cause’s page.</li>
					<li>Your aggregated or anonymized usage data in blog posts, press releases, or in other ways to share information about Causevest usage. Aggregated or anonymized data cannot be linked back to any individual Causevest user.</li>
				</ul>
			</div>
			<div class="p_inner_text">
				<h3>Your Preferences and Rights over Data</h3>
				<h4>Choosing Your Preferences</h4>
				<p>The Settings link is located by clicking on your avatar or profile at the top right hand of your screen. Settings lets you see what your account preferences are. You can see and adjust your settings by viewing your preferences and, if you wish, by changing your selections.</p>
			</div>
			<div class="p_inner_text">
				<h3>Exercising Your Data Rights</h3>
				<p>You can exercise rights over your data in the following ways:</p>
				<ul style="list-style-type: decimal;">
					<li><h4>Modifying the Information in Your Account</h4></li>
					<li>You can modify or delete certain information associated with your account on the settings pages, as described in the “Choosing Your Preferences” section above.</li>
					<li><h4>Disabling Your Account</h4>
					<li>You can disable your account if you go to your account settings while you are logged into Causevest. Click the “Disable My Account” link to have your account immediately disabled and removed from public view. Please be aware that this is a final act and you cannot log into your account again after clicking this link. When you disable your account, we immediately remove your ability to log in. If you create a new account, you will be asked to use a different email address from the one you used previously. Even if an account is disabled, some information may be retained by our analytics providers on their own servers.</li>
					<li><h4>Removing Marketing Emails</h4></li>
					<li>You can stop receiving marketing emails by changing your email settings. While this removes your marketing emails, service related emails are still sent, including how you can optimize your Causevest experience and suggestions for which Causes to follow.</li>
					<li><h4>Turning off Mobile Notifications</h4></li>
					<li>If you download the Causevest App you may also receive notifications on your mobile device. These can be disabled in the App settings.</li>
					<li><h4>Understanding Our Data Retention Periods</h4></li>
					<li>We retain your information for ten years, unless you disable your account information from your account settings. Please note that once you disable your account, you will not be able to log in again. If you want to log into Patreon again by creating a new account, you may be prompted to use a new email address from the one you previously used on your disabled account.</li>
					<li><h4>Additional Questions Related to Privacy</h4></li>
					<li>For additional privacy-related questions about your account, you may email us at privacy@causevest.io.</li>
				</ul>
			</div>
			<div class="p_inner_text">
				<h3>European Union Data Transfer</h3>
				<h4>Transfer of Data to the United States</h4>
				<p>We are based in the United States. By using our services you consent to the transfer of your personal data to the United States, and to the processing and use of that personal data in the United States. This processing and use is limited to what is described in this privacy policy and the terms of use</p>
				<h4>Marketing Activities</h4>
				<p>We may use your information to offer you products and services that we think may be of interest to you, unless you have opted out of marketing communications. If you are in the European Economic Area, where required by applicable law, we will obtain your consent before sending you any marketing communications. You may object to the use of your data for direct marketing purposes at any time, free of charge, when we collect your personal information and in every marketing communication. We will never provide your information to third parties for marketing purposes without your prior consent.</p>
				<h4>Security</h4>
				<p>The security of your personal information is important to us and we follow industry standards to protect it. Full credit card numbers are never accessed by our system, and the most sensitive data we store, such as tax forms, are encrypted to keep them secure.
				To further protect your information we suggest using a strong and unique password for your Causevest account. We also offer <a href='https://patreon.zendesk.com/hc/en-us/articles/206538086-How-Do-I-Set-Up-2-Factor-Authorization-'>two-factor authentication</a> as an additional security measure.</p>
				<h4>Children</h4>
				<p>Causevest is not directed at children under the age of 13, and they may not create an account or otherwise use Causevest.</p>
				<h4>Changes</h4>
				<p>We may sometimes make changes to this policy. If we make material changes that adversely affect your rights under this policy, we will let you know by posting an announcement on the site or sending you an email prior to the changes coming into effect. Continuing to use Causevest after a change to this policy means you accept the new policy.
				If you have any questions, please email privacy@causevest.io.</p>
			</div>

		</div>
	</section>
@endsection