@extends('layouts.pages')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@section('content')
<main role="main" class="single-campaign donate">
    <!-- section 1-->
    <section class="campaign-donate">
        <div class="container">
            <div id="dom-target" style="display: none;">
                <?php 
        $output = get_option('convenant_fee'); //Again, do some operation, get the output.
        echo htmlspecialchars($output); /* You have to escape because the result
        will not be valid HTML otherwise. */
        ?>
    </div>
    {{--{{ Form::open(['class' => 'form-horizontal', 'files' => true]) }} --}}
    <div class="row">
        <div class="checkout-wrap">
            <div class="col-xs-12 col-md-8 col-lg-4 mx-auto pt-5">
                <h3 aria-live="assertive" class="text-center">{{$campaign->title}}</h3>
                <h6 aria-live- "assertive" class="card-text-secondary text-center">CAUSE CREATOR</h6>
                <div class="campaign-profile vertical-center">
                    <div class="image-profile">
                        <img src="{{ $campaign->user->get_gravatar(100) }}">
                    </div>
                    <div class="profile-text">
                        <h5>{{$campaign->user->name}}</h5>
                        <div>
                            <a href="#">
                                <span>5</span> Campaigns</a>
                            </div>
                        </div>
                    </div>
                    <div style="padding-top:10px" class="donate-form">
                        <div class="donate-form">
                            @if( ! Auth::check())
                            <p class="guest_checkout_text"><strong> Anonymous Checkout <span class="text-muted">@lang('app.or')</span> <a href="{{route('login')}}">@lang('app.log_in')</a> </strong></p>
                            @endif

                            @if( ! Auth::check())
                            <div style="flex-direction: row; padding:10px" class="form-group card">
                                <input type="radio" name="donate" value="Anonymous" checked="checked">Anonymous
                            </div>
                            @else
                            <div style="flex-direction: row; padding:10px" class="form-group card">
                                <input type="radio" name="donate" value="{{$user->name}}" checked="checked">{{$user->name}}
                            </div>

                            <div style="flex-direction: row; padding:10px" class="form-group card">
                                <input type="radio" name="donate" value="Anonymous">Anonymous
                            </div>
                            @endif

                            @php
                            if(session('cart.cart_type') == 'reward'){
                                $donation_amount = $reward->amount;
                            }else{
                                $donation_amount = session('cart.amount');
                            }
                            @endphp
                            <form action="">
                                <div class="card campaign-donate">
                                    <div class="card-body">
                                        <div class="card-title">
                                            <h5 class="card-tag">Donate without a reward</h5>
                                        </div>
                                        <div class="currency-field">
                                            <select disabled id="currency"class="select-currency">
                                                <option value="Pound">£</option>
                                                <option selected="selected" value="Dollar">$</option>
                                                <option value="Naira">₦</option>
                                            </select>
                                            <input id="amount" min="<?php if(session('cart')['annuity'] == 1) echo $donation_amount;  ?>" <?php if(session('cart.cart_type') == 'reward') echo 'disabled';  ?> type="number" name="currency-val" class="currency-val" value="{{$donation_amount}}">
                                        </div>
                                        
                                        {{-- @if (session('cart.cart_type') == 'reward') --}}
                                            <div style="flex-direction: row; padding:10px" class="form-group card">
                                                <input type="checkbox" id="check" name="check" value="convenantcheckbox">&nbsp;&nbsp;&nbsp;&nbsp;Add Covenant
                                            </div>
                                        {{-- @endif --}}
                                        
                                        
                                            <div style="padding:10px; display: none; " class="form-group card" id="convenanttext">
                                            <p><?php echo get_option('convenant_description');  ?></p>
                                            <textarea placeholder="Enter a challenge: post a photo of yourself holding a baby turtle" rows="6" id="convenant" class="form-control" name="convenant" maxlength="200"></textarea>
                                            {{-- <input type="text" id="convenant" class="form-control" name="convenant" placeholder="Enter a challenge: post a photo of yourself holding a baby turtle" /> --}}
                                        </div>
                                        
                                        <div class="total-donation">
                                            <p>
                                                Total: <span class="currency">$</span><span id="append" class="value"></span> 
                                            </p>
                                            {{-- <p id="convs"></p> --}}
                                        </div>



                                        {{-- <button type="submit" class="btn btn-primary btn-lg btn-light btn-block" aria-label="donate now" role="button">Donate Now</button> --}}
                                        @if(get_option('enable_stripe') == 1)
                                        <div class="stripe-button-container">

                                            <script src="https://checkout.stripe.com/checkout.js"></script>

                                            <button class="btn btn-primary btn-md btn-light" id="customButton">Donate Now</button>

                                            <script>
                                                var amount = parseInt(document.getElementById('amount').value);
                                                var name = $('.form-group.card input:radio:checked').val();
                                                var convenant = $('#convenant').val();

                                                $('.form-group.card input:radio').change(() => {
                                                    name = $('input:radio:checked').val();
                                                    console.log('name: '+name);
                                                });

                                                document.getElementById('append').innerHTML = document.getElementById('amount').value;

                                                document.getElementById('convenant').innerHTML = document.getElementById('convenant').value;

                                                document.getElementById('convenant').addEventListener('keyup', () => {
                                                            // console.log(document.getElementById('convenant').value);
                                                            convenant = document.getElementById('convenant').value;
                                                            // console.log('final convenant output: '+ convenant);
                                                        });

                                                document.getElementById('amount').addEventListener('keyup', () => {
                                                    document.getElementById('append').innerHTML = document.getElementById('amount').value;
                                                    amount = parseInt(document.getElementById('amount').value);
                                                    if( {{ session('cart')['annuity'] }} == 1 ) {
                                                        if( document.getElementById('amount').value < {{get_option('annuity')}} ) {
                                                            document.getElementById('customButton').disabled = true;

                                                            console.log('low low');
                                                        }
                                                        else{
                                                            document.getElementById('customButton').disabled = false;
                                                        }
                                                    }

                                                });

                                                var handler = StripeCheckout.configure({
                                                    key: '{{ get_stripe_key() }}',
                                                    image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                                                    locale: 'auto',
                                                    token: function(token) {

                                                        $.ajax({
                                                            url : '{{route('payment_stripe_receive')}}',
                                                            type: "POST",
                                                            data: {name: name, convenant: convenant, amount: amount, stripeToken : token.id, _token : '{{ csrf_token() }}' },
                                                            success : function (data) {
                                                                if (data.success == 1){
                                                                    $('.checkout-wrap').html(data.response);
                                                                    toastr.success(data.msg, '@lang('app.success')', toastr_options);
                                                                }
                                                            }
                                                        });
                                                    }
                                                });

                                                document.getElementById('customButton').addEventListener('click', function(e) {
                                                        // Open Checkout with further options:
                                                        handler.open({
                                                            name: 'Stripe.com',
                                                            //description: '2 widgets',
                                                            zipCode: false,
                                                            amount: parseInt(document.getElementById('amount').value) * 100,
                                                            name: name
                                                        });
                                                        e.preventDefault();
                                                    });

                                                    // Close Checkout on page navigation:
                                                    window.addEventListener('popstate', function() {
                                                        handler.close();
                                                    });
                                                </script>
                                            </div>
                                            @endif

                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="donate-disclaimer">
                                <p>Causevest is a way to bring the causes you love to life. Causevest does not guarantee campaigns
                                    or investigate a creator's ability to complete their project. It is the responsibility of
                                    the project creator to complete their project as promised, and the claims of this project
                                are theirs alone.</p>

                                <p>Your payment method will not be charged at this time. If the project is successfully funded,
                                    your payment method will be charged $10.00 when the project ends. <a href="#">Learn More.</a></p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                {{--{{ Form::close() }}--}}
            </div>
        </section>
    </main>
    <!-- CK Editor -->
    <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script>
        const toastr_options = {
            'closeButton': true,
            'debug': false,
            'newestOnTop': false,
            'progressBar': false,
            'positionClass': 'toast-top-right',
            'preventDuplicates': false,
            'showDuration': '1000',
            'hideDuration': '1000',
            'timeOut': '5000',
            'extendedTimeOut': '1000',
            'showEasing': 'swing',
            'hideEasing': 'linear',
            'showMethod': 'fadeIn',
            'hideMethod': 'fadeOut',
        }
        // $(function () {
        //     // Replace the <textarea id="editor1"> with a CKEditor
        //     // instance, using default configuration.
        //     CKEDITOR.replace('convenant');
        //     //bootstrap WYSIHTML5 - text editor
        //     $(".textarea").wysihtml5();
        // });
        $(function() {

            @if(get_option('enable_bank_transfer') == 1)

            $('#bankTransferBtn').click(function(){
                $('.bankPaymetWrap').slideToggle();
            });

            $('#bankTransferForm').submit(function(e){
                e.preventDefault();

                var form_input = $(this).serialize()+'&_token={{csrf_token()}}';

                $.ajax({
                    url : '{{route('bank_transfer_submit')}}',
                    type: "POST",
                    data: form_input,
                    success : function (data) {
                        if (data.success == 1){
                            $('.checkout-wrap').html(data.response);
                            toastr.success(data.msg, '@lang('app.success')', toastr_options);
                        }
                    },
                    error   : function ( jqXhr, json, errorThrown ) {
                        var errors = jqXhr.responseJSON;
                        var errorsHtml= '';
                        $.each( errors, function( key, value ) {
                            errorsHtml += '<li>' + value[0] + '</li>';
                        });
                        toastr.error( errorsHtml , "Error " + jqXhr.status +': '+ errorThrown);
                    }
                });

            });
            @endif
        });
        $(function(){
            $('#check').change(function(){
                var div = document.getElementById("dom-target");
                var myData = parseInt(div.textContent);

                var datas = parseInt($('#amount').val());
                if ( datas >= myData) {
                    if ($(this).is(':checked')) {
                        $('#convenanttext').show();
                    }
                    else{
                        $('#convenanttext').hide();
                    }
                }else{
                    toastr.success('You must donate a minimum of $' + myData + ' for this campaign', toastr_options);
                    $("#check").prop("checked", false);
                }
            });
        });
    </script>
    @endsection

    @section('page-js')

    <script>
        $(function(){
            $(document).on('click', '.donate-amount-placeholder ul li', function(e){
                $(this).closest('form').find($('[name="amount"]')).val($(this).data('value'));
            });
        });

    </script>

    @endsection