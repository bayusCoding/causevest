@php
    $backers = $campaign->success_payments()->orderBy('id','desc')->paginate(20);
@endphp
<div class="left-holder font">
    @if($backers->count())
        <table class="table table-bordered table-striped">
            <tr>
                <th>@lang('app.backer_name')</th>
                <th>@lang('app.amount')</th>
            </tr>

            @if($backers->count() > 0)
                @foreach($backers as $backer)

                    <tr>
                        <td>{{ $backer->contributor_name_display =='anonymous' ? trans('app.anonymous') : $backer->name}}</td>
                        <td>{{get_amount($backer->amount)}} <?php if(!empty($backer->top_up)) echo' + '.get_amount($backer->top_up); ?> </td>
                    </tr>

                @endforeach
            @endif
        </table>

        {{ $backers->links() }}
    @else

        <div class="no-data">
            <i class="fa fa-smile-o"></i> <h1>@lang('app.no_contribute')</h1>
        </div>

    @endif    
</div>