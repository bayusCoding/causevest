<div class="left-holder font">
    <h3>Frequently Asked Questions</h3>
    <?php $i = 1; ?>
    @if(!empty($campaign->faqs))
        @foreach($campaign->faqs as $faq)
            <div class="faq-wrapper">
                <div class="faq-wrap">
                    <button class="btn btn-block" type="button" data-toggle="collapse" data-target="#faq{{ $i }}" aria-expanded="false" aria-controls="collapseExample">
                        {{ $faq->title }}
                    </button>
                </div>
                <div class="collapse" id="faq{{ $i++ }}">
                    <div class="card card-body">
                        {!! safe_output(nl2br($faq->description)) !!}
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <h4>No FAQs found for this campaign</h4>
    @endif
</div>
<div class="faq-disclaimer col-md-10 col-lg-8 pt-0 mb-0 pl-0">
        <p class="dislaimer-text text-left pb-2">
            Don't see the answer to your question? Ask the cause creator directly.
        </p>
        <a href="#" class="btn btn-primary btn-lg btn-orange d-block" role="button" aria-label="ask a question">Ask A Question</a>
</div>