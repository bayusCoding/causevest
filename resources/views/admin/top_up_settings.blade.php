@extends('layouts.admin')

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>

    <div class="dashboard-wrap font">
        <div class="container">
            <div style="margin-top: 40px" id="wrapper" class="row">
                
                @include('admin.sidebar')

                <div class="col-md-9">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12">
                                <h2 class="page-header">  Donation  </h2>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')


                    <div class="row">
                        <div class="col-md-6">
                        {{ Form::open(['route'=>'top_up','class' => 'form-horizontal', 'files' => true]) }}

                                <div class="form-group">
                                    <label for="pool">Pool ($)</label>
                                    <input type="number" name="pool" value="<?php if(!empty($meta_data['pool'])) echo $meta_data['pool']; ?>" class="form-control" placeholder="Pool Amount" aria-label="Pool">
                                </div>

                                <div class="form-group">
                                    <label for="pool">Top Up Type</label>
                                    <select name="select" id="select" class="form-control">
                                        <option>Select Top up </option>
                                        <option value="percentage" @if( !empty($meta_data['percent']) ) selected="selected" @endif >Pecentage</option>
                                        <option value="fixed" @if( !empty($meta_data['amount']) ) selected="selected" @endif >Fixed</option>
                                    </select>
                                </div>

                                <div class="form-group fixed">
                                    <label for="pool">Fixed Amount ($)</label>
                                    <input type="number" name="amount" value="<?php if(!empty($meta_data['amount'])) echo $meta_data['amount']; ?>" class="form-control" placeholder="$5" aria-label="Pool">
                                </div>


                                <div class="form-group percentage">
                                    <label for="pool">Percentage (%)</label>
                                    <input type="number" name="percent" value="<?php if(!empty($meta_data['percent'])) echo $meta_data['percent']; ?>" class="form-control" placeholder="top up percent" aria-label="Pool">
                                </div>

                                <div class="form-group">
                                    <label for="pool">Criteria ($)</label>
                                    <input type="number" name="criteria" value="<?php if(!empty($meta_data['criteria'])) echo $meta_data['criteria']; ?>" class="form-control" placeholder="top up criteria" aria-label="Pool">
                                </div>



                                <div class="row">
                                    <div class="col-lg-12">
                                        <h2 class="page-header"> Topup Settings </h2>
                                    </div> <!-- /.col-lg-12 -->
                                </div>
                                
                                <div class="form-group">
                                    <label for="topup_amount">Monthly Topup Amount ($)</label>
                                    <input type="number" name="topup_amount" value="<?php if(get_option('topup_amount') != 'topup_amount') echo get_option('topup_amount'); ?>" class="form-control" placeholder="Monthly topup amount" aria-label="topup_amount">
                                </div>

                                <div class="form-group">
                                    <label for="campaigns_eligible" class="control-label">  Number of Topup Normal Beneficiary </label>
                                    <input type="text" class="form-control" value="<?php if(get_option('campaigns_eligible') != 'campaigns_eligible') echo get_option('campaigns_eligible'); ?>" id="campaigns_eligible" name="campaigns_eligible" placeholder="50">
                                </div>

                                <div class="form-group">
                                    <label for="campaigns_eligible" class="control-label">  Number of Topup XCV Beneficiary </label>
                                    <input type="text" class="form-control" value="<?php if(get_option('campaigns_xcv_eligible') != 'campaigns_xcv_eligible') echo get_option('campaigns_xcv_eligible'); ?>" id="campaigns_xcv_eligible" name="campaigns_xcv_eligible" placeholder="50">
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h2 class="page-header"> Annuity Donation </h2>
                                    </div> <!-- /.col-lg-12 -->
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="pool">Minimum Annuity Donation ($)</label>
                                            <input type="number" name="annuity" value="<?php if(!empty($meta_data['annuity'])) echo $meta_data['annuity']; ?>" class="form-control" placeholder="Minimum annuity amount" aria-label="Pool">
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div>
                                            <p style="font-size:18px">Total Annuity Donation</p>
                                            <b>{{$totalAnnuityDonation}}</b>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="pool">Percentage Rate ($)</label>
                                            <input type="number" id="rate" name="annuity-rate" value="<?php if(!empty($meta_data['annuity-rate'])) echo $meta_data['annuity-rate']; ?>" class="form-control" placeholder="10%" aria-label="Pool">
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div style="font-size:18px; margin-top:35px" id="showVal"></div>
                                    </div>
                                </div>
                                
                                <!-- <hr /> -->

                                <div class="form-imput">
                                    <button style="margin-bottom:10px" type="submit" class="btn btn-primary btn-lg col-md-4 pull-left btn-orange mx-auto d-block">Save</button>
                                </div>

                            {{ Form::close() }}
                        
                        </div>
                        

                    </div>

                </div>   <!-- /#page-wrapper -->

            </div>   <!-- /#wrapper -->
            </div>

        </div> <!-- /#container -->
    </div> <!-- /#dashboard wrap -->

    <script>
        var temp = ($('#rate').val()/100) * {{$totalAnnuityDonation}}
        $('#showVal').html('$'+temp)


        $('#rate').keyup(() => {
            var rate = $('#rate').val()
            var temp = (rate/100) * {{$totalAnnuityDonation}}
            $('#showVal').html('$'+temp)
        });

        $('.fixed').hide();
        $('.percentage').hide();

        @if( !empty($meta_data['amount']) )
            $('.fixed').show();
        @endif

        @if( !empty($meta_data['percent']) )
            $('.percentage').show();
        @endif

        $('select#select').change( () => {
            if( $('select#select').val() == 'percentage' ){
                showPercentageOption();
            }
            else if( $('select#select').val() == 'fixed' ){
                showFixedOption();
            }
            else{
                $('.fixed').hide();
                $('.percentage').hide();
            }
        });

        function showPercentageOption(){
            $('.fixed').hide();
            $('.fixed>input').val(null)
            $('.percentage').show();
        }

        function showFixedOption() {
            $('.percentage').hide();
            $('.percentage>input').val(null)
            $('.fixed').show();
        }

    </script>

@endsection

@section('page-js')


@endsection