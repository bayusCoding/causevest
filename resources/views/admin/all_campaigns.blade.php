@extends('layouts.admin')

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>

    <div class="dashboard-wrap font">
        <div class="container">
            <div id="wrapper" class="row" style="margin-top:60px">

                @include('admin.sidebar')

                <div class="col-md-9">
                <div id="page-wrapper">

                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-6">
                                <h1 class="page-header"> {{ $title }}  </h1>
                            </div> <!-- /.col-lg-12 -->
                            <div class="col-md-6">
                                    <div class="row">
                                        <form class="form-inline" method="get" action="{{route('campaign_admin_search')}}">
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <input type="text" name="q" value="{{request('q')}}" class="form-control" placeholder="title keyword">
                                                </div>
                                            </div>
                                                <button type="submit" class="btn btn-default">@lang('app.search')</button>
                                            
                                        </form>
                                    </div>

                               </div>
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')

                       <div class="admin-campaign-lists">
                           <div class="row">
                               <div class="col-md-3">
                                   @lang('app.total') : {{$campaigns->count()}}
                               </div>
                           </div>
                       </div>

                    @if($campaigns->count() > 0)
                        <table class="table table-striped table-bordered">

                            <tr>
                                <th></th>
                                <th>@lang('app.title')</th>
                                <th>@lang('app.campaign_info')</th>
                                <th>@lang('app.owner_info')</th>
                                <th>@lang('app.actions')</th>
                            </tr>

                            @foreach($campaigns as $campaign)

                                <tr>

                                    <td width="100"><img height="100px" width="150px" src="{{$campaign->feature_img_url()}}" class="img-responsive" /></td>
                                    <td>{{$campaign->title}}

                                        @if($campaign->is_funded == 1)
                                            <p class="bg-success">@lang('app.added_to_funded')</p>
                                        @endif
                                    </td>
                                    <td style="width:20%">
                                        @lang('app.goal') : {{get_amount($campaign->goal)}} <br />
                                        @lang('app.raised') :  {{get_amount($campaign->success_payments->sum('amount'))}} <br />
                                        @lang('app.raised_percent') : {{$campaign->percent_raised()}}%<br />
                                        @lang('app.days_left') : {{$campaign->days_left()}}<br />
                                        @lang('app.backers') : {{$campaign->success_payments->count()}}<br />
                                    </td>

                                    <td>
                                        <strong>{{$campaign->user->name}}</strong> <br />
                                        @lang('app.address') : {{$campaign->address}}
                                    </td>

                                    <td style="width:20%">
                                        <a href="{{route('campaign_single', [$campaign->id, $campaign->slug])}}" class="btn btn-default btn-sm" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i> </a>

                                    @if($campaign->status == 0)
                                            <a href="{{route('campaign_status', [$campaign->id, 'approve'])}}" class="btn btn-success btn-sm" data-toggle="tooltip" title="@lang('app.approve')"><i class="fa fa-check-circle-o"></i> </a>
                                            <a href="{{route('campaign_status', [$campaign->id, 'block'])}}" class="btn btn-danger btn-sm" data-toggle="tooltip" title="@lang('app.block')"><i class="fa fa-ban"></i> </a>


                                        @elseif($campaign->status == 1)

                                            <!-- Modal -->
                                            <div class="modal fade" id="{{$campaign->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4>Are you sure you want to block this campaign<h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <a href="{{route('campaign_status', [$campaign->id, 'block'])}}" class="btn btn-orange">Block</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <span data-toggle="modal" data-target="#{{$campaign->id}}">
                                                <a class="btn btn-danger btn-sm" title="@lang('app.block')" data-toggle="tooltip"><i class="fa fa-ban w"></i> </a>
                                            </span>
                                        @elseif($campaign->status == 2)
                                            <a href="{{route('campaign_status', [$campaign->id, 'approve'])}}" class="btn btn-success btn-sm" data-toggle="tooltip" title="@lang('app.approve')"><i class="fa fa-check-circle-o"></i> </a>
                                        @endif

                                            <div class="modal fade" id="delete-{{$campaign->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4>Are you sure you want to delete this campaign<h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <a href="{{route('campaign_delete', $campaign->id)}}" class="btn btn-orange">Delete</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        <span data-toggle="modal" data-target="#delete-{{$campaign->id}}">
                                            <a class="btn btn-delete btn-danger btn-sm" data-toggle="tooltip" title="@lang('app.delete')"><i class="fa fa-trash-o w"></i> </a>
                                        </span>
                                        @if(request()->segment(3) == 'expired_campaigns')
                                            @if($campaign->is_funded != 1)
                                                <a href="{{route('campaign_status', [$campaign->id, 'funded'])}}" class="btn btn-info btn-sm" data-toggle="tooltip" title="@lang('app.mark_as_funded')"><i class="fa fa-check-circle-o"></i>  @lang('app.mark_as_funded')</a>
                                            @endif
                                        @endif

                                        {{-- @if($campaign->is_staff_picks != 1)
                                            <a href="{{route('campaign_status', [$campaign->id, 'add_staff_picks'])}}" class="btn btn-info btn-sm" data-toggle="tooltip" title="@lang('app.add_staff_picks')"><i class="fa fa-plus-square-o"></i>  @lang('app.add_staff_picks')</a>

                                        @else
                                            <a href="{{route('campaign_status', [$campaign->id, 'remove_staff_picks'])}}" class="btn btn-warning btn-sm" data-toggle="tooltip" title="@lang('app.remove_staff_picks')"><i class="fa fa-minus-square-o"></i>  @lang('app.remove_staff_picks')</a>
                                        @endif --}}


                                        <div class="modal fade" id="eligible-{{$campaign->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel"></h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <h4>Are you sure you want to 
                                                            @if( $campaign->topup_eligibility == 1 )
                                                                revoke this campaign's topup eligibility
                                                            @else
                                                                approve this campaign for top up
                                                            @endif
                                                        <h4>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <a href="{{route('campaign_eligibility', $campaign->id)}}" class="btn btn-orange">Approve</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        {{-- <span data-toggle="modal" data-target="#eligible-{{$campaign->id}}">
                                            <a @if( $campaign->topup_eligibility == 1 ) class="btn btn-success btn-sm" @else class="btn btn-danger btn-sm" @endif data-toggle="tooltip" title="topup eligible"><i class="fa fa-check-circle-o w"></i>  </a>
                                        </span> --}}
                                    </td>

                                </tr>

                            @endforeach

                        </table>

                        {!! $campaigns->links() !!}
                    @else
                        @lang('app.no_campaigns_to_display')
                    @endif

                </div>

                </div>

            </div>
        </div>
    </div>


@endsection

@section('page-js')

    <script type="text/javascript">
        $(document).ready(function() {
            $('.btn-delete').click(function(e){
                if (! confirm("@lang('app.are_you_sure_undone')") ){
                    e.preventDefault();
                }
            });
        });
    </script>
@endsection