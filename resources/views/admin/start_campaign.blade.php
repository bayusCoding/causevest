@extends('layouts.admin')

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('page-css')
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/summernote/summernote.css')}}">
@endsection


@section('content')
<style>
    .alert{
        background-color:#100c0114 !important;
        border-color:#100c0114 !important;
        color: #000 !important;
    }
    .text-info{
        color: #125cd5 !important;
    }
</style>
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>
    <div class="dashboard-wrap font">
        <div class="container">
            <div style="margin-top: 50px" id="wrapper" class="row">

                <div class="col-md-3" style="padding:10px; font-size:14">
                    {{-- @include('admin.menu') --}}
                        <div class="row">
                            <div class="col-xs-12 col-md-10 user-card card mt-0">
                            <div class="row">
                                    <div class="card-header col-md-12">
                                        <div class="campaign-profile user-profile image-center vertical-center">
                                            <div class="image-profile">
                                                <img src="{{ $user->get_gravatar(100) }}">
                                            </div>
                                            <div class="profile-text">
                                                <h4>{{$user->name}}</h4>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                            <a href="{{route('my_campaigns')}}">Campaigns</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                {{$userCampaignCount}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                <a href="{{route('view-donations')}}">Donations</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                            {{$payments->count()}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                Following
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                3
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                <a href="{{route('disputes')}}" >Disputes</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                0
                                            </h4>
                                        </div>
                                    </div>
                                    
                                    <div class="btn-wrapper">
                                        <a href="{{route('start_campaign')}}" class="btn btn-primary btn-lg col-md-10 btn-orange mx-auto d-block" role="button" aria-label="ask a question">Start a Campaign</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                <div class="col-md-9">
                    <div id="page-wrapper">

                        @if( ! empty($title))
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 class="page-header mt-1"> {{ $title }}  </h3>
                                </div> <!-- /.col-lg-12 -->
                            </div> <!-- /.row -->
                        @endif

                        @include('admin.flash_msg')

                        <div class="row">
                            <div class="col-md-10 col-xs-12">

                                {{ Form::open(['id'=>'startCampaignForm', 'class' => 'form-horizontal', 'files' => true]) }}

                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <i class="fa fa-info-circle"></i> @lang('app.feature_available_info')
                                    </div>
                                </div>

                                <legend class="mt-5">@lang('app.campaign_info')</legend>

                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group  {{ $errors->has('category')? 'has-error':'' }}">
                                            <label for="category" class="control-label">@lang('app.category') <span class="field-required">*</span></label>
                                            <div>
                                                <select class="form-control select2" name="category">
                                                    <option value="">@lang('app.select_a_category')</option>
                                                    @foreach($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                                    @endforeach
                                                </select>
                                                {!! $errors->has('category')? '<p class="help-block">'.$errors->first('category').'</p>':'' !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group {{ $errors->has('title')? 'has-error':'' }}">
                                            <label for="title" class="control-label">@lang('app.title') <span class="field-required">*</span></label>
                                            <div>
                                                <input type="text" class="form-control" id="title" value="{{ old('title') }}" name="title" placeholder="@lang('app.title')">
                                                {!! $errors->has('title')? '<p class="help-block">'.$errors->first('title').'</p>':'' !!}
                                                <small class="text-info"> @lang('app.great_title_info')</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                                

                                <div class="form-group {{ $errors->has('short_description')? 'has-error':'' }}">
                                    <label for="short_description" class="control-label">@lang('app.short_description')</label>
                                    <div class="">
                                        <textarea name="short_description" class="form-control" rows="3">{{ old('short_description') }}</textarea>
                                        {!! $errors->has('short_description')? '<p class="help-block">'.$errors->first('short_description').'</p>':'' !!}
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('description')? 'has-error':'' }}">
                                    <label for="description" class="control-label">@lang('app.description') <span class="field-required">*</span></label>
                                    <div class="">
                                        <div class="alert alert-info"> @lang('app.image_insert_limitation') </div>
                                    </div>
                                    <div class="">
                                        <textarea name="description" class="form-control description" rows="8">{{ old('description') }}</textarea>
                                        {!! $errors->has('description')? '<p class="help-block">'.$errors->first('description').'</p>':'' !!}
                                        <p class="text-info"> @lang('app.description_info_text')</p>
                                    </div>
                                </div>

                                <div class="alert alert-info">
                                    <h4> <i class="fa fa-money"></i> @lang('app.you_will_get') {{get_option('campaign_owner_commission')}}% @lang('app.of_total_raised')</h4>
                                </div>

                                <div class="form-group {{ $errors->has('goal')? 'has-error':'' }}">
                                    <label for="goal" class="control-label">@lang('app.goal') <span class="field-required">*</span></label>
                                    <div class="">
                                        <input type="number" class="form-control" id="goal" value="{{ old('goal') }}" name="goal" placeholder="@lang('app.goal')">
                                        {!! $errors->has('goal')? '<p class="help-block">'.$errors->first('goal').'</p>':'' !!}
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col">
                                    <div class="form-group {{ $errors->has('min_amount')? 'has-error':'' }}">
                                            <label for="min_amount" class="control-label">@lang('app.min_amount')</label>
                                            <div class="">
                                                <input type="number" class="form-control" id="min_amount" value="{{ old('min_amount') }}" name="min_amount" placeholder="@lang('app.min_amount')">
                                                {!! $errors->has('min_amount')? '<p class="help-block">'.$errors->first('min_amount').'</p>':'' !!}
                                            </div>
                                        </div>  
                                    </div>

                                    <div class="col">
                                        <div class="form-group {{ $errors->has('max_amount')? 'has-error':'' }}">
                                            <label for="max_amount" class="control-label">@lang('app.max_amount')</label>
                                            <div class="">
                                                <input type="number" class="form-control" id="max_amount" value="{{ old('max_amount') }}" name="max_amount" placeholder="@lang('app.max_amount')">
                                                {!! $errors->has('max_amount')? '<p class="help-block">'.$errors->first('max_amount').'</p>':'' !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col">
                                    <div class="form-group {{ $errors->has('recommended_amount')? 'has-error':'' }}">
                                            <label for="recommended_amount" class="control-label">@lang('app.recommended_amount')</label>
                                            <div class="">
                                                <input type="number" class="form-control" id="recommended_amount" value="{{ old('recommended_amount') }}" name="recommended_amount" placeholder="@lang('app.recommended_amount')">
                                                {!! $errors->has('recommended_amount')? '<p class="help-block">'.$errors->first('recommended_amount').'</p>':'' !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group {{ $errors->has('amount_prefilled')? 'has-error':'' }}">
                                            <label for="amount_prefilled" class="control-label">@lang('app.amount_prefilled')</label>
                                            <div class="">
                                                <input type="text" class="form-control" id="amount_prefilled" value="{{ old('amount_prefilled') }}" name="amount_prefilled" placeholder="@lang('app.amount_prefilled')">
                                                {!! $errors->has('amount_prefilled')? '<small class="help-block">'.$errors->first('amount_prefilled').'</small>':'' !!}
                                                <small class="text-info"> @lang('app.amount_prefilled_info_text')</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                


                                <div class="form-group {{ $errors->has('end_method')? 'has-error':'' }}">
                                    <label for="end_method" style="text-align: center" class="control-label">@lang('app.campaign_end_method')</label>
                                    <div class="form-row">
                                        <div class="col-md-5">
                                            <label>
                                                <input type="radio" name="end_method"  value="goal_achieve" @if(old('end_method') == 'goal_achieve') checked="checked" @endif > @lang('app.after_goal_achieve')
                                            </label>
                                        </div>
                                        <div class="col">
                                            <label>
                                                <input type="radio" name="end_method" value="end_date"  @if(old('end_method') == 'end_date') checked="checked" @endif > @lang('app.after_end_date')
                                            </label>
                                        </div>
                                        <div class="col">
                                            <label>
                                                <input type="radio" name="end_method" value="perpetuity"  @if(old('end_method') == 'perpetuity') checked="checked" @endif > @lang('app.perpetuity')
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">                                        
                                        {{--<label>
                                            <input type="radio" name="end_method" value="both"  @if(old('end_method') == 'both') checked="checked" @endif > @lang('app.both_need')
                                        </label>--}}

                                        {!! $errors->has('end_method')? '<p class="help-block">'.$errors->first('end_method').'</p>':'' !!}

                                        <small class="text-info"> @lang('app.end_method_info_text')</small>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="end_method" style="text-align: center" class="control-label">Campaign Balance Option</label>
                                    <div class="form-row">
                                        <div class="col-md-5">
                                            <label>
                                                <input type="radio" name="show_balance"  value="1" checked="checked"> Show My Campaign Balance
                                            </label>
                                        </div>
                                        <div class="col">
                                            <label>
                                                <input type="radio" name="show_balance" value="0" > Hide My Campaign Balance
                                            </label>
                                        </div>
                                    </div>
                                </div>



                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group {{ $errors->has('video')? 'has-error':'' }}">
                                        <label for="video" class="control-label">Video Url</label>
                                        <div class="">
                                            <input type="text" class="form-control" id="video" value="{{ old('video') }}" name="video" placeholder="https://www.youtube.com/watch?v=t4b72JKvko8">
                                            {!! $errors->has('video')? '<p class="help-block">'.$errors->first('video').'</p>':'' !!}
                                            <small class="text-info"> @lang('app.video_info_text')</small>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group  {{ $errors->has('country_id')? 'has-error':'' }}">
                                            <label for="country_id" class="control-label">@lang('app.country')<span class="field-required">*</span></label>
                                            <div class="">
                                                <select class="form-control select2" name="country_id">

                                                    <option value="">@lang('app.select_a_country')</option>

                                                    @foreach($countries as $country)
                                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                                    @endforeach

                                                </select>
                                                {!! $errors->has('country_id')? '<p class="help-block">'.$errors->first('country_id').'</p>':'' !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                


                                

                                <div class="form-group {{ $errors->has('address')? 'has-error':'' }}">
                                    <label for="address" class="control-label">@lang('app.address')</label>
                                    <div class="">
                                        <input type="text" class="form-control" id="address" value="{{ old('address') }}" name="address" placeholder="@lang('app.address')">
                                        {!! $errors->has('address')? '<p class="help-block">'.$errors->first('address').'</p>':'' !!}
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="col">
                                    <div class="form-group {{ $errors->has('start_date')? 'has-error':'' }}">
                                        <label for="start_date" class="control-label">@lang('app.start_date')</label>
                                        <div class="">
                                            <input type="text" class="form-control" id="start_date" value="{{ old('start_date') }}" name="start_date" placeholder="@lang('app.start_date')">
                                            {!! $errors->has('start_date')? '<p class="help-block">'.$errors->first('start_date').'</p>':'' !!}
                                        </div>
                                    </div>
                                    </div>

                                    <div class="col">
                                        <div class="form-group {{ $errors->has('end_date')? 'has-error':'' }}">
                                            <label for="end_date" class="col-sm-4 control-label">@lang('app.end_date')</label>
                                            <div class="">
                                                <input type="text" class="form-control" id="end_date" value="{{ old('end_date') }}" name="end_date" placeholder="@lang('app.end_date')">
                                                {!! $errors->has('end_date')? '<p class="help-block">'.$errors->first('end_date').'</p>':'' !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                @if( $user->is_operator() || $user->is_admin() )
                                <div class="form-row">
                                    <div class="col">
                                        <div class="form-group">
                                            <h5 for="cause_campaign"> <input type="checkbox" name="cause_campaign" value="1"> Check if this is a campaign for someone else</h5>
                                        </div>
                                    </div>
                                </div>
                                @endif

                                <div class="show">
                                    <div class="form-row">
                                        <div class="col">                                        
                                            <div class="form-group">
                                                <label for="cause_name">Cause Name</label>
                                                <input type="text" name="cause_name" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                

                                    <div class="form-row">
                                        <div class="col">                                        
                                            <div class="form-group">
                                                <label for="cause_description">Cause Description</label>
                                                <textarea type="text" name="cause_description" rows="5" col="10" class="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div class="col">
                                            <div class="form-group">
                                                <label for="cause_contact">Cause Contact</label>
                                                <textarea type="text" name="cause_contact" rows="5" col="10" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col">                                        
                                            <div class="form-group">
                                                <label for="cause_account">Cause Account Number</label>
                                                <input type="number" name="cause_account" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                            
                                <div class="form-group">
                                    <div class="col-sm-offset-4">
                                        <button type="submit" class="btn btn-primary btn-lg btn-orange btn">@lang('app.submit_new_campaign')</button>
                                    </div>
                                </div>

                                {{ Form::close() }}

                            </div>
                        </div>



                    </div>
                </div>

            </div>
        </div>
    </div>

<script>
    $('.show').hide();

    $('input:checkbox').change(() => {
        $('.show').toggle();
    })
</script>
@endsection

@section('page-js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js')}}"></script>

    <!-- <script src="{{asset('assets/plugins/summernote/summernote.js')}}"></script> -->
    <script>
        $(function () {
            $('#start_date, #end_date').datetimepicker({format: 'YYYY-MM-DD'});
        });

        $(document).ready(function() {
            $('.description').summernote({  height: 300});
        });

    </script>
@endsection