@extends('layouts.admin')

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp


@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>
    <div class="dashboard-wrap font">
        <div class="container">
            <div style="margin-top: 40px" id="wrapper" class="row">

                {{-- <div class="col-md-3" style="padding:10px; font-size:14">
                    <div class="row">
                            <div class="col-xs-12 col-md-10 user-card card">
                                <div class="row">
                                    <div class="card-header col-md-12">
                                        <div class="campaign-profile user-profile image-center vertical-center">
                                            <div class="image-profile">
                                                <img src="{{ $user->get_gravatar(100) }}">
                                            </div>
                                            <div class="profile-text">
                                                <h4>{{ $user->name }}</h4>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                            <a href="{{route('my_campaigns')}}">Campaigns</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                {{$userCampaignCount}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                            <a href="{{route('view-donations')}}">Donations</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                            {{$payments->count()}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                Following
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                3
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                <a href="{{route('disputes')}}" >Disputes</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                0
                                            </h4>
                                        </div>
                                    </div>
                                    
                                    <div class="btn-wrapper">
                                        <a href="{{route('start_campaign')}}" class="btn btn-primary btn-lg col-md-10 btn-orange mx-auto d-block" role="button" aria-label="ask a question">Start A Campaign</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                </div> --}}
                @include('admin.sidebar')

                <div class="col-md-9">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header"> {{ $title }}  </h1>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')

                    {{ Form::open(['route'=>'save_settings','class' => 'form-horizontal', 'files' => true]) }}


                    <div id="social_login_settings_wrap">

                        <div class="form-group {{ $errors->has('enable_recaptcha_login')? 'has-error':'' }}">
                            <label class="col-md-4 control-label">@lang('app.enable_disable') </label>
                            <div class="col-md-8">
                                <label for="enable_recaptcha_login" class="checkbox-inline">
                                    <input type="checkbox" value="1" id="enable_recaptcha_login" name="enable_recaptcha_login" {{ get_option('enable_recaptcha_login') == 1 ? 'checked="checked"': '' }}>
                                    @lang('app.enable_recaptcha_login')
                                </label>
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('enable_recaptcha_registration')? 'has-error':'' }}">
                            <label class="col-md-4 control-label">@lang('app.enable_disable') </label>
                            <div class="col-md-8">
                                <label for="enable_recaptcha_registration" class="checkbox-inline">
                                    <input type="checkbox" value="1" id="enable_recaptcha_registration" name="enable_recaptcha_registration" {{ get_option('enable_recaptcha_registration') == 1 ? 'checked="checked"': '' }}>
                                    @lang('app.enable_recaptcha_registration')
                                </label>
                            </div>
                        </div>

                        <div class="form-group {{ $errors->has('enable_recaptcha_contact_form')? 'has-error':'' }}">
                            <label class="col-md-4 control-label">@lang('app.enable_disable') </label>
                            <div class="col-md-8">
                                <label for="enable_recaptcha_contact_form" class="checkbox-inline">
                                    <input type="checkbox" value="1" id="enable_recaptcha_contact_form" name="enable_recaptcha_contact_form" {{ get_option('enable_recaptcha_contact_form') == 1 ? 'checked="checked"': '' }}>
                                    @lang('app.enable_recaptcha_contact_form')
                                </label>
                            </div>
                        </div>

                        <hr />
                        <div class="form-group">
                            <label for="recaptcha_site_key" class="col-sm-4 control-label">@lang('app.site_key')</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="site_key" value="{{ get_option('recaptcha_site_key') }}" name="recaptcha_site_key" placeholder="@lang('app.site_key')">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="recaptcha_secret_key" class="col-sm-4 control-label">@lang('app.secret_key')</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="recaptcha_secret_key" value="{{ get_option('recaptcha_secret_key') }}" name="recaptcha_secret_key" placeholder="@lang('app.secret_key')">
                            </div>
                        </div>



                    </div>

                    <hr />

                    <div class="form-group">
                        <div class="col-sm-offset-4 col-sm-8">
                            <button type="submit" id="settings_save_btn" class="btn btn-orange">@lang('app.save_settings')</button>
                        </div>
                    </div>

                    {{ Form::close() }}

                </div>   <!-- /#page-wrapper -->

            </div>   <!-- /#wrapper -->
            </div>

        </div> <!-- /#container -->
    </div> <!-- /#dashboard -->
@endsection


@section('page-js')
    <script>
        $(document).ready(function(){

            $('input[type="checkbox"], input[type="radio"]').click(function(){
                var input_name = $(this).attr('name');
                var input_value = 0;
                if ($(this).prop('checked')){
                    input_value = $(this).val();
                }
                $.ajax({
                    url : '{{ route('save_settings') }}',
                    type: "POST",
                    data: { [input_name]: input_value, '_token': '{{ csrf_token() }}' }
                });
            });

            /**
             * show or hide stripe and paypal settings wrap
             */
            $('#enable_facebook_login').click(function(){
                if ($(this).prop('checked')){
                    $('#facebook_login_api_wrap').slideDown();
                }else{
                    $('#facebook_login_api_wrap').slideUp();
                }
            });
            $('#enable_google_login').click(function(){
                if ($(this).prop('checked')){
                    $('#google_login_api_wrap').slideDown();
                }else{
                    $('#google_login_api_wrap').slideUp();
                }
            });

            /**
             * Send settings option value to server
             */
            $('#settings_save_btn').click(function(e){
                e.preventDefault();

                var this_btn = $(this);
                this_btn.attr('disabled', 'disabled');

                var form_data = this_btn.closest('form').serialize();
                $.ajax({
                    url : '{{ route('save_settings') }}',
                    type: "POST",
                    data: form_data,
                    success : function (data) {
                        if (data.success == 1){
                            this_btn.removeAttr('disabled');
                            toastr.success(data.msg, '@lang('app.success')', toastr_options);
                        }
                    }
                });
            });

        });
    </script>
@endsection