@extends('layouts.admin')

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('content')

<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>
    <div class="dashboard-wrap font">
        <div class="container">
            <div style="margin-top: 40px" id="wrapper" class="row">  
                <div class="col-md-3" style="padding:10px; font-size:14">
                    <div class="row">
                            <div class="col-xs-12 col-md-10 user-card card">
                                <div class="row">
                                    <div class="card-header col-md-12">
                                        <div class="campaign-profile user-profile image-center vertical-center">
                                            <div class="image-profile">
                                                <img src="{{ $user->get_gravatar(100) }}">
                                            </div>
                                            <div class="profile-text">
                                                <h4>{{ $user->name }}</h4>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                            <a href="{{route('my_campaigns')}}">Campaigns</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                {{$userCampaignCount}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                            <a href="{{route('view-donations')}}">Donations</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                            {{$payments->count()}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                Following
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                3
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                <a href="{{route('disputes')}}" >Disputes</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                0
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="btn-wrapper">
                                        <a href="{{route('start_campaign')}}" class="btn btn-primary btn-lg col-md-10 btn-orange mx-auto d-block" role="button" aria-label="ask a question">Start A Campaign</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                </div>


                <div class="col-md-9 font">
                <div id="page-wrapper">

                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header"> {{ $title }}  </h1>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')


                    <div class="admin-campaign-lists">

                        <div class="row">
                            <div class="col-md-5">

                            </div>

                            <div class="col-md-7">

                                <!-- <form class="form-inline" method="get" action="">
                                    <div class="form-group">
                                        <input type="text" name="q" value="{{request('q')}}" class="form-control" placeholder="@lang('app.payer_email')">
                                    </div>
                                    <button type="submit" class="btn btn-default">@lang('app.search')</button>
                                </form> -->

                            </div>
                        </div>

                    </div>

                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>@lang('app.campaign_title')</th>
                            <td>{{$payment->campaign->title}}</td>
                        </tr>

                        <tr>
                            <th>@lang('app.payer_name')</th>
                            <td>{{$payment->name}}</td>
                        </tr>
                        <tr>
                            <th>@lang('app.payer_email')</th>
                            <td>{{$payment->email}}</td>
                        </tr>

                        <tr>
                            <th>@lang('app.amount')</th>
                            <td>{{get_amount($payment->amount)}}</td>
                        </tr>

                        @if ($payment->annuity_status == 1)
                            <tr>
                                <th>Annuity Information</th>
                                @if( !empty($payment->annuity_info) )<td>{{($payment->annuity_info)}}</td> @endif
                            </tr>
                        @endif

                        <tr>
                            <th>@lang('app.method')</th>
                            <td>{{$payment->payment_method}}</td>
                        </tr>

                        <tr>
                            <th>@lang('app.currency')</th>
                            <td>{{$payment->currency}}</td>
                        </tr>

                        @if($payment->payment_method == 'stripe')

                            <tr>
                                <th>@lang('app.card_last4')</th>
                                <td>{{$payment->card_last4}}</td>
                            </tr>

                            <tr>
                                <th>@lang('app.card_id')</th>
                                <td>{{$payment->card_id}}</td>
                            </tr>

                            <tr>
                                <th>@lang('app.card_brand')</th>
                                <td>{{$payment->card_brand}}</td>
                            </tr>

                            <tr>
                                <th>@lang('app.card_expire')</th>
                                <td>{{$payment->card_exp_month}},{{$payment->card_exp_year}}</td>
                            </tr>

                        @endif

                        <tr>
                            <th>@lang('app.gateway_transaction_id')</th>
                            <td>{{$payment->charge_id_or_token}}</td>
                        </tr>

                        @if($payment->payment_method == 'bank_transfer')
                            <tr>
                                <th colspan="2"><h4>@lang('app.bank_transfer_information')</h4></th>
                            </tr>
                            <tr>
                                <th>@lang('app.bank_swift_code')</th>
                                <td>{{$payment->bank_swift_code}}</td>
                            </tr>

                            <tr>
                                <th>@lang('app.account_number')</th>
                                <td>{{$payment->account_number}}</td>
                            </tr>

                            <tr>
                                <th>@lang('app.branch_name')</th>
                                <td>{{$payment->branch_name}}</td>
                            </tr>

                            <tr>
                                <th>@lang('app.branch_address')</th>
                                <td>{{$payment->branch_address}}</td>
                            </tr>

                            <tr>
                                <th>@lang('app.account_name')</th>
                                <td>{{$payment->account_name}}</td>
                            </tr>

                            <tr>
                                <th>@lang('app.iban')</th>
                                <td>{{$payment->iban}}</td>
                            </tr>
                        @endif

                        <tr>
                            <th>@lang('app.time')</th>
                            <td>{{$payment->created_at->format('F d, Y h:i a')}}</td>
                        </tr>

                        <tr>
                            <th>Reassign Donation</th>
                            <th>
                                {{ Form::open(['route'=>'reassign-donation-post','class' => 'form-horizontal', 'files' => true]) }}
                                <div class="row">
                                    <div class="col">
                                        <select name="campaign_id" class="form-control">
                                            @foreach($annuityCampaigns as $annuityCampaign)
                                                <option @if( $payment->campaign_id == $annuityCampaign->id ) selected="selected" @endif value="{{$annuityCampaign->id}}">{{$annuityCampaign->title}}</option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" name="payment_id" value="{{$payment->id}}">
                                    </div>

                                    <div class="col">
                                        <div class="form-imput">
                                            <button type="submit" class="btn btn-primary btn-lg col-md-4 pull-left btn-orange mx-auto d-block">Save</button>
                                        </div>
                                    </div>
                                </div>
                                                                
                                {{ Form::close() }}
                            </th>
                        </tr>
                    </table>


                    @if($payment->reward)
                        <h3>@lang('app.selected_reward')</h3>
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th>@lang('app.amount')</th>
                                <td>{{get_amount($payment->reward->amount)}}</td>
                            </tr>
                            <tr>
                                <th>@lang('app.description')</th>
                                <td>{{$payment->reward->description}}</td>
                            </tr>
                            <tr>
                                <th>@lang('app.estimated_delivery')</th>
                                <td>{{$payment->reward->estimated_delivery}}</td>
                            </tr>                            
                        </table>
                    @endif

                    @if($auth_user->is_admin() || $auth_user->is_operator())
                        @if($payment->annuity_status == 1)
                        {{Form::open([ 'route' => 'update_annuity', 'class' => 'form-horizontal'])}}
                            <h2>Annuity Information</h2>
                            <div class="form-group">
                                <label for="pool">Annuity</label>
                                <textarea name="annuity" required class="form-control" placeholder="E.g 5% per annum" aria-label="Annuity"><?php if( !empty($payment->annuity_info) ) echo $payment->annuity_info; ?></textarea>
                            </div>
                            <input type="hidden" name="id" value="{{$payment->id}}">
                            <button type="submit" style="text-align: center" class="btn btn-filled">save</button>
                        {{Form::close()}}
                        @endif

                    @else

                        @if( !empty($payment->annuity_info) )
                            <h3> Annuity Information </h3>
                            <p>{{$payment->annuity_info}}</p>
                        @endif

                    @endif
                        
                    @if($payment->status != 'success')
                        <a href="{{route('status_change', [$payment->id, 'success'] )}}" class="btn btn-success"><i class="fa fa-check-circle-o"></i> @lang('app.mark_as_success') </a>
                    @endif



                </div>
                </div>

            </div>
        </div>
    </div>


@endsection