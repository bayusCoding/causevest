@extends('layouts.admin')

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('content')
<style>
a.btn.btn-primary.btn-lg.btn-light.btn-block {
    width: auto;
    margin-left: 10px;
    padding: .77rem;
    font-size: 12px;
    word-wrap: break-word;
    float: right;
}

.table td, .table th {
    padding: .4rem;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
}
</style>
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>

    <div class="dashboard-wrap font">
        <div class="container">
            <div style="margin-top: 50px" id="wrapper" class="row">

                @include('admin.sidebar')

                <div class="col-md-9 font">
                <div id="page-wrapper">

                    @if( ! empty($title))
                        <div class="row">
                            <div class="col">
                                <h1 class="page-header">
                                    {{ $title }}
                                </h1>
                                
                            </div>
                            <div class="mb-3">
                            <select class="btn btn-primary btn-lg btn-light btn-block form-control" style="height: 50px;" name="formal" onchange="location = this.value;">
                                @if ($auth_user->is_admin())
                                <option value="{{ route('view-annuity') }}">Annuity Payments</option>
                                <option value="{{route('view-convenant')}}">Covenant Payments</option>
                                <option value="{{route('payments_pending')}}">@lang('app.pending_payments')</option>
                                @endif

                                <option value="{{route('view-donations')}}">View your Donations</option>
                            </select>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')


                    <div class="admin-campaign-lists">

                        <div class="row">
                            <div class="col-md-5">
                                @lang('app.total') : {{$payments->count()}}
                            </div>

                            <!-- <div class="col-md-7">

                                <form class="form-inline" method="get" action="">
                                    <div class="form-group">
                                        <input type="text" name="q" value="{{request('q')}}" class="form-control" placeholder="@lang('app.payer_email')">
                                    </div>
                                    <button type="submit" class="btn btn-default">@lang('app.search')</button>
                                </form>

                            </div> -->
                        </div>

                    </div>

                    @if($payments->count() > 0)
                        <table class="table table-striped table-bordered">

                            <tr>
                                <th>@lang('app.campaign_title')</th>
                                {{-- <th>@lang('app.payer_email')</th> --}}
                                <th>@lang('app.amount')</th>
                                <th>@lang('app.method')</th>
                                <th>Covenant</th>
                                <th>Response</th>
                                <th>@lang('app.time')</th>
                                <th>#</th>
                                <th>#</th>
                            </tr>

                            @foreach($payments as $payment)
                                <tr>
                                    <td>
                                        @if($payment->campaign)
                                            <a href="{{route('payment_view', $payment->id)}}">{{$payment->campaign->title}}</a>
                                        @else
                                            @lang('app.campaign_deleted')
                                        @endif
                                    </td>
                                    <!-- <td><a href="{{route('payment_view', $payment->id)}}"> {{$payment->email}} </a></td> -->
                                    <td>
                                        {{get_amount($payment->amount)}} 
                                        <?php 
                                            if(!empty($payment->top_up)) echo' + '.get_amount($payment->top_up);
                                            if($payment->responsestatus == 0 || $payment->responsestatus == 1) echo' <font color="green">Covenant Payment</font>';
                                        ?> 
                                    </td>
                                    
                                    <td>{{$payment->payment_method}}</td>
                                    <td>{{ $payment->convenant }}</td>
                                    <td>{{ $payment->causeresponse }}</td>
                                    <td><span data-toggle="tooltip" title="{{$payment->created_at->format('F d, Y h:i a')}}">{{$payment->created_at->format('F d, Y')}}</span></td>

                                    <td>
                                        @if($payment->reward)
                                            <a href="{{route('payment_view', $payment->id)}}" data-toggle="tooltip" title="@lang('app.selected_reward')">
                                                <i class="fa fa-gift"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($payment->status == 'success')
                                            <span class="text-success" data-toggle="tooltip" title="{{$payment->status}}"><i class="fa fa-check-circle-o"></i> </span>
                                        @else
                                            <span class="text-warning" data-toggle="tooltip" title="{{$payment->status}}"><i class="fa fa-exclamation-circle"></i> </span>
                                        @endif

                                        <a href="{{route('payment_view', $payment->id)}}"><i class="fa fa-eye"></i> </a>
                                    </td>

                                </tr>
                            @endforeach

                        </table>

                        {!! $payments->links() !!}

                    @else
                        @lang('app.no_data')
                    @endif

                </div>
                </div>
            </div>
        </div>
    </div>

  <script>

    $(document).ready(function() {
        $('#payment').DataTable();
    });

    function handleSelect(elm)
    {
        window.location = elm.value;
    }

</script>

@endsection