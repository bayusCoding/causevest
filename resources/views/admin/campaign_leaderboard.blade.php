@extends('layouts.admin')

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="dashboard-wrap font">
    <div class="container">
        <div style="margin-top: 40px" id="wrapper" class="row">

            @include('admin.sidebar')

            <div class="col-md-9">
                <div id="page-wrapper">
                    @if( ! empty($title))
                    <div class="row">
                        <div class="col-lg-12">
                            <h2 class="page-header"> {{-- $title --}}  </h2>
                        </div> <!-- /.col-lg-12 -->
                    </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')

                    <h4>XCV Leaderboard</h4>
                    <table id="tables" width="100%" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <td style="width:40%">Campaigns</td>
                                <td>XCV Votes</td>
                                <td> Amount </td>
                            </tr>
                        </thead>

                        @foreach($campaignXcv as $camp)
                        <tr>
                            <td> <a target="_blank" href="{{ route('campaign_single', $camp->campaign_id) }}"> {{ $camp->getCampaignTitle( $camp->campaign_id ) }} </a> </td>
                            <td>{{ $camp->getXcv( $camp->campaign_id ) }}</td>
                            <td> ${{ number_format( $xcv_amount) }} </td>
                        </tr>
                        @endforeach
                    </table>

                    <br><br><br>

                    <h4>Normal Leaderboard</h4>
                    <table id="table" width="100%" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <td style="width:40%">Campaigns</td>
                                <td>Normal Votes</td>
                                <td>Amount</td>
                            </tr>
                        </thead>

                        @foreach($campaignNormal as $camp)
                        <tr>
                            <td> <a target="_blank" href="{{ route('campaign_single', $camp->campaign_id) }}"> {{ $camp->getCampaignTitle( $camp->campaign_id ) }} </a> </td>
                            <td>{{ $camp->getNormal( $camp->campaign_id ) }}</td>
                            <td> ${{ number_format( $normal_amount) }} </td>
                        </tr>
                        @endforeach
                    </table>
                    
                    <br><br><br>

                    @foreach ($verify as $ver)
                    @if($ver->verify == 0)
                        <div class="btn-wrapper">
                            <form action="{{ route('update.verify') }}" method="post">
                                {{ csrf_field() }}
                                <button class="btn btn-primary btn-lg col-md-4 btn-orange mx-auto d-block" role="button" aria-label="ask a question">Approve Payment</button>
                            </form>
                        </div>

                    @else

                        <div class="btn-wrapper">
                            <form action="{{ route('update.cancelverify') }}" method="post">
                                {{ csrf_field() }}
                                <button class="btn btn-primary btn-lg col-md-4 btn-orange mx-auto d-block" role="button" aria-label="ask a question">Cancel Payment</button>
                            </form>
                        </div>
                    @endif
                    @endforeach
                    
                    <br><br><br>

                </div>   <!-- /#page-wrapper -->

            </div>   <!-- /#wrapper -->
        </div>

    </div> <!-- /#container -->
</div> <!-- /#dashboard wrap -->

<script>

    $(document).ready(function() {
        $('#table').DataTable({
            "order": [[ 1, "desc" ]]
        });

        $('#tables').DataTable({
            "order": [[ 1, "desc" ]]
        });
        
    } );

</script>

@endsection

@section('page-js')

@endsection