@extends('layouts.admin')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>

    <div class="dashboard-wrap font">

        <div class="container">

            <div style="margin-top: 60px" id="wrapper" class="row">

                @include('admin.sidebar')

                <div class="col-md-9">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header"> {{-- $title --}} Edit Category </h1>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')

                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-1 col-xs-12">

                            {{ Form::open(['class' => 'form-horizontal', 'files' => true]) }}


                            <div class="form-group {{ $errors->has('category_name')? 'has-error':'' }}">
                                <label for="category_name" class="col-sm-4 control-label">@lang('app.category_name')</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="category_name" value="{{ $category->category_name }}" name="category_name" placeholder="@lang('app.category_name')">
                                    {!! $errors->has('category_name')? '<p class="help-block">'.$errors->first('category_name').'</p>':'' !!}
                                </div>
                            </div>


                            <div class="form-group {{ $errors->has('image')? 'has-error':'' }}">
                                <label for="image" class="col-sm-4 control-label"></label>
                                <div class="col-sm-4">

                                    <img src="{{$category->get_image_url()}}" class="img-responsive img-thumbnail" />

                                </div>
                            </div>
                            
                            <div class="form-group {{ $errors->has('image')? 'has-error':'' }}">
                                <label for="image" class="col-sm-4 control-label">@lang('app.image')</label>
                                <div class="col-sm-8">

                               
                                    <input type="file" name="image" id="image" class="form-control">

                                    {!! $errors->has('image')? '<p class="help-block">'.$errors->first('image').'</p>':'' !!}

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="submit" class="btn btn-orange">@lang('app.save_new_category')</button>
                                </div>
                            </div>
                            {{ Form::close() }}

                        </div>

                    </div>

                </div>   <!-- /#page-wrapper -->
                </div>

            </div>   <!-- /#wrapper -->

        </div> <!-- /#container -->

    </div>
@endsection

@section('page-js')

@endsection