@extends('layouts.admin')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    
                        @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>
    <div class="dashboard-wrap font">

        <div class="container">

            <div id="wrapper" class="row" style="margin-top:60px">

                <div class="col-md-3">
                    <div class="row">
                            <div class="col-xs-12 col-md-10 user-card card">
                                <div class="row">
                                    <div class="card-header col-md-12">
                                        <div class="campaign-profile user-profile image-center vertical-center">
                                            <div class="image-profile">
                                                <img src="{{ asset('assets/img/campaign-img.png') }}">
                                            </div>
                                            <div class="profile-text">
                                                <h4>John Doe</h4>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                Campaigns
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                5
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                Donations
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                0
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                Following
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                3
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="btn-wrapper">
                                        <a href="{{route('start_campaign')}}" class="btn btn-primary btn-lg col-md-10 btn-orange mx-auto d-block" role="button" aria-label="ask a question">Start A Campaign</a>
                                    </div>

                                </div>
                            </div>
                    </div>
                </div>

                <div class="col-md-9">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header"> {{ $title }} <a href="{{route('edit_campaign', $campaign_id)}}" class="btn btn-sm btn-orange pull-right"><i class="fa fa-arrow-circle-o-left"></i> @lang('app.back_to_campaign')</a> </h1>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')

                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-1 col-xs-12">

                            {{ Form::open(['class' => 'form-horizontal', 'files' => true]) }}


                            <div class="form-group {{ $errors->has('title')? 'has-error':'' }}">
                                <label for="title" class="col-sm-4 control-label">@lang('app.title')</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="title" value="{{ old('title') }}" name="title" placeholder="@lang('app.title')">
                                    {!! $errors->has('title')? '<p class="help-block">'.$errors->first('title').'</p>':'' !!}
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('description')? 'has-error':'' }}">
                                <label for="description" class="col-sm-4 control-label">@lang('app.description')</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="description">{{old('description')}}</textarea>
                                    {!! $errors->has('description')? '<p class="help-block">'.$errors->first('description').'</p>':'' !!}
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="submit" class="btn btn-orange">@lang('app.save_faq')</button>
                                </div>
                            </div>
                            {{ Form::close() }}

                        </div>

                    </div>

                        @if($faqs->count())
                            <div class="row">
                                <div class="col-xs-12 col-sm-8">
                                    <table class="table table-bordered categories-lists">
                                        <tr>
                                            <th>@lang('app.title') </th>
                                            <th>@lang('app.description') </th>
                                            <th>@lang('app.action') </th>
                                        </tr>
                                        @foreach($faqs as $faq)
                                            <tr>
                                                <td> {{ $faq->title }}  </td>
                                                <td> {{ $faq->description }}  </td>
                                                <td width="100">
                                                    <a href="{{ route('faq_update', [$faq->campaign_id,$faq->id]) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> </a>
                                                    <a href="javascript:;" class="btn btn-danger btn-xs" data-id="{{ $faq->id }}"><i class="fa fa-trash"></i> </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        @endif



                </div>   <!-- /#page-wrapper -->

                </div>


            </div>   <!-- /#wrapper -->


        </div> <!-- /#container -->

    </div>
@endsection

@section('page-js')

    <script>
        $(document).ready(function() {
            $('.btn-danger').on('click', function (e) {
                if (!confirm("@lang('app.are_you_sure_undone')")) {
                    e.preventDefault();
                    return false;
                }

                var selector = $(this);
                var data_id = $(this).data('id');

                $.ajax({
                    type: 'POST',
                    url: '{{ route('delete_faq') }}',
                    data: {data_id: data_id, _token: '{{ csrf_token() }}'},
                    success: function (data) {
                        if (data.success == 1) {
                            selector.closest('tr').hide('slow');
                        }
                    }
                });
            });
        });
    </script>
@endsection