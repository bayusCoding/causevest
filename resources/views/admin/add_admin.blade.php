@extends('layouts.admin')

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp
<style>
    <style>
    .m{
        margin-bottom: 0;
        line-height: 1.5;
        text-align: left
    }
</style>
</style>
@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>

    <div class="dashboard-wrap font">
        <div class="container">
            <div style="margin-top: 40px" id="wrapper" class="row">

                <div class="col-md-3" style="padding:10px; font-size:14">
                    <div class="row">
                            <div class="col-xs-12 col-md-10 user-card card">
                                <div class="row">
                                    <div class="card-header col-md-12">
                                        <div class="campaign-profile user-profile image-center vertical-center">
                                            <div class="image-profile">
                                                <img src="{{ $user->get_gravatar(100) }}">
                                            </div>
                                            <div class="profile-text">
                                                <h4>{{ $user->name }}</h4>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                            <a href="{{route('my_campaigns')}}">Campaigns</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                {{$userCampaignCount}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                            <a href="{{route('view-donations')}}">Donations</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                            {{$payments->count()}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                Following
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                3
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                <a href="{{route('disputes')}}" >Disputes</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                0
                                            </h4>
                                        </div>
                                    </div>
                                    
                                    <div class="btn-wrapper">
                                        <a href="{{route('start_campaign')}}" class="btn btn-primary btn-lg col-md-10 btn-orange mx-auto d-block" role="button" aria-label="ask a question">Start A Campaign</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                </div>

                <div class="col-md-9">
                
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12">
                                <h2 class="page-header"> {{ $title }}  </h2>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')
                    

                    <div class="modal fade" id="pop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="m" id="exampleModalLabel">User Already Exists</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5>Do you want to make this user an Internal Operator<h5>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <a href="{{ route('existing_user', session('fail')) }}" class="btn btn-orange">Upgrade</a>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            {{ Form::open(['route'=>'add_admin','class' => 'form-horizontal', 'files' => true]) }}
                                
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="form-control" placeholder="Daniel" aria-label="Name" required>
                                </div>

                                <div class="form-group">
                                    <label for="email">Email Address</label>
                                    <input type="email" name="email" class="form-control" placeholder="me@example.com" aria-label="Email Address" required>
                                </div>

                                
                                <div class="form-group">
                                <label for="level"> User Role </label>
                                    <input type="text" name="user_type" value="Internal Operator" disabled class="form-control">
                                </div>

                                <div class="form-imput">
                                    <button type="submit" class="btn btn-primary btn-lg col-md-4 pull-left btn-orange mx-auto d-block">Save</button>
                                </div>

                            {{ Form::close() }}
                        
                        </div>
                        

                    </div>

                </div>   <!-- /#page-wrapper -->

            </div>   <!-- /#wrapper -->
            </div>

        </div> <!-- /#container -->
    </div> <!-- /#dashboard wrap -->
    <script>
        @if(session('fail'))
            $('#pop').modal('show');
        @else
            $('#pop').modal({ show: false});
        @endif
    </script>
@endsection

@section('page-js')


@endsection