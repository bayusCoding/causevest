@extends('layouts.admin')

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('content')
<style>
a.btn.btn-primary.btn-lg.btn-light.btn-block {
    width: auto;
    margin-left: 10px;
    padding: .77rem;
    font-size: 12px;
    word-wrap: break-word;
    float: right;
}

.table td, .table th {
    padding: .4rem;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
}
</style>
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>

    <div class="dashboard-wrap font">
        <div class="container">
            <div style="margin-top: 50px" id="wrapper" class="row">

                @include('admin.sidebar')

                <div class="col-md-9 font">
                <div id="page-wrapper">

                    @if( ! empty($title))
                        <div class="row">
                            <div class="col">
                                <h1 class="page-header">
                                    {{ $title }}
                                </h1>
                                
                            </div>
                            <div class="mb-3">
                            <select class="btn btn-primary btn-lg btn-light btn-block form-control" style="height: 50px;" name="formal" onchange="location = this.value;">
                                @if ($auth_user->is_admin())
                                <option value="{{ route('view-annuity') }}">Annuity Payments</option>
                                <option value="{{route('view-convenant')}}">Covenant Payments</option>
                                <option value="{{route('payments_pending')}}">@lang('app.pending_payments')</option>
                                @endif

                                <option value="{{route('view-donations')}}">View your Donations</option>
                            </select>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')


                    <div class="admin-campaign-lists">

                        <div class="row">
                            <div class="col-md-5">
                                @lang('app.total') : {{$payments->count()}}
                            </div>

                            <!-- <div class="col-md-7">

                                <form class="form-inline" method="get" action="">
                                    <div class="form-group">
                                        <input type="text" name="q" value="{{request('q')}}" class="form-control" placeholder="@lang('app.payer_email')">
                                    </div>
                                    <button type="submit" class="btn btn-default">@lang('app.search')</button>
                                </form>

                            </div> -->
                        </div>

                    </div>

                    @if($payments->count() > 0)
                        <table class="table table-striped table-bordered">

                            <tr>
                                <th>@lang('app.campaign_title')</th>
                                <th>@lang('app.amount')</th>
                                <th>@lang('app.method')</th>
                                <th>Covenant</th>
                                <th>Response</th>
                                <th>@lang('app.time')</th>
                                <th>#</th>
                                <th>#</th>
                                <th>Cause Reply</th>
                                {{-- <th>Status</th> --}}
                                <th>@lang('app.approveresponse')</th>
                                
                            </tr>

                            @foreach($payments as $payment)
                                <tr>
                                    <td>
                                        @if($payment->campaign)
                                            <a href="{{route('payment_view', $payment->id)}}">{{$payment->campaign->title}}</a>
                                        @else
                                            @lang('app.campaign_deleted')
                                        @endif
                                    </td>
                                    {{--<td><a href="{{route('payment_view', $payment->id)}}"> {{$payment->email}} </a></td>--}}

                                    <td>
                                        {{get_amount($payment->amount)}} 
                                        <?php 
                                            if(!empty($payment->top_up)) echo' + '.get_amount($payment->top_up);
                                            if($payment->annuity_status == 1) echo' <font color="green">Annuity Payment</font>';
                                        ?> 
                                    </td>

                                    <td>{{$payment->payment_method}}</td>
                                    <td>{{ $payment->convenant }}</td>

                                    @if ($payment->causeresponse != NULL)
                                        <td>{{ $payment->causeresponse }}</td>
                                    @else
                                        <td></td>
                                    @endif
                                    
                                    <td><span data-toggle="tooltip" title="{{$payment->created_at->format('F d, Y h:i a')}}">{{$payment->created_at->format('F d, Y')}}</span></td>

                                    <td>
                                        @if($payment->reward)
                                            <a href="{{route('payment_view', $payment->id)}}" data-toggle="tooltip" title="@lang('app.selected_reward')">
                                                <i class="fa fa-gift"></i>&nbsp;
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($payment->status == 'success')
                                            <span class="text-success" data-toggle="tooltip" title="{{$payment->status}}"><i class="fa fa-check-circle-o"></i> </span>&nbsp;
                                        @else
                                            <span class="text-warning" data-toggle="tooltip" title="{{$payment->status}}"><i class="fa fa-exclamation-circle"></i> </span>&nbsp;
                                        @endif

                                        <a href="{{route('payment_view', $payment->id)}}"><i class="fa fa-eye"></i> </a>&nbsp;

                                        
                                    </td>
                                    <td>
                                        @if ($payment->convenant != null)
                                             <a href="javascript:;" class="fa fa-edit" data-toggle="modal" data-target="#{{$payment->id}}-response-modal"></a>
                                        @endif
                                       
                                    </td>
                                   {{--  @if ($payment->responsestatus == 1)
                                        <td>Challenge accepted</td>
                                        @else
                                        <td>Not Accepted</td>
                                    @endif --}}
                                    
                                    <td>
                                        @if ($payment->causeresponse != null)
                                        <a href="javascript:;" class="fa fa-eye" data-toggle="modal" data-target="#{{$payment->id}}-accept-modal"></a>
                                        {{-- <a href="{{route('payment_view', $payment->id)}}"><i class="fa fa-eye"></i> </a> --}}
                                        @endif
                                    </td>
                                    
                                </tr>
                            @endforeach

                        </table>

                        {!! $payments->links() !!}

                    @else
                        @lang('app.no_data')
                    @endif

                </div>
                </div>

                @foreach ($payments as $payment)
                    <div class="modal fade" id="{{$payment->id}}-response-modal" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{-- <h4 class="modal-title" id="myModalLabel">@lang('app.your_withdraw_amount') <strong>{{get_amount($campaign->amount_raised()->campaign_owner_commission)}}</strong> </h4> --}}
                                    </div>
                                    <div class="modal-body">
                                        {{-- <form action="{{ url('covenantresponse') }}" method="post"> --}}
                                        {!! Form::open([ 'route' => 'covenant.response', 'class' => 'form-horizontal']) !!}
                                        <div class="form-group">
                                            <p style="background-color: #D3D3D3; padding: 5px 5px 5px 5px;">{{ $payment->convenant }}</p>
                                        </div>
                                        <div class="form-group">
                                             <textarea placeholder="" rows="6" id="response" class="form-control" name="response" maxlength="200"></textarea>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row">
                                            <div class="col">
                                                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                            </div>

                                            <div class="col">
                                               
                                                <input type="hidden" name="response_campaign_id" value="{{$payment->id}}" />
                                                <button type="submit" class="btn btn-orange">Update</button>
                                                {!! Form::close() !!}
                                                {{-- </form> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                @endforeach

                @foreach ($payments as $payment)
                    <div class="modal fade" id="{{$payment->id}}-accept-modal" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{-- <h4 class="modal-title" id="myModalLabel">@lang('app.your_withdraw_amount') <strong>{{get_amount($campaign->amount_raised()->campaign_owner_commission)}}</strong> </h4> --}}
                                    </div>
                                    <div class="modal-body">
                                        {{-- <form action="{{ url('covenantresponse') }}" method="post"> --}}
                                        {!! Form::open([ 'route' => 'covenant.response.reply', 'class' => 'form-horizontal']) !!}
                                        <div class="form-group">
                                            <p style="background-color: #D3D3D3; padding: 5px 5px 5px 5px;">{{ $payment->convenant }}</p>
                                            <label>Challenge</label>
                                        </div>
                                        <div class="form-group">
                                            <p style="background-color: #D3D3D3; padding: 5px 5px 5px 5px;">{{ $payment->causeresponse }}</p>
                                            <label>Response</label>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <div class="row">
                                            <div class="col">
                                                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                            </div>

                                            <div class="col">
                                               
                                                <input type="hidden" name="response_campaign_id" value="{{$payment->id}}" />
                                                <button type="submit" class="btn btn-orange">Accept</button>
                                                {!! Form::close() !!}
                                                {{-- </form> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                @endforeach
            </div>
        </div>
    </div>

    <script>

    $(document).ready(function() {
        $('#payment').DataTable();

        document.getElementById("foo").onchange = function() {
            if (this.selectedIndex!==0) {
                window.location.href = this.value;
            }        
        };
    });

</script>

@endsection