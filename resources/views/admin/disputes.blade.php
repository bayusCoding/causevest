@extends('layouts.admin')

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

<style>
    .card{
        padding: 20px;
        padding-left: 10px;
    }

    .row.card {
        margin-left: auto;
    }
</style>

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>
    <div class="dashboard-wrap font">
        <div class="container">
            <div style="margin-top: 40px" id="wrapper" class="row">

                @include('admin.sidebar')

                <div class="col-md-9">
                    <div id="page-wrapper" style="margin-top: 40px">
                        <h2>Dispute</h2>

                        @include('admin.flash_msg')

                        @if(!$user->is_admin())
                        {{ Form::open(['route'=> 'dispute', 'class' => 'form-horizontal', 'files' => true]) }}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="subject"> Subject </label>
                                        <input type="text" name="subject" required class="form-control" placeholder="Subject" aria-label="Subject">
                                    </div>

                                    <div class="form-group">
                                        <label for="subject"> Body </label>
                                        <textarea name="body" rows="6" class="form-control" aria-label="Body"></textarea>
                                    </div>

                                    <input type="hidden" value="{{$user->id}}" name="user_id">

                                    
                                    <div class="form-imput">
                                        <button type="submit" class="btn btn-primary btn-lg col-md-4 pull-left btn-orange mx-auto d-block">Send</button>
                                    </div>
                                </div>
                            </div>
                        {{ Form::close() }}
                        @endif

                        <div class="row">
                            <div class="col-md-12">
                                @if($disputes->count() > 0)
                                <table class="table table-striped table-bordered">
                                    <tr>
                                        <th>Subject</th>
                                        <th>Body</th>
                                        <th>Reply</th>
                                    </tr>

                                    @foreach($disputes as $dispute)
                                        
                                        <tr>
                                            <td>{{$dispute->subject}}</td>
                                            <td>{{$dispute->body}}</td>
                                            <td> <a class="btn btn-orange" href="{{route('reply_dispute', $dispute->id)}}"> reply </a> </td>
                                        </tr>
                                        
                                    @endforeach
                                </table>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
    $('.nav-item').click((e) => {
        element = $(e.target).attr('id');
        console.log(element);
        //$('.'+element).show();
    })
</script>
@endsection