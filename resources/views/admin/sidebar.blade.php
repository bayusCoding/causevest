
        <div class="col-md-3" style="padding:10px; font-size:14">
            <div class="row">
                <div class="col-xs-12 col-md-10 user-card card">
                    <div class="row">
                        <div class="card-header col-md-12">
                            <div class="campaign-profile user-profile image-center vertical-center">
                                <div class="image-profile">
                                    <img src="{{ $user->get_gravatar(100) }}">
                                </div>
                                <div class="profile-text">
                                    <h4>{{ $user->name }}</h4>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card-list row-card-body">
                            <div>
                                <h4 class="text-left">
                                <a href="{{route('my_campaigns')}}">Campaigns</a>
                                </h4>
                            </div>
                            <div>
                                <h4 class="text-right">
                                    {{$userCampaignCount}}
                                </h4>
                            </div>
                        </div>

                        <div class="card-list row-card-body">
                            <div>
                                <h4 class="text-left">
                                <a href="{{route('view-donations')}}">Donations</a>
                                </h4>
                            </div>
                            <div>
                                <h4 class="text-right">
                                {{$donation->count()}}
                                </h4>
                            </div>
                        </div>

                       {{--   <div class="card-list row-card-body">
                            <div>
                                <h4 class="text-left">
                                <a href="{{route('view-donations_convenant')}}">Convenant Donations</a>
                                </h4>
                            </div>
                            <div>
                                <h4 class="text-right">
                                {{$donation->count()}}
                                </h4>
                            </div>
                        </div> --}}

                        {{-- <div class="card-list row-card-body">
                            <div>
                                <h4 class="text-left">
                                <a href="#" >Following</a>
                                </h4>
                            </div>
                            <div>
                                <h4 class="text-right">
                                    3
                                </h4>
                            </div>
                        </div> --}}

                        <div class="card-list row-card-body">
                            <div>
                                <h4 class="text-left">
                                    <a href="{{route('disputes')}}" >Disputes</a>
                                </h4>
                            </div>
                            <div>
                                <h4 class="text-right">
                                    {{$disputes->count()}}
                                </h4>
                            </div>
                        </div>
                        <div class="btn-wrapper">
                            <a href="{{route('start_campaign')}}" class="btn btn-primary btn-lg col-md-10 btn-orange mx-auto d-block" role="button" aria-label="ask a question">Start A Campaign</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>