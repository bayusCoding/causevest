@extends('layouts.admin')

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="{{ route('dashboard') }}" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('my_campaigns')}}" class="nav-link">My Campaigns</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('payments')}}" class="nav-link">Payments</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('withdraw')}}" class="nav-link">Withdrawals</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('profile')}}" class="nav-link">Profile</a>
                    </li>
                    <li class="nav-item"> 
                        <a href="{{route('change_password')}}" class="nav-link"> @lang('app.change_password')</a> 
                    </li>
                    @if($auth_user->is_admin())
                        @include('admin.menu')                    
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>

    <div class="dashboard-wrap">
        <div class="container">
            <div style="margin-top: 40px" id="wrapper" class="row">

                <div class="col-md-3" style="padding:10px; font-size:14">
                    <div class="row">
                            <div class="col-xs-12 col-md-10 user-card card">
                                <div class="row">
                                    <div class="card-header col-md-12">
                                        <div class="campaign-profile user-profile image-center vertical-center">
                                            <div class="image-profile">
                                                <img src="{{ $user->get_gravatar(100) }}">
                                            </div>
                                            <div class="profile-text">
                                                <h4>{{ $user->name }}</h4>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                            <a href="{{route('my_campaigns')}}">Campaigns</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                {{$userCampaignCount}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                            <a href="{{route('view-donations')}}">Donations</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                            {{$payments->count()}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                Following
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                3
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="btn-wrapper">
                                        <a href="{{route('start_campaign')}}" class="btn btn-primary btn-lg col-md-10 btn-orange mx-auto d-block" role="button" aria-label="ask a question">Start A Campaign</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                </div>

                <div class="col-md-9">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header"> {{ $title }}  </h1>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')


                    <div class="row">
                        <div class="col-xs-12">

                            {!! Form::open(['class'=>'form-horizontal', 'files'=>'true']) !!}

                            <div class="form-group  {{ $errors->has('logo')? 'has-error':'' }}">
                                <label class="col-sm-4 control-label">@lang('app.site_logo')</label>
                                <div class="col-sm-8">

                                    @if(logo_url())
                                        <img src="{{ logo_url() }}" />
                                    @endif


                                    <input type="file" id="logo" name="logo" class="filestyle" >
                                    {!! $errors->has('logo')? '<p class="help-block">'.$errors->first('logo').'</p>':'' !!}
                                </div>
                            </div>


                            <hr />

                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-4">
                                    <button type="submit" class="btn btn-primary">@lang('app.edit')</button>
                                </div>
                            </div>

                            {!! Form::close() !!}

                        </div>
                    </div>

                </div>   <!-- /#page-wrapper -->

            </div>   <!-- /#wrapper -->
            </div>

        </div> <!-- /#container -->
    </div> <!-- /#dashboard wrap -->
@endsection

@section('page-js')


@endsection