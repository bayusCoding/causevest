@extends('layouts.admin')

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    
                        @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>
    <div class="dashboard-wrap font">
        <div class="container">
            <div style="margin-top: 60px" id="wrapper" class="row">

                <div class="col-md-3" style="padding:10px; font-size:14">
                {{-- @include('admin.menu') --}}
                    <div class="row">
                            <div class="col-xs-12 col-md-10 user-card card">
                                <div class="row">
                                    <div class="card-header col-md-12">
                                        <div class="campaign-profile user-profile image-center vertical-center">
                                            <div class="image-profile">
                                                <img src="{{ $userr->get_gravatar(100) }}">
                                            </div>
                                            <div class="profile-text">
                                                <h4>{{ $userr->name }}</h4>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                <a href="{{route('my_campaigns')}}">Campaigns</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                {{$userCampaignCount}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                <a href="{{route('view-donations')}}">Donations</a> 
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                {{$payments->count()}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                Following
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                3
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                <a href="{{route('disputes')}}" >Disputes</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                0
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="btn-wrapper">
                                        <a href="{{route('start_campaign')}}" class="btn btn-primary btn-lg col-md-10 btn-orange mx-auto d-block" role="button" aria-label="ask a question">Start A Campaign</a>
                                    </div>

                                </div>
                            </div>
                    </div>
                </div>


                <div class="col-md-9">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header"> {{ $title }}  </h1>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="profile-avatar">
                                <img src="{{ $user->get_gravatar(100) }}" class="img-thumbnail img-circle" width="100" />
                            </div>

                            <table style="width:50vw; padding:10px" class="table table-bordered table-striped">

                                <tr>
                                    <th>@lang('app.name')</th>
                                    <td>{{ $user->name }}</td>
                                </tr>

                                <tr>
                                    <th>@lang('app.email')</th>
                                    <td>{{ $user->email }}</td>
                                </tr>
                                <tr>
                                    <th>@lang('app.gender')</th>
                                    <td>{{ ucfirst($user->gender) }}</td>
                                </tr>
                                <tr>
                                    <th>@lang('app.phone')</th>
                                    <td>{{ $user->phone }}</td>
                                </tr>
                                <tr>
                                    <th>@lang('app.address')</th>
                                    <td>{{ $user->address }}</td>
                                </tr>
                                <tr>
                                    <th>@lang('app.country')</th>
                                    <td>
                                        @if($user->country)
                                            {{ $user->country->name }}
                                        @endif
                                    </td>
                                </tr>

                                <tr>
                                    <th>@lang('app.created_at')</th>
                                    <td>{{ $user->signed_up_datetime() }}</td>
                                </tr>
                                <tr>
                                    <th>@lang('app.status')</th>
                                    <td>{{ $user->status_context() }}</td>
                                </tr>
                                <tr>
                                    <th>@lang('app.contributed')</th>
                                    <td>
                                        @php $total_contributed = $user->contributed_amount(); @endphp
                                        @if($total_contributed > 0)
                                            {{get_amount($total_contributed)}}
                                        @endif
                                    </td>
                                </tr>

                                <tr>
                                    <th>User Role</th>
                                    <td>
                                        {{ $user->user_type }}

                                        @if($user->user_type == 'user')
                                            <p class="text-muted">Benefits of upgrading to external operator </p>

                                            <p> <a class="btn btn-light" data-toggle="modal" data-target="#pop">Upgrade</a> </p>

                                            <div class="modal fade" id="pop" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h4>Are you sure you want to Upgrade your Account<h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                            <a href="{{ route('user_upgrade', $user->id) }}" class="btn btn-orange">Upgrade</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        @endif
                                    </td>
                                </tr>
                            </table>

                            @if( ! empty($is_user_id_view))
                                <a href="{{route('users_edit', $user->id)}}"><i class="fa fa-pencil-square-o"></i> @lang('app.edit') </a>
                            @else
                                <a class="btn btn-orange" href="{{ route('profile_edit') }}"><i class="fa fa-pencil-square-o"></i> @lang('app.edit') </a>
                            @endif

                        </div>
                    </div>


                </div>   <!-- /#page-wrapper -->


                </div>

            </div>   <!-- /#wrapper -->


        </div> <!-- /#container -->
    </div> <!-- /#dashboard wrap -->
@endsection

@section('page-js')

@endsection