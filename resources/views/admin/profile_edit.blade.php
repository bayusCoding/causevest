@extends('layouts.admin')
@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp
@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>
    <div class="dashboard-wrap font">
        <div class="container">
            <div style="margin-top: 50px" id="wrapper" class="row">

                <div class="col-md-3" style="padding:10px; font-size:14">
                        {{-- @include('admin.menu') --}}
                        <div class="row">
                            <div class="col-xs-12 col-md-10 user-card card">
                                <div class="row">
                                    <div class="card-header col-md-12">
                                        <div class="campaign-profile user-profile image-center vertical-center">
                                            <div class="image-profile">
                                                <img src="{{ $userr->get_gravatar(100) }}">
                                            </div>
                                            <div class="profile-text">
                                                <h4>{{ $userr->name }}</h4>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                <a href="{{route('my_campaigns')}}">Campaigns</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                {{$userCampaignCount}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                <a href="{{route('view-donations')}}">Donations</a> 
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                {{$payments->count()}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                Following
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                3
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                <a href="{{route('disputes')}}" >Disputes</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                0
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="btn-wrapper">
                                        <a href="{{route('start_campaign')}}" class="btn btn-primary btn-lg col-md-10 btn-orange mx-auto d-block" role="button" aria-label="ask a question">Start A Campaign</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                </div>

                <div class="col-md-9">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12">
                                {{--  --}}
                                @if($auth_user->is_admin() || $auth_user->is_operator() && $userr->name != $user->name)
                                    <h1 class="page-header">Editing the profile of {{ $user->name }}</h1>
                                @else
                                    <h1 class="page-header"> {{ $title }}  </h1>
                                @endif
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif


                    @include('admin.flash_msg')

                    <div class="row">
                        <!-- <div class="col-xs-12"> -->

                            {!! Form::open(['class'=>'form-horizontal', 'files'=>'true']) !!}
                            <div class="row">
                                <div class="col">
                                    <div class="form-group {{ $errors->has('name')? 'has-error':'' }}">
                                        <label for="name" class="control-label">@lang('app.name')</label>
                                            <input type="text" class="form-control" id="name" value="{{ old('name')? old('name') : $user->name }}" name="name" placeholder="@lang('app.name')">
                                            {!! $errors->has('name')? '<p class="help-block">'.$errors->first('name').'</p>':'' !!}
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group {{ $errors->has('email')? 'has-error':'' }}">
                                        <label for="email" class="control-label">@lang('app.email')</label>
                                            <input type="email" class="form-control" id="email" value="{{ old('email')? old('email') : $user->email }}" name="email" placeholder="@lang('app.email')">
                                            {!! $errors->has('email')? '<p class="help-block">'.$errors->first('email').'</p>':'' !!}
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group {{ $errors->has('gender')? 'has-error':'' }}">
                                        <label for="gender" class="control-label">@lang('app.gender')</label>
                                            <select id="gender" name="gender" class="form-control select2">
                                                <option value="">Select Gender</option>
                                                <option value="male" {{ $user->gender == 'male'?'selected':'' }}>Male</option>
                                                <option value="female" {{ $user->gender == 'female'?'selected':'' }}>Female</option>
                                                {{-- <option value="third_gender" {{ $user->gender == 'third_gender'?'selected':'' }}>Third Gender</option> --}}
                                            </select>

                                            {!! $errors->has('gender')? '<p class="help-block">'.$errors->first('gender').'</p>':'' !!}
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group {{ $errors->has('phone')? 'has-error':'' }}">
                                        <label for="phone" class="control-label">@lang('app.phone')</label>
                                            <input type="text" class="form-control" id="phone" value="{{ old('phone')? old('phone') : $user->phone }}" name="phone" placeholder="@lang('app.phone')">
                                            {!! $errors->has('phone')? '<p class="help-block">'.$errors->first('phone').'</p>':'' !!}
                                    </div>
                                </div>
                            </div>

                            
                            <div class="row">
                                <div class="col">
                                    <div class="form-group {{ $errors->has('country_id')? 'has-error':'' }}">
                                    <label for="phone" class="control-label">@lang('app.country')</label>
                                        <select id="country_id" name="country_id" class="form-control select2">
                                            <option value="">@lang('app.select_a_country')</option>
                                            @foreach($countries as $country)
                                                <option value="{{ $country->id }}" {{ $user->country_id == $country->id ? 'selected' :'' }}>{{ $country->name }}</option>
                                            @endforeach
                                        </select>
                                        {!! $errors->has('country_id')? '<p class="help-block">'.$errors->first('country_id').'</p>':'' !!}                                        </div>
                                </div>

                                <div class="col">
                                    <div class="form-group {{ $errors->has('address')? 'has-error':'' }}">
                                        <label for="address" class="control-label">@lang('app.address')</label>
                                            <input type="text" class="form-control" id="address" value="{{ old('address')? old('address') : $user->address }}" name="address" placeholder="@lang('app.address')">
                                            {!! $errors->has('address')? '<p class="help-block">'.$errors->first('address').'</p>':'' !!}
                                    </div>
                                </div>
                            </div>
                            

                            

                            <div class="form-group  {{ $errors->has('photo')? 'has-error':'' }}">
                                <label class="col-sm-4 control-label">@lang('app.change_avatar')</label>
                                <div class="col-sm-8">
                                    <input type="file" id="photo" name="photo" class="filestyle" >
                                    {!! $errors->has('photo')? '<p class="help-block">'.$errors->first('photo').'</p>':'' !!}
                                </div>
                            </div>

                            <hr />

                            <div class="form-group">
                                <div class="col-sm-3 col-sm-offset-4">
                                    <button type="submit" class="btn btn-primary btn-lg col-md-10 btn-orange mx-auto d-block">@lang('app.edit')</button>
                                </div>
                            </div>

                            {!! Form::close() !!}

                        <!-- </div> -->
                    </div>

                </div>   <!-- /#page-wrapper -->
                </div>
            </div>   <!-- /#wrapper -->


        </div> <!-- /#container -->
    </div> <!-- /#dashboard wrap -->
@endsection

@section('page-js')


@endsection