@extends('layouts.admin')

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('content')
<style>
a.btn.btn-primary.btn-lg.btn-light.btn-block {
    width: auto;
    margin-left: 10px;
    padding: .77rem;
    font-size: 12px;
    word-wrap: break-word;
    float: right;
}

.table td, .table th {
    padding: .4rem;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
}
</style>
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>

    <div class="dashboard-wrap font">
        <div class="container">
            <div style="margin-top: 50px" id="wrapper" class="row">

                @include('admin.sidebar')

                <div class="col-md-9 font">
                <div id="page-wrapper">

                    @if( ! empty($title))
                        <div class="row">
                            <div class="col">
                                <h1 class="page-header">
                                    {{ $title }}
                                </h1>
                                
                            </div>
                            <div class="mb-3">
                            @if($auth_user->is_admin())
                                <a class="mt-3 btn btn-primary btn-lg btn-light btn-block" href="{{route('view-annuity')}}">Annuity Payments</a>
                                <a class="mt-3 btn btn-primary btn-lg btn-light btn-block" href="{{route('payments_pending')}}">@lang('app.pending_payments')</a>
                            @endif
                                <a class="mt-3 btn btn-primary btn-lg btn-light btn-block" href="{{route('view-donations')}}">View your Donations</a>
                                
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')


                    <div class="admin-campaign-lists">

                        <div class="row">
                            <div class="col-md-5">
                                @lang('app.total') : {{$payments->count()}}
                            </div>

                            <!-- <div class="col-md-7">

                                <form class="form-inline" method="get" action="">
                                    <div class="form-group">
                                        <input type="text" name="q" value="{{request('q')}}" class="form-control" placeholder="@lang('app.payer_email')">
                                    </div>
                                    <button type="submit" class="btn btn-default">@lang('app.search')</button>
                                </form>

                            </div> -->
                        </div>

                    </div>

                    @if($convenant->count() > 0)
                        <table class="table table-striped table-bordered">

                            <tr>
                                <th>@lang('app.name')</th>
                                <th>@lang('app.campaign_title')</th>
                                <th>@lang('app.time')</th>
                                <th>#</th>
                                <th>#</th>
                            </tr>

                            @foreach($payments as $payment)
                                <tr>
                                    <td>{{ $payment->user->name }}</td>
                                    <td>
                                        @if($payment->campaign)
                                            <a href="{{route('payment_view', $payment->id)}}">{{$payment->campaign->title}}</a>
                                        @else
                                            @lang('app.campaign_deleted')
                                        @endif
                                    </td>
                                    <td><span data-toggle="tooltip" title="{{$payment->created_at->format('F d, Y h:i a')}}">{{$payment->created_at->format('F d, Y')}}</span></td>

                                    <td>
                                        @if($payment->reward)
                                            <a href="{{route('payment_view', $payment->id)}}" data-toggle="tooltip" title="@lang('app.selected_reward')">
                                                <i class="fa fa-gift"></i>&nbsp;
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($payment->status == 'success')
                                            <span class="text-success" data-toggle="tooltip" title="{{$payment->status}}"><i class="fa fa-check-circle-o"></i> </span>&nbsp;
                                        @else
                                            <span class="text-warning" data-toggle="tooltip" title="{{$payment->status}}"><i class="fa fa-exclamation-circle"></i> </span>&nbsp;
                                        @endif

                                        <a href="{{route('payment_view', $payment->id)}}"><i class="fa fa-eye"></i> </a>&nbsp;

                                        <a href="{{route('payment_view', $payment->id)}}"><i class="fa fa-edit"></i> </a>
                                    </td>
                                </tr>
                            @endforeach

                        </table>

                        {!! $payments->links() !!}

                    @else
                        @lang('app.no_data')
                    @endif

                </div>
                </div>
            </div>
        </div>
    </div>

@endsection