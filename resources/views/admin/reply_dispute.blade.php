@extends('layouts.admin')

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

<style>
    .card{
        padding: 20px;
        padding-left: 10px;
    }

    .black{
        color: black;
    }

    .row.card {
        margin-left: auto;
    }

    .line{
        line-height: 0px;
    }

    .weight {
        font-weight: 100;
    }
</style>

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>
    <div class="dashboard-wrap font">
        <div class="container">
            <div style="margin-top: 40px" id="wrapper" class="row">

                @include('admin.sidebar')

                <div class="col-md-9">
                    <div id="page-wrapper" style="margin-top: 40px">
                        <h2>Reply</h2><br>
                        
                        <div class="row">
                            <div class="col-md-12">
                                <h3 class="line weight">{{$dispute->subject}}</h3>
                                <h5>{{$dispute->body}}</h5>
                                <span style="font-size:10px" class="text-muted">{{$dispute->created_at->diffForHumans()}}<span>

                                @if($replies->count() > 0)
                                    <table class="table table-striped table-bordered">
                                        <tr>
                                            <th> Replies </th>
                                        </tr>
                                        
                                        @foreach($replies as $reply)
                                            <tr>
                                                <td>
                                                    <p><b>{{$user->getUserName($reply->user_id)}}</b><p>
                                                    {{$reply->body}} <br>
                                                    <small class="text-muted">{{$reply->created_at->diffForHumans()}}<small>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                @endif

                                {{ Form::open(['route'=> 'reply_dispute_post', 'class' => 'form-horizontal', 'files' => true]) }}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="subject" class="font black"> <b>Body</b> </label>
                                                <textarea name="body" rows="6" class="form-control" aria-label="Body"></textarea>
                                            </div>

                                            <input type="hidden" value="{{$parent_id}}" name="parent_id">

                                            <input type="hidden" value="{{$user->id}}" name="user_id">

                                            
                                            <div class="form-imput">
                                                <button style="font-size:13px" type="submit" class="btn btn-primary btn-lg col-md-4 pull-left btn-orange mx-auto d-block">Send</button>
                                            </div>
                                        </div>
                                    </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
    $('.nav-item').click((e) => {
        element = $(e.target).attr('id');
        console.log(element);
        //$('.'+element).show();
    })
</script>
@endsection