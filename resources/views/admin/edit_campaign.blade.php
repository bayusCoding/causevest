@extends('layouts.admin')

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('page-css')
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/summernote/summernote.css')}}">
@endsection

<style>
    .alertt{
        background-color:#100c0114 !important;
        border-color:#100c0114 !important;
        color: #000 !important;
    }
    .text-info{
        color: #125cd5 !important;
    }
</style>

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>
    <div class="dashboard-wrap">
        <div class="container">
            <div id="wrapper" class="row" style="margin-top:60px">
                {{-- <div class="col-md-3">
                    
                    <div class="row">
                            <div class="col-xs-12 col-md-10 user-card card">
                                <div class="row">
                                    <div class="card-header col-md-12">
                                        <div class="campaign-profile user-profile image-center vertical-center">
                                            <div class="image-profile">
                                                <img src="{{ Auth::user()->get_gravatar(100) }}">
                                            </div>
                                            <div class="profile-text">
                                                <h4>{{ Auth::user()->name }}</h4>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                Campaigns
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                {{$userCampaignCount}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                Donations
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                {{$donation->count()}}
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                Following
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                3
                                            </h4>
                                        </div>
                                    </div>

                                    <div class="card-list row-card-body">
                                        <div>
                                            <h4 class="text-left">
                                                <a href="{{route('disputes')}}" >Disputes</a>
                                            </h4>
                                        </div>
                                        <div>
                                            <h4 class="text-right">
                                                0
                                            </h4>
                                        </div>
                                    </div>
                                    
                                    <div class="btn-wrapper">
                                        <a href="{{route('start_campaign')}}" class="btn btn-primary btn-lg col-md-10 btn-orange mx-auto d-block" role="button" aria-label="ask a question">Start A Campaign</a>
                                    </div>

                                </div>
                            </div>
                    </div>
                </div> --}}
                @include('admin.sidebar')
                
                <div class="col-md-9">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header"> {{ $title }}  </h1>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')

                    <div class="row">
                        <div class="col-md-10 col-xs-12">

                            <div class="alert alert-info">
                                <h5> <i class="fa fa-money"></i> @lang('app.you_will_get') {{$campaign->campaign_owner_commission}}% @lang('app.of_total_raised')</h5>
                            </div>


                            <ul class="campaign-update-nav">
                                <li><a href="{{route('edit_campaign_rewards', $campaign->id)}}"> @lang('app.rewards') ({{$campaign->rewards->count()}})</a> </li>
                                <li><a href="{{route('edit_campaign_updates',  $campaign->id )}}"> @lang('app.updates') ({{$campaign->updates->count()}})</a> </li>
                                <li><a href="{{route('edit_campaign_faqs', $campaign->id)}}"> @lang('app.faq') ({{$campaign->faqs->count()}})</a> </li>
                            </ul>

                            {{ Form::open(['id'=>'startCampaignForm', 'class' => 'form-horizontal', 'files' => true]) }}
                            <legend>@lang('app.campaign_info')</legend>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group  {{ $errors->has('category')? 'has-error':'' }}">
                                        <label for="category" class="control-label">@lang('app.category') <span class="field-required">*</span></label>
                                            <select class="form-control select2" name="category">
                                                <option value="">@lang('app.select_a_category')</option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}" @if($campaign->category_id == $category->id) selected="selected" @endif >{{ $category->category_name }}</option>
                                                @endforeach
                                            </select>
                                            {!! $errors->has('category')? '<p class="help-block">'.$errors->first('category').'</p>':'' !!}
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group {{ $errors->has('title')? 'has-error':'' }}">
                                        <label for="title" class="control-label">@lang('app.title') <span class="field-required">*</span></label>
                                            <input type="text" class="form-control" id="title" value="{{ $campaign->title }}" name="title" placeholder="@lang('app.title')">
                                            {!! $errors->has('title')? '<p class="help-block">'.$errors->first('title').'</p>':'' !!}
                                            <p class="text-info"> @lang('app.great_title_info')</p>
                                    </div>
                                </div>
                            </div>
                            

                            <div class="form-group {{ $errors->has('short_description')? 'has-error':'' }}">
                                <label for="short_description" class="ccontrol-label">@lang('app.short_description')</label>
                                    <textarea name="short_description" class="form-control" rows="3">{{$campaign->short_description}}</textarea>
                                    {!! $errors->has('short_description')? '<p class="help-block">'.$errors->first('short_description').'</p>':'' !!}
                            </div>

                            <div class="form-group {{ $errors->has('description')? 'has-error':'' }}">
                                <label for="description" class="control-label">@lang('app.description') <span class="field-required">*</span></label>
                                <div class="">
                                    <div class="alert alert-info"> @lang('app.image_insert_limitation') </div>
                                </div>
                                <div class="">
                                    <textarea name="description" class="form-control description" rows="8">{{ $campaign->description}}</textarea>
                                    {!! $errors->has('description')? '<p class="help-block">'.$errors->first('description').'</p>':'' !!}
                                    <p class="text-info"> @lang('app.description_info_text')</p>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('goal')? 'has-error':'' }}">
                                <label for="goal" class="control-label">@lang('app.goal') <span class="field-required">*</span></label>
                                    <input type="number" class="form-control" id="goal" value="{{ $campaign->goal }}" name="goal" placeholder="@lang('app.goal')">
                                    {!! $errors->has('goal')? '<p class="help-block">'.$errors->first('goal').'</p>':'' !!}
                            </div>
                            
                            {{--
                            --}}

                            <div class="row">
                                <div class="col">
                                    <div class="form-group {{ $errors->has('min_amount')? 'has-error':'' }}">
                                        <label for="min_amount" class="control-label">@lang('app.min_amount')</label>
                                            <input type="number" class="form-control" id="min_amount" value="{{ $campaign->min_amount }}" name="min_amount" placeholder="@lang('app.min_amount')">
                                            {!! $errors->has('min_amount')? '<p class="help-block">'.$errors->first('min_amount').'</p>':'' !!}
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group {{ $errors->has('max_amount')? 'has-error':'' }}">
                                        <label for="max_amount" class="control-label">@lang('app.max_amount')</label>
                                            <input type="number" class="form-control" id="max_amount" value="{{ $campaign->max_amount }}" name="max_amount" placeholder="@lang('app.max_amount')">
                                            {!! $errors->has('max_amount')? '<p class="help-block">'.$errors->first('max_amount').'</p>':'' !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group {{ $errors->has('recommended_amount')? 'has-error':'' }}">
                                        <label for="recommended_amount" class="control-label">@lang('app.recommended_amount')</label>
                                            <input type="number" class="form-control" id="recommended_amount" value="{{ $campaign->recommended_amount}}" name="recommended_amount" placeholder="@lang('app.recommended_amount')">
                                            {!! $errors->has('recommended_amount')? '<p class="help-block">'.$errors->first('recommended_amount').'</p>':'' !!}
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group {{ $errors->has('amount_prefilled')? 'has-error':'' }}">
                                        <label for="amount_prefilled" class="control-label">@lang('app.amount_prefilled')</label>
                                            <input type="text" class="form-control" id="amount_prefilled" value="{{ $campaign->amount_prefilled}}" name="amount_prefilled" placeholder="@lang('app.amount_prefilled')">
                                            {!! $errors->has('amount_prefilled')? '<p class="help-block">'.$errors->first('amount_prefilled').'</p>':'' !!}
                                            <p class="text-info"> @lang('app.amount_prefilled_info_text')</p>
                                    </div>
                                </div>
                            </div>

                            

                            

                            <div class="form-group {{ $errors->has('end_method')? 'has-error':'' }}">
                                <label for="end_method" class="control-label">@lang('app.campaign_end_method')</label>
                                    <div class="row">
                                        <div class="col">
                                            <label>
                                                <input type="radio" name="end_method"  value="goal_achieve" @if($campaign->end_method == 'goal_achieve') checked="checked" @endif > @lang('app.after_goal_achieve')
                                            </label>
                                        </div>

                                        <div class="col">
                                            <label>
                                                <input type="radio" name="end_method" value="end_date"  @if($campaign->end_method == 'end_date') checked="checked" @endif > @lang('app.after_end_date')
                                            </label>
                                        </div>

                                        <div class="col">
                                            <label>
                                                <input type="radio" name="end_method" value="both"  @if($campaign->end_method == 'perpetuity') checked="checked" @endif > Perpetuity
                                            </label>
                                        </div>
                                    </div>

                                    {!! $errors->has('end_method')? '<p class="help-block">'.$errors->first('end_method').'</p>':'' !!}

                                    <p class="text-info"> @lang('app.end_method_info_text')</p>
                            </div>


                            <div class="form-group">
                                <label for="end_method" style="text-align: center" class="control-label">Campaign Balance Option</label>
                                <div class="form-row">
                                    <div class="col-md-5">
                                        <label>
                                            <input type="radio" name="show_balance"  value="1" @if($campaign->show_balance == 1) checked="checked" @endif> Show My Campaign Balance
                                        </label>
                                    </div>
                                    <div class="col">
                                        <label>
                                            <input type="radio" name="show_balance" value="0" @if($campaign->show_balance == 0) checked="checked" @endif> Hide My Campaign Balance
                                        </label>
                                    </div>
                                </div>
                            </div>

                            
                            <div class="row">
                                <div class="col">
                                    <div class="form-group {{ $errors->has('video')? 'has-error':'' }}">
                                        <label for="video" class="control-label">@lang('app.video')</label>
                                            <input type="text" class="form-control" id="video" value="{{$campaign->video}}" name="video" placeholder="@lang('app.video')">
                                            {!! $errors->has('video')? '<p class="help-block">'.$errors->first('video').'</p>':'' !!}
                                            <p class="text-info"> @lang('app.video_info_text')</p>
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group  {{ $errors->has('country_id')? 'has-error':'' }}">
                                        <label for="country_id" class="control-label">@lang('app.country')<span class="field-required">*</span></label>
                                            <select class="form-control select2" name="country_id">

                                                <option value="">@lang('app.select_a_country')</option>

                                                @foreach($countries as $country)
                                                    <option value="{{$country->id}}" @if($campaign->country_id == $country->id) selected="selected" @endif >{{$country->name}}</option>
                                                @endforeach

                                            </select>
                                            {!! $errors->has('country_id')? '<p class="help-block">'.$errors->first('country_id').'</p>':'' !!}
                                    </div>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('address')? 'has-error':'' }}">
                                <label for="address" class="control-label">@lang('app.address')</label>
                                    <input type="text" class="form-control" id="address" value="{{ $campaign->address}}" name="address" placeholder="@lang('app.address')">
                                    {!! $errors->has('address')? '<p class="help-block">'.$errors->first('address').'</p>':'' !!}
                            </div>
                            
                            <div class="row">
                                <div class="col">
                                    <div class="form-group {{ $errors->has('start_date')? 'has-error':'' }}">
                                        <label for="start_date" class="control-label">@lang('app.start_date')</label>
                                            <input type="text" class="form-control" id="start_date" value="{{ $campaign->start_date}}" name="start_date" placeholder="@lang('app.start_date')">
                                            {!! $errors->has('start_date')? '<p class="help-block">'.$errors->first('start_date').'</p>':'' !!}
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group {{ $errors->has('end_date')? 'has-error':'' }}">
                                        <label for="end_date" class="control-label">@lang('app.end_date')</label>
                                            <input type="text" class="form-control" id="end_date" value="{{ $campaign->end_date}}" name="end_date" placeholder="@lang('app.end_date')">
                                            {!! $errors->has('end_date')? '<p class="help-block">'.$errors->first('end_date').'</p>':'' !!}
                                    </div>
                                </div>
                            </div>

                            
                            <div class="form-group {{ $errors->has('feature_image')? 'has-error':'' }}">
                                <label for="end_date" class="col-sm-4 control-label">@lang('app.feature_image')</label>
                                    <div class="col-sm-8">
                                        <label for="feature_image" class="img_upload"><i class="fa fa-cloud-upload"></i> </label>
                                        <input type="file" id="feature_image" name="feature_image" style="display: none;" />
                                        <div id="feature_image_preview">@if($campaign->feature_image) <img src="{{ $campaign->feature_img_url()}}" /> @endif</div>
                                    </div>
                            </div>
                                
                            <div class="form-group">
                                <div class="col-sm-offset-4">
                                    <button type="submit" class="btn btn-primary btn-lg btn-orange btn">@lang('app.edit_campaign')</button>
                                </div>
                            </div>
                                
                            </div>

                            {{ Form::close() }}

                        </div>
                    </div>



                </div>
                </div>

            </div>
        </div>
    </div>


@endsection

@section('page-js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.min.js')}}"></script>

    <!-- <script src="{{asset('assets/plugins/summernote/summernote.js')}}"></script> -->
    <script>
        $(function () {
            $('#start_date, #end_date').datetimepicker({format: 'YYYY-MM-DD'});
        });

        $(document).ready(function() {
            $('.description').summernote({  height: 300});

            $('#feature_image').change(function(){

                var input = this;
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#feature_image_preview').html('<img src="'+e.target.result+'" />');
                    };
                    reader.readAsDataURL(input.files[0]);
                }

            });
        });

    </script>
@endsection
