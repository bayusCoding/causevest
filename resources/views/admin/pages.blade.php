@extends('layouts.admin')

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    <li class="nav-item">
                        <a href="{{ route('dashboard') }}" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('my_campaigns')}}" class="nav-link">My Campaigns</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('payments')}}" class="nav-link">Payments</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('withdraw')}}" class="nav-link">Withdrawals</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('profile')}}" class="nav-link">Profile</a>
                    </li>
                    <!-- <li class="nav-item"> 
                        <a href="{{route('change_password')}}" class="nav-link"> @lang('app.change_password')</a> 
                    </li> -->
                    @if($auth_user->is_admin() || $auth_user->is_operator())
                        @include('admin.menu')
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
    <div class="dashboard-wrap">
        <div class="container">
            <div id="wrapper">

                @include('admin.menu')

                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header"> {{ $title }}
                                    <a href="{{ route('create_new_page') }}" class="btn btn-info pull-right"> <i class="fa fa-floppy-o"></i> @lang('app.create_new_page')</a>
                                </h1>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')

                    <div class="row">
                        <div class="col-xs-12">
                            <table class="table table-bordered table-striped" id="jDataTable">
                                <tr>
                                    <th>@lang('app.title')</th>
                                    <th>@lang('app.created_at')</th>
                                    <th>@lang('app.actions')</th>
                                </tr>

                                @foreach($pages as $page)

                                    <tr>
                                        <td>{{$page->title}}</td>
                                        <td>{{$page->created_at_datetime()}}</td>
                                        <td>

                                            <a href="{{route('edit_page',$page->slug)}}" class="btn btn-primary"><i class="fa fa-edit"></i> </a><a href="javascript:;" class="btn btn-danger deletePage" data-slug="{{$page->slug}}"><i class="fa fa-trash"></i> </a>'

                                        </td>
                                    </tr>

                                @endforeach

                            </table>


                            {!! $pages->links() !!}
                        </div>
                    </div>

                </div>   <!-- /#page-wrapper -->

            </div>   <!-- /#wrapper -->


        </div> <!-- /#container -->
    </div> <!-- /#dashboard wrap -->
@endsection

@section('page-js')
    <script>
        $(document).ready(function() {
            $('body').on('click', '.btn-danger', function (e) {
                if (!confirm("<?php echo trans('app.are_you_sure') ?>")) {
                    e.preventDefault();
                    return false;
                }

                var selector = $(this);
                var slug = $(this).data('slug');

                $.ajax({
                    type: 'POST',
                    url: '{{ route('delete_page') }}',
                    data: {slug: slug, _token: '{{ csrf_token() }}'},
                    success: function (data) {
                        if (data.success == 1) {
                            selector.closest('tr').hide('slow');
                            var options = {closeButton: true};
                            toastr.success(data.msg, '<?php echo trans('app.success') ?>', options)
                        }
                    }
                });
            });
        });
    </script>

    <script>
        var options = {closeButton : true};
        @if(session('success'))
            toastr.success('{{ session('success') }}', '<?php echo trans('app.success') ?>', options);
        @endif
    </script>
@endsection


