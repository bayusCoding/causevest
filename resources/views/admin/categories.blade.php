@extends('layouts.admin')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

<style>
    .pagination>li>a, .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        /* color: #125cd5; */
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }
</style>

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>
    <div class="dashboard-wrap font">

        <div class="container">

            <div style="margin-top: 60px" id="wrapper" class="row">

                @include('admin.sidebar')

                <div class="col-md-9">
                <div id="page-wrapper">
                    @if( ! empty($title))
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header"> {{ $title }}  </h1>
                            </div> <!-- /.col-lg-12 -->
                        </div> <!-- /.row -->
                    @endif

                    @include('admin.flash_msg')

                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-1 col-xs-12">

                            {{ Form::open(['class' => 'form-horizontal', 'files' => true]) }}


                            <div class="form-group {{ $errors->has('category_name')? 'has-error':'' }}">
                                <label for="category_name" class="col-sm-4 control-label">@lang('app.category_name')</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="category_name" value="{{ old('category_name') }}" name="category_name" placeholder="@lang('app.category_name')">
                                    {!! $errors->has('category_name')? '<p class="help-block">'.$errors->first('category_name').'</p>':'' !!}

                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('image')? 'has-error':'' }}">
                                <label for="image" class="col-sm-4 control-label">@lang('app.image')</label>
                                <div class="col-sm-8">
                                    <input type="file" name="image" id="image" class="form-control">
                                    {!! $errors->has('image')? '<p class="help-block">'.$errors->first('image').'</p>':'' !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-4 col-sm-8">
                                    <button type="submit" class="btn btn-orange">@lang('app.save_new_category')</button>
                                </div>
                            </div>
                            {{ Form::close() }}

                        </div>

                    </div>

                        @if($categories->count())
                            <div class="row">
                                <div class="col-xs-12" style="margin-left:20px">
                                    <table class="table table-bordered categories-lists">
                                        <tr>
                                            <th>@lang('app.category_name') </th>
                                            <th>@lang('app.image') </th>
                                            <th>@lang('app.action') </th>
                                        </tr>
                                        @foreach($categories as $category)
                                            <tr>
                                                <td class="catWidth"> {{ $category->category_name }}  </td>
                                                <td> <img class="sizeCatImg" src="{{$category->get_image_url()}}" />  </td>
                                                <td>
                                                    <a style="margin-top:5px" href="{{ route('edit_categories', $category->id) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> </a>
                                                    <a style="margin-top:5px" href="javascript:;" class="btn btn-danger btn-xs" data-id="{{ $category->id }}"><i class="fa fa-trash"></i> </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                    <div class="container">
                                        {!! $categories->links() !!}
                                    </div>
                                </div>
                            </div>
                        @endif



                </div>   <!-- /#page-wrapper -->
                </div>



            </div>   <!-- /#wrapper -->


        </div> <!-- /#container -->

    </div>
@endsection

@section('page-js')
    <script>
        $(document).ready(function() {
            $('.btn-danger').on('click', function (e) {
                if (!confirm("Are you sure? its can't be undone")) {
                    e.preventDefault();
                    return false;
                }

                var selector = $(this);
                var data_id = $(this).data('id');

                $.ajax({
                    type: 'POST',
                    url: '{{ route('delete_categories') }}',
                    data: {data_id: data_id, _token: '{{ csrf_token() }}'},
                    success: function (data) {
                        if (data.success == 1) {
                            selector.closest('tr').hide('slow');
                        }
                    }
                });
            });
        });
    </script>
@endsection