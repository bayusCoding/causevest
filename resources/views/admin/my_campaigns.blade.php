@extends('layouts.admin')

@section('title') @if(! empty($title)) {{$title}} @endif - @parent @endsection

@php
$auth_user = \Illuminate\Support\Facades\Auth::user();
@endphp

@section('content')
<div class="header d-lg-flex p-0" id="headerMenuCollapse">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                    @include('admin.menu')
                </ul>
            </div>
        </div>
    </div>
</div>
<main role="main" class="dashboard font">
        <!-- section 1-->
        <section class="dashboard-entry">
            <div class="container">
                <div class="row">
                
                    @include('admin.sidebar')

                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-lg-11">

                                <div class="pr-5">
                                    <div class="left-holder">
                                        <div id="featured-switch-tabs" class="col-md-12 mt-4">
                                            <div class="row">
                                                <ul>
                                                    <li class="switch-tabs active">
                                                        <a href="#">
                                                            <span>Feed</span>
                                                        </a>
                                                    </li>
                                                    <li class="switch-tabs">
                                                        <a href="#">
                                                            <span>Donor Only Post</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-12 clearfix">
                                            <div class="row">
                                                <h4>Recent Campaigns</h4>
                                                <div class="campaigns-row">
                                                    <div class="row">
                                                        @foreach($campaigns as $camp)
                                                        <div class="col-xs-12 col-md-6">
                                                            <div class="card p-4">
                                                                <div class="image-card">
                                                                    <div class="like-container">
                                                                        <div class="like-cnt unchecked" id="like-cnt">
                                                                            <svg height="62" viewBox="0 0 54 52" width="64" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                                <filter id="a" height="116.9%" width="124.7%" x="-12.3%" y="-6.2%">
                                                                                    <feMorphology in="SourceAlpha" operator="dilate" radius="3" result="shadowSpreadOuter1"></feMorphology>
                                                                                    <feOffset dy="12" in="shadowSpreadOuter1" result="shadowOffsetOuter1"></feOffset>
                                                                                    <feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="10"></feGaussianBlur>
                                                                                    <feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.05 0"></feColorMatrix>
                                                                                    <feMerge>
                                                                                        <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
                                                                                        <feMergeNode in="SourceGraphic"></feMergeNode>
                                                                                    </feMerge>
                                                                                </filter>
                                                                                <path d="m25.8488318 14.7377048c-1.6528355-1.6496691-4.3332467-1.6503936-5.9868084-.001449-.0007263.0003623-.0010894.0010868-.0014525.001449l-.8158878.8139672-.8158878-.8139672c-1.6539249-1.6496691-4.3346992-1.6496691-5.988624 0-1.6535617 1.6496691-1.6535617 4.3244949 0 5.974164l.8162509.8139672 5.9882609 5.974164 5.9882609-5.974164.8158878-.8139672c1.6535618-1.6489446 1.654288-4.3230459.0014524-5.972715z"
                                                                                    fill="#fff" fill-rule="evenodd" filter="url(#a)"
                                                                                    stroke="#125cd5" transform="translate(-2.5 -4.5)"></path>
                                                                            </svg>
                                                                        </div>
                                                                    </div>
                                                                    <img class="card-img-top" src="{{ $camp->feature_img_url()}}" alt="campaign image" role="img">
                                                                    <div class="badge-post badge campaign-badge">
                                                                        <span class="badge-img">
                                                                            <img src="{{ asset('assets/img/badge-notice.svg') }}" alt="Badge Notice">
                                                                        </span>
                                                                        <span>{{$camp->get_category->category_name}}</span>
                                                                    </div>
                                                                </div>
                                                                <div class="progress">
                                                                    <div class="progress-bar" role="progressbar" style="width: {{ $camp->percent_raised() }}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                                </div>
                                                                <div class="card-body" style="padding-bottom: 0rem;">
                                                                    <div class="progress-title-wrapper">
                                                                        <h5 class="progress-title" aria-live="assertive">raised
                                                                            <span class="primary-color">{{get_amount($camp->success_payments->sum('amount'))}}</span> / {{ $camp->goal }}</h5>
                                                                        <h5 class="progress-title" aria-live="assertive">
                                                                            <span class="primary-color">{{ $camp->percent_raised() }}%</span>
                                                                        </h5>
                                                                    </div>
                                                                </div>
                                                                <div class="card-body">
                                                                    <h4 class="card-title" aria-live="assertive"><a href="{{route('campaign_single', [$camp->id, $camp->slug])}}">{{ $camp->title }}</a></h4>
                                                                    <p class="card-text" aria-live="assertive">{{ str_limit($camp->short_description, $limit = 70, $end = '...') }}</p>
                                                                </div>

                                                                <div class="row">
                                                                    <div class="col">
                                                                        <div class="btn-wrapper">
                                                                            <a href="{{route('edit_campaign', $camp->id)}}" class="btn btn-primary btn-sm btn-orange mx-auto" role="button" aria-label="ask a question">Edit</a>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col offset-sm-1">
                                                                        <div class="btn-wrapper">
                                                                            <a href="{{route('campaign_single', [$camp->id, $camp->slug])}}" class="btn btn-primary btn-sm btn-light mx-auto" role="button" aria-label="ask a question">View</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="campaign-details">
                                                                    <a class="card-text donors-total" aria-live="assertive">
                                                                        <img src="{{ asset('assets/img/users-silhouette.svg') }}">
                                                                        <p>
                                                                            <span>{{$camp->success_payments->count()}}</span> Donors
                                                                        </p>
                                                                    </a>
                                                                    <a class="card-text days-left" aria-live="assertive">
                                                                        <img src="{{ asset('assets/img/clock.svg') }}">
                                                                        <p>
                                                                            <span>{{$camp->days_left()}}</span> Days to go</p>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>                                                        
                                                        @endforeach
                                                    </div>

                                                </div>
                                                <!-- <div class="campaign-post col-md-12 mt-4">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 card">
                                                            <div class="card-body">
                                                                <div class="text-left"> -->
                                                                    <!-- <div class="campaign-profile vertical-center">
                                                                        <div class="image-profile">
                                                                            <img src="{{ asset('assets/img/campaign-img.png') }}">
                                                                        </div>
                                                                        <div class="profile-text">
                                                                            <h5>Joan Arc</h5>
                                                                            <div>
                                                                                <h6>
                                                                                    26 Jun 5:28PM
                                                                                </h6>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <h4 class="card-title">Post Title</h4>
                                                                    <p>
                                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                                                        Ut enim ad minim veniam, quis nostrud exercitation
                                                                        ullamco
                                                                    </p> -->
                                                                    <!-- <div class="comments-wrap text-right">
                                                                        <span class="upload">
                                                                            <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                                <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch
                                                                                <desc>Created with Sketch.</desc>
                                                                                <defs></defs>
                                                                                <g id="Web-Platform" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                                                    <g id="message-square-(1)" transform="translate(-52.000000, 1.000000)" stroke="#999999" stroke-width="2">
                                                                                        <g id="upload" transform="translate(53.000000, 0.000000)">
                                                                                            <path d="M18,12 L18,16 C18,17.1045695 17.1045695,18 16,18 L2,18 C0.8954305,18 0,17.1045695 0,16 L0,12" id="Shape"></path>
                                                                                            <polyline id="Shape" points="14 5 9 0 4 5"></polyline>
                                                                                            <path d="M9,0 L9,12" id="Shape"></path>
                                                                                        </g>
                                                                                    </g>
                                                                                </g>
                                                                            </svg>
                                                                        </span>
                                                                        <span class="comments-icon">
                                                                            <svg width="20px" height="20px" style="margin-right: 5px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                                                xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                                <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch
                                                                                <desc>Created with Sketch.</desc>
                                                                                <defs></defs>
                                                                                <g id="Web-Platform" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                                                    <g id="message-square-(1)" transform="translate(1.000000, 1.000000)" stroke="#999999" stroke-width="2">
                                                                                        <path d="M18,12 C18,13.1045695 17.1045695,14 16,14 L4,14 L0,18 L0,2 C0,0.8954305 0.8954305,0 2,0 L16,0 C17.1045695,0 18,0.8954305 18,2 L18,12 Z"
                                                                                            id="Shape"></path>
                                                                                    </g>
                                                                                </g>
                                                                            </svg>
                                                                            <span class="comments-count">
                                                                                <u>
                                                                                    20 Comments
                                                                                </u>
                                                                            </span>
                                                                        </span>
                                                                        <span class="like-comment">


                                                                            <svg width="24px" height="21px" style="margin-right: 5px" viewBox="0 0 24 21" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                                                <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch
                                                                                <desc>Created with Sketch.</desc>
                                                                                <defs></defs>
                                                                                <g id="Web-Platform" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
                                                                                    <g id="message-square-(1)" transform="translate(-158.000000, -3.000000)" fill="#125cd5" stroke="#125cd5" stroke-width="2">
                                                                                        <g id="heart" transform="translate(159.000000, 3.000000)">
                                                                                            <path d="M19.84,2.61 C18.8084758,1.57799147 17.409138,0.998174379 15.95,0.998174379 C14.490862,0.998174379 13.0915242,1.57799147 12.06,2.61 L11,3.67 L9.94,2.61 C7.79161231,0.461612378 4.30838771,0.461612404 2.16000006,2.61000006 C0.0116124038,4.75838771 0.0116123778,8.24161231 2.16,10.39 L3.22,11.45 L11,19.23 L18.78,11.45 L19.84,10.39 C20.8720085,9.3584758 21.4518256,7.95913803 21.4518256,6.5 C21.4518256,5.04086197 20.8720085,3.6415242 19.84,2.61 Z"
                                                                                                id="Shape"></path>
                                                                                        </g>
                                                                                    </g>
                                                                                </g>
                                                                            </svg>
                                                                            <span class="like-counts">
                                                                                5 Likes
                                                                            </span>
                                                                        </span>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="campaign-update col-md-12 mt-4">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 card">
                                                            <div class="card-body">
                                                                <div class="update-field text-center">
                                                                   {{--  <div class="image-profile">
                                                                        <img src="{{ asset('assets/img/campaign-img.png') }}">
                                                                    </div>
                                                                    <p>
                                                                        <span class="bold-text">Joan Arc</span> started a new campaign to
                                                                        <span class="bold-text">Save Joan</span>
                                                                    </p>
                                                                    <div class="btn-wrapper">
                                                                        <a href="#" class="btn btn-primary btn-lg btn-orange mx-auto" role="button" aria-label="ask a question">View Campaign</a>
                                                                    </div> --}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="dashboard-disclaimer col-md-10 col-lg-6 mx-auto pt-5 mb-5 mt-5">
                                        <a href="#" class="btn btn-primary btn-lg btn-light mx-auto d-block col-md-9" role="button" aria-label="ask a question">View More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            </div>
        </section>
    </main>
@endsection