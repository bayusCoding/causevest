<li class="nav-item">
    <a href="{{ route('dashboard') }}" class="nav-link">Home</a>
</li>
<li class="nav-item">
    <a href="{{route('my_campaigns')}}" class="nav-link">My Campaigns</a>
</li>
<li class="nav-item">
    <a href="{{route('payments')}}" class="nav-link">Payments</a>
</li>

<li class="nav-item">
    <a href="{{route('withdraw')}}" class="nav-link">Withdrawals</a>
</li>


@if($auth_user->is_admin() || $auth_user->is_operator())
<li class="nav-item"> 
    <a href="{{route('users')}}" class="nav-link"> Users</a> 
</li>

<li class="nav-item"> 
    <a href="{{route('categories')}}" class="nav-link"> Categories </a>
</li>
@endif

@if($auth_user->is_admin() || $auth_user->is_operator())
<li class="nav-item" id="campaign-nav">
    <a href="#" class="nav-link">
        Campaigns 
        <i class="fa fa-caret-down"></i>
    </a>
    
    <ul class="nav nav-second-level settings-nav card">
        <li> <a href="{{ route('all_campaigns') }}">@lang('app.all_campaigns')</a> </li>
        {{--<li> <a href="{{ route('staff_picks') }}">@lang('app.staff_picks')</a> </li>--}}
        <li> <a href="{{ route('funded') }}">@lang('app.funded')</a> </li>
        <li> <a href="{{ route('blocked_campaigns') }}">@lang('app.blocked_campaigns')</a> </li>
        <li> <a href="{{ route('pending_campaigns') }}">@lang('app.pending_campaigns')</a> </li>
        <li> <a href="{{ route('expired_campaigns') }}">@lang('app.expired_campaigns')</a> </li>
        <li> <a href="{{ route('campaign_leaderboard') }}">Campaign Leaderboard</a> </li>
    </ul>
</li>
@endif

@if($auth_user->is_admin())
<li class="nav-item" id="settings-nav">
    <a href="#" class="nav-link">
        Settings
        <i class="fa fa-caret-down"></i>
    </a>
    <ul class="nav nav-second-level settings-nav card">
        <li> <a href="{{ route('general_settings') }}">@lang('app.general_settings')</a> </li>
        <li> <a href="{{ route('payment_settings') }}">@lang('app.payment_settings')</a> </li>
        <!-- <li> <a href="{{ route('theme_settings') }}">@lang('app.theme_settings')</a> </li> -->
        <li> <a href="{{ route('social_settings') }}">@lang('app.social_settings')</a> </li>
        <li> <a href="{{ route('re_captcha_settings') }}">@lang('app.re_captcha_settings')</a> </li>
        <!-- <li> <a href="{{ route('other_settings') }}">@lang('app.other_settings')</a> </li> -->
        <li> <a href="{{ route('top_up') }}">Donation</a> </li>
        <li> <a href="{{ route('add_admin') }}">Add Admin</a> </li>
    </ul>
</li>
@endif