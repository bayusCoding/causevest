@if($campaignsByCategoryId->count() > 0)
    @foreach($campaignsByCategoryId as $campaign)
    <div class="col-xs-12 col-md-4">
        <div class="card p-4">
            <div class="like-container">
                <div class="like-cnt unchecked" id="like-cnt">
                    <svg height="62" viewBox="0 0 54 52" width="64" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <filter id="a" height="116.9%" width="124.7%" x="-12.3%" y="-6.2%">
                            <feMorphology in="SourceAlpha" operator="dilate" radius="3" result="shadowSpreadOuter1"></feMorphology>
                            <feOffset dy="12" in="shadowSpreadOuter1" result="shadowOffsetOuter1"></feOffset>
                            <feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="10"></feGaussianBlur>
                            <feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.05 0"></feColorMatrix>
                            <feMerge>
                                <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
                                <feMergeNode in="SourceGraphic"></feMergeNode>
                            </feMerge>
                        </filter>
                        <path d="m25.8488318 14.7377048c-1.6528355-1.6496691-4.3332467-1.6503936-5.9868084-.001449-.0007263.0003623-.0010894.0010868-.0014525.001449l-.8158878.8139672-.8158878-.8139672c-1.6539249-1.6496691-4.3346992-1.6496691-5.988624 0-1.6535617 1.6496691-1.6535617 4.3244949 0 5.974164l.8162509.8139672 5.9882609 5.974164 5.9882609-5.974164.8158878-.8139672c1.6535618-1.6489446 1.654288-4.3230459.0014524-5.972715z" fill="#fff" fill-rule="evenodd" filter="url(#a)" stroke="#125cd5" transform="translate(-2.5 -4.5)"></path>
                    </svg>
                </div>
            </div>
            <img class="card-img-top" src="{{ $campaign->feature_img_url() }}" alt="campaign image" role="img">
            @if($campaign->show_balance == 1)
            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: {{ $campaign->percent_raised() }}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            
            <div class="card-body" style="padding-bottom: 0rem;">
                <div class="progress-title-wrapper">
                    <h5 class="progress-title" aria-live="assertive">raised <span class="primary-color">{{get_amount($campaign->success_payments->sum('amount'))}}</span> / ${{ $campaign->goal }}</h5>
                    <h5 class="progress-title" aria-live="assertive"><span class="primary-color">{{ $campaign->percent_raised() }}%</span></h5>
                </div>
            </div>
            @endif
            <div class="card-body">
                <h4 class="card-title" aria-live="assertive"><a href="{{route('campaign_single', [$campaign->id, $campaign->slug])}}">{{$campaign->title}}</a></h4>
                <p class="card-text" aria-live="assertive">{{ str_limit($campaign->short_description, $limit = 70, $end = '...') }}</p>
            </div>
            <div class="campaign-details">
                <a class="card-text donors-total" aria-live="assertive">
            <img src="{{ asset('assets/img/users-silhouette.svg') }}">
                <p><span>2,332</span> Donors</p>
            </a>
                <a class="card-text days-left" aria-live="assertive">
            <img src="{{ asset('assets/img/clock.svg') }}">
            <p><span>23</span> Days to go</p>
        </a>
            </div>
        </div>
    </div>
    @endforeach
@else
    <div class="col-xs-12 col-md-4 offset-md-4">
        <div class="card p-4">
            <h4 style="text-align:center" class="card-title" aria-live="assertive">No Campains Under this Category</h4>
        </div>
    </div>
@endif