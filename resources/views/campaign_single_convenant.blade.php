@extends('layouts.pages')
@section('title') @if( ! empty($title)) {{ $title }} | @endif @parent @endsection

<style>
    .qr{
        margin-bottom: 0;
        line-height: 1.5;
        text-align: left
    }

    .pad{
        padding: 10px;
    }
</style>

@section('meta-data')
    <meta name="description" content="{{$campaign->short_description ? $campaign->short_description : $campaign->description}}" />

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="{{$campaign->short_description ? $campaign->short_description : $campaign->description}}">
    {{--<meta name="twitter:site" content="@publisher_handle">--}}
    <meta name="twitter:title" content="@if( ! empty($title)) {{ $title }} @endif">
    <meta name="twitter:description" content="{{$campaign->short_description ? $campaign->short_description : $campaign->description}}">
    {{--<meta name="twitter:creator" content="@author_handle" />--}}
    <!-- Twitter Summary card images must be at least 120x120px -->
    <meta name="twitter:image" content="{{$campaign->feature_img_url(true)}}">

    <!-- Open Graph data -->
    <meta property="og:title" content="@if( ! empty($title)) {{ $title }} @endif" />
    <meta property="og:url" content="{{route('campaign_single', [$campaign->id, $campaign->slug])}}" />
    <meta property="og:image" content="{{$campaign->feature_img_url(true)}}" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="{{$campaign->short_description ? $campaign->short_description : $campaign->description}}" />
    
@endsection
<style>
.hide{
    display: none;
}

.reward-text{
    margin-top: 25px;
}
</style>
    
@section('content')

    <main role="main" class="single-campaign">
        <div class="header-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-12 pt-4">
                        <div class="row">
                            <div class="col-xs-12 col-md-7 featured-image-wrapper pr-4">
                                <img src="{{url('/')}}/uploads/images/{{$campaign->feature_image}}" class="campaign-featured" alt="little boy in a heart shape" role="img">
                            </div>
                            <div class="col-xs-12 col-md-5 featured-description-wrapper">
                                <div class="badge-post badge">
                                    <span class="badge-img">
                                        <img src="{{ asset('assets/img/badge-notice.svg') }}" alt="Badge Notice">
                                    </span>
                                    <span>{{$campaign->get_category->category_name}}</span>
                                </div>
                                @if ($campaign->end_method == 'perpetuity')
                                <div class="badge-post badge">
                                    <span class="badge-img">
                                        <img src="{{ asset('assets/img/badge-notice.svg') }}" alt="Badge Notice">
                                    </span>
                                    <span>{{$campaign->end_method}}</span>
                                </div>
                                @endif
                                <h3 aria-live="assertive">{{$campaign->title}}</h3>
                                <p aria-live="assertive">{{$campaign->short_description}}</p>

                                <h6 data-val="" class="card-text-secondary"> </h6>

                                <script>
                                    
                                    $('.card-text-secondary').html("CV"+"{{$campaign->id}}".padStart(6, 0))
                                    $('.card-text-secondary').data('val', "CV"+"{{$campaign->id}}".padStart(6, 0))
                                    console.log( $('.card-text-secondary').data('val') )
                                </script>
                                
                                <div class="campaign-contributor">
                                    <div class="campaign-profile vertical-center">
                                        <div class="image-profile">
                                            <img src="{{ $campaignCreator->get_gravatar(100) }}">
                                        </div>
                                        <div class="profile-text">
                                            <h5>{{ $campaignCreator->name }}</h5>
                                            <div>
                                                <a style="text-decoration: none">
                                                    @if($campaign->cause_campaign == 1)
                                                        <span>{{ $campaign->cause_name }}</span>
                                                    @endif
                                                </a>
                                                <!-- <span>{{ $campaignCount }}</span> Campaigns</a> -->
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="follow-contributor">
                                        <a href="#" class="btn btn-primary btn-md btn-light float-right">
                                            Follow
                                        </a>
                                    </div> -->
                                    <span> <a href="" data-toggle="modal" data-target="#qrc" class="btn btn-light">QRCode</a> </span>

                                </div>

                                <div class="modal fade" id="qrc" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="qr" id="exampleModalLabel">SCAN QRCODE</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <span style="padding:50px">{{ QRCode::url( route('campaign_single', [$campaign->id, $campaign->slug]) )->setSize(8)->setMargin(2)->svg() }} </span>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                                
                                @if($campaign->show_balance == 1)
                                <div class="balance">
                                    <div class="donor-stats">
                                        <h3>
                                            <span class="amount-no">{{get_amount($campaign->success_payments->sum('amount'))}}</span>
                                        </h3>
                                        <div>
                                            <span class="amount-no">of {{get_amount($campaign->goal)}} </span> raised by
                                            <span>{{$campaign->success_payments->count()}}</span> Donors
                                        </div>
                                    </div>
                                        @php
                                            $percent_raised = $campaign->percent_raised();
                                        @endphp
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: {{$percent_raised}}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>

                                    <div class="balance-status">
                                        <div class="text-left">
                                            <span>{{$percent_raised}}</span>% funded
                                            <!-- <span style="color: #125cd5; font-weight: bold;">All or Nothing Goal</span> -->
                                        </div>
                                        <div>
                                            <div class="text-right">
                                                <span>{{$campaign->days_left()}}</span> days to go
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                @endif
                                <div class="like-votes">
                                    <div class="campaign-like">
                                        <div class="like-cnt unchecked" id="like-cnt">
                                            <span>Remind Me</span>

                                            <svg width="14px" height="14px" viewBox="0 0 14 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
                                                <desc>Created with Sketch.</desc>
                                                <defs></defs>
                                                <g id="Web-Platform" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <g id="Campaign-Light-Mode-FAQ" transform="translate(-1038.000000, -472.000000)" stroke="#125cd5">
                                                        <g id="Group-12" transform="translate(869.000000, 99.000000)">
                                                            <g id="Group-14">
                                                                <path d="M181.351541,374.4134 C180.132516,373.195979 178.155618,373.195444 176.936057,374.412331 C176.935522,374.412598 176.935254,374.413133 176.934986,374.4134 L176.33324,375.014091 L175.731493,374.4134 C174.511665,373.195979 172.534499,373.195979 171.31467,374.4134 C170.09511,375.630821 170.09511,377.604786 171.31467,378.822207 L171.916684,379.422897 L176.33324,383.831704 L180.749795,379.422897 L181.351541,378.822207 C182.571102,377.605321 182.571637,375.63189 181.352612,374.414469 L181.351541,374.4134 Z"
                                                                    id="Fill-1-Copy"></path>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>


                                    <div class="input-group" style="margin-left:5px">
                                        <input type="hidden" id="campaign_url_input" class="" value="{{route('campaign_single', [$campaign->id, $campaign->slug])}}">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary btn-md btn-light btn-block" id="campaign_url_copy_btn" style="padding: 10px 14.5">
                                                <span>copy
                                                    <svg width="11px" height="11px" viewBox="0 0 11 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                        <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
                                                        <desc>Created with Sketch.</desc>
                                                        <defs></defs>
                                                        <g id="Web-Platform" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <g id="link-symbol" fill="#125cd5" fill-rule="nonzero">
                                                                <path d="M10.1236507,4.97339301 L8.06307205,7.03329913 C6.92579694,8.17124672 5.08084716,8.17124672 3.94337991,7.03329913 C3.76420961,6.85463319 3.62404367,6.65331878 3.50095415,6.44571179 L4.45838646,5.48835153 C4.50389956,5.44245415 4.56010044,5.41615502 4.61380349,5.38517249 C4.67999563,5.61132096 4.79554367,5.82541266 4.97339301,6.00328603 C5.54135808,6.57170742 6.46555022,6.57091485 7.03317904,6.00328603 L9.09310917,3.94342795 C9.66150655,3.3750786 9.66150655,2.45110262 9.09310917,1.88318559 C8.52545633,1.31526856 7.60150437,1.31526856 7.03317904,1.88318559 L6.30067031,2.6164869 C5.70623799,2.38503057 5.06581223,2.32280131 4.44236681,2.41416376 L6.00328603,0.853292576 C7.14123362,-0.284438865 8.98570306,-0.284438865 10.1236507,0.853292576 C11.2610459,1.99097598 11.2610459,3.83573362 10.1236507,4.97339301 Z M4.67639301,8.36057642 L3.9433559,9.09387773 C3.37543886,9.66150655 2.45119869,9.66150655 1.88318559,9.09387773 C1.31522052,8.52545633 1.31522052,7.60148035 1.88318559,7.03329913 L3.9433559,4.97339301 C4.51172926,4.4050917 5.43534498,4.4050917 6.00326201,4.97339301 C6.18070306,5.1508821 6.29637118,5.36492576 6.36318777,5.59078603 C6.41720306,5.5593952 6.47270742,5.53388865 6.51819651,5.48832751 L7.47555677,4.53135153 C7.35325983,4.32292795 7.21237336,4.12233406 7.033131,3.94347598 C5.89595197,2.80574454 4.05083406,2.80574454 2.91310262,3.94347598 L0.853244541,6.00340611 C-0.284366812,7.1414738 -0.284366812,8.98570306 0.853244541,10.1236747 C1.99097598,11.2610459 3.83566157,11.2610459 4.973369,10.1236747 L6.53469651,8.56246725 C5.91091485,8.65435808 5.27032096,8.59160044 4.67639301,8.36057642 Z"
                                                                    id="Shape"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </span>
                                            </button>
                                        </div>
                                    </div>

                                    <!-- hidden -->
                                    <div class="btn-vote" style="visibility:hidden">
                                        @if($user)
                                            @if( $userHasVoted )
                                                <a class="btn btn-primary btn-md btn-orange btn-block on">
                                            @else
                                                <a class="btn btn-primary btn-md btn-light btn-block on">
                                            @endif
                                        @else
                                        <a class="btn btn-primary btn-md btn-light btn-block on">
                                        @endif
                                            <span>vote
                                                <svg width="14px" height="12px" viewBox="0 0 10 10" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Web-Platform" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="Campaign-Light-Mode-FAQ" transform="translate(-542.000000, -652.000000)" fill="#125cd5" fill-rule="nonzero">
                                                            <g id="Group-12" transform="translate(542.000000, 99.000000)">
                                                                <g id="Group-14">
                                                                    <g id="vote" transform="translate(0.000000, 553.000000)">
                                                                        <path d="M1.66666001,3 L0.33333998,3 C0.150038885,3 0,3.1567472 0,3.34999057 L0,9.65005129 C0,9.84321092 0.14938082,10 0.33333998,10 L1.66666001,10 C1.85061917,10 2,9.84318998 2,9.65005129 L2,3.34999057 C2,3.1567472 1.84994117,3 1.66666001,3 Z"
                                                                            id="Shape"></path>
                                                                        <path d="M9.22771206,3.08366815 C9.17789629,3.07470163 6.84538181,3.08366815 6.84538181,3.08366815 L7.1711829,2.13919447 C7.39598306,1.48687996 7.25044031,0.490868438 6.62668834,0.136769459 C6.42359468,0.0214631275 6.13983721,-0.0364456626 5.91105845,0.0247469197 C5.77985717,0.0598264721 5.66455178,0.152126594 5.59582378,0.275789882 C5.51678843,0.418015444 5.52494918,0.584033751 5.4968029,0.74173443 C5.42542866,1.14172784 5.24755749,1.52203817 4.97210882,1.80749213 C4.49186402,2.30519311 3,3.74101649 3,3.74101649 L3,9 L8.15539605,8.99999983 C8.8510404,9.00043243 9.30693246,8.17496179 8.96382909,7.53017838 C9.37281068,7.25184259 9.51269086,6.66557753 9.27316062,6.21544238 C9.68216072,5.93710658 9.8220224,5.35084153 9.58249216,4.90070638 C10.2881663,4.42046652 10.0522445,3.23185173 9.22771206,3.08366815 Z"
                                                                            id="Shape"></path>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </span>


                                            <span class="votes-count">
                                                <span class="vote" value="{{ $voteCount }}">{{ $voteCount }}</span>Votes
                                            </span>
                                        </a>
                                    </div>
                                    <!-- hidden -->

                                    

                                    <div class="btn-vote" style="margin-top:11px; margin-right:4px">
                                        @if($user)
                                            @if( $userHasVoted )
                                                <a class="btn btn-primary btn-md btn-orange btn-block on">
                                            @else
                                                <a class="btn btn-primary btn-md btn-light btn-block on">
                                            @endif
                                        @else
                                        <a class="btn btn-primary btn-md btn-light btn-block on">
                                        @endif
                                            <span class="votel">vote
                                                <svg width="14px" height="12px" viewBox="0 0 10 10" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Web-Platform" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="Campaign-Light-Mode-FAQ" transform="translate(-542.000000, -652.000000)" fill="#125cd5" fill-rule="nonzero">
                                                            <g id="Group-12" transform="translate(542.000000, 99.000000)">
                                                                <g id="Group-14">
                                                                    <g id="vote" transform="translate(0.000000, 553.000000)">
                                                                        <path d="M1.66666001,3 L0.33333998,3 C0.150038885,3 0,3.1567472 0,3.34999057 L0,9.65005129 C0,9.84321092 0.14938082,10 0.33333998,10 L1.66666001,10 C1.85061917,10 2,9.84318998 2,9.65005129 L2,3.34999057 C2,3.1567472 1.84994117,3 1.66666001,3 Z"
                                                                            id="Shape"></path>
                                                                        <path d="M9.22771206,3.08366815 C9.17789629,3.07470163 6.84538181,3.08366815 6.84538181,3.08366815 L7.1711829,2.13919447 C7.39598306,1.48687996 7.25044031,0.490868438 6.62668834,0.136769459 C6.42359468,0.0214631275 6.13983721,-0.0364456626 5.91105845,0.0247469197 C5.77985717,0.0598264721 5.66455178,0.152126594 5.59582378,0.275789882 C5.51678843,0.418015444 5.52494918,0.584033751 5.4968029,0.74173443 C5.42542866,1.14172784 5.24755749,1.52203817 4.97210882,1.80749213 C4.49186402,2.30519311 3,3.74101649 3,3.74101649 L3,9 L8.15539605,8.99999983 C8.8510404,9.00043243 9.30693246,8.17496179 8.96382909,7.53017838 C9.37281068,7.25184259 9.51269086,6.66557753 9.27316062,6.21544238 C9.68216072,5.93710658 9.8220224,5.35084153 9.58249216,4.90070638 C10.2881663,4.42046652 10.0522445,3.23185173 9.22771206,3.08366815 Z"
                                                                            id="Shape"></path>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                                &nbsp;
                                            </span>


                                            <span class="votes-count">
                                                <span class="vote" value="{{ $voteCount }}">{{ $voteCount }}</span>Votes
                                            </span>
                                        </a>
                                    </div>


                                    <div class="btn-xcv" style="margin-top:11px; margin-right:10px">
                                        @if($numberOfUserXcvVotesByCampaignId > 0)
                                            <a class="btn btn-primary btn-md btn-orange btn-block xcvs">
                                        @else
                                            <a class="btn btn-primary btn-md btn-light btn-block xcvs">
                                        @endif
                                            <span>XCV Votes
                                                <svg width="14px" height="12px" viewBox="0 0 10 10" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                    xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
                                                    <desc>Created with Sketch.</desc>
                                                    <defs></defs>
                                                    <g id="Web-Platform" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <g id="Campaign-Light-Mode-FAQ" transform="translate(-542.000000, -652.000000)" fill="#125cd5" fill-rule="nonzero">
                                                            <g id="Group-12" transform="translate(542.000000, 99.000000)">
                                                                <g id="Group-14">
                                                                    <g id="vote" transform="translate(0.000000, 553.000000)">
                                                                        <path d="M1.66666001,3 L0.33333998,3 C0.150038885,3 0,3.1567472 0,3.34999057 L0,9.65005129 C0,9.84321092 0.14938082,10 0.33333998,10 L1.66666001,10 C1.85061917,10 2,9.84318998 2,9.65005129 L2,3.34999057 C2,3.1567472 1.84994117,3 1.66666001,3 Z"
                                                                            id="Shape"></path>
                                                                        <path d="M9.22771206,3.08366815 C9.17789629,3.07470163 6.84538181,3.08366815 6.84538181,3.08366815 L7.1711829,2.13919447 C7.39598306,1.48687996 7.25044031,0.490868438 6.62668834,0.136769459 C6.42359468,0.0214631275 6.13983721,-0.0364456626 5.91105845,0.0247469197 C5.77985717,0.0598264721 5.66455178,0.152126594 5.59582378,0.275789882 C5.51678843,0.418015444 5.52494918,0.584033751 5.4968029,0.74173443 C5.42542866,1.14172784 5.24755749,1.52203817 4.97210882,1.80749213 C4.49186402,2.30519311 3,3.74101649 3,3.74101649 L3,9 L8.15539605,8.99999983 C8.8510404,9.00043243 9.30693246,8.17496179 8.96382909,7.53017838 C9.37281068,7.25184259 9.51269086,6.66557753 9.27316062,6.21544238 C9.68216072,5.93710658 9.8220224,5.35084153 9.58249216,4.90070638 C10.2881663,4.42046652 10.0522445,3.23185173 9.22771206,3.08366815 Z"
                                                                            id="Shape"></path>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </span>


                                            <span class="votes-count">
                                                <span class="xcv" value="{{ $countXcvVote }}"> {{ $countXcvVote }} </span>Votes
                                                 
                                            </span>
                                        </a>
                                    </div>


                                </div>
                                {{Form::open([ 'route' => 'add_to_cart', 'class' => 'form-horizontal'])}}
                                <div class="btn-wrapper">
                                    
                                    <input type="hidden" name="amount" step="1" min="1" name="amount" class="form-control" value="{{$campaign->recommended_amount}}" />
                                    <input type="hidden" name="campaign_id" value="{{$campaign->id}}" />
                                    <input type="hidden" name="annuity" value="0" />
                                    {{-- <input type="checkbox" name="convenant" value="Convenant"/> --}}
                                    <button type="submit" class="btn btn-primary btn-lg btn-orange btn" aria-label="explore campaigns" role="button">Convenant Donation</button>
                                    

                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{route('campaign_single', [$campaign->id, $campaign->slug])}}" class="share facebook-share">
                                        <input type="hidden" id="campaign_url_input" class="form-control" value="{{route('campaign_single', [$campaign->id, $campaign->slug])}}">
                                        <span>Share</span>

                                        <svg width="14px" height="15px" viewBox="0 0 12 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
                                            <desc>Created with Sketch.</desc>
                                            <defs></defs>
                                            <g id="Web-Platform" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <g id="Campaign-Light-Mode" transform="translate(-810.000000, -595.000000)" fill="#3B5998">
                                                    <g id="Group-12" transform="translate(810.000000, 99.000000)">
                                                        <g id="CTA-Copy-4" transform="translate(0.000000, 360.000000)">
                                                            <g id="Group-9" transform="translate(0.500000, 12.000000)">
                                                                <path d="M10.388779,124 L0.606853933,124 C0.271662921,124 0,124.271663 0,124.606854 L0,134.388779 C0,134.723929 0.271662921,134.995674 0.606853933,134.995674 L5.87313483,134.995674 L5.87313483,130.737562 L4.44016854,130.737562 L4.44016854,129.078086 L5.87313483,129.078086 L5.87313483,127.854285 C5.87313483,126.43409 6.74052809,125.660753 8.00746442,125.660753 C8.61435955,125.660753 9.13593258,125.705906 9.28791386,125.726094 L9.28791386,127.210311 L8.40923221,127.210723 C7.72022846,127.210723 7.58678652,127.538127 7.58678652,128.018584 L7.58678652,129.078086 L9.23007116,129.078086 L9.01612734,130.737562 L7.58678652,130.737562 L7.58678652,134.995674 L10.388779,134.995674 C10.7239288,134.995674 10.995633,134.723929 10.995633,134.388779 L10.995633,124.606854 C10.995633,124.271663 10.7239288,124 10.388779,124"
                                                                    id="Fill-1"></path>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                    </a>

                                    <a target="_blank" href="https://twitter.com/home?status={{route('campaign_single', [$campaign->id, $campaign->slug])}}" class="share twitter-share">
                                        <span>Tweet</span>


                                        <svg width="14px" height="15px" viewBox="0 0 14 11" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                            <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
                                            <desc>Created with Sketch.</desc>
                                            <defs></defs>
                                            <g id="Web-Platform" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <g id="Campaign-Light-Mode" transform="translate(-1202.000000, -471.000000)" fill="#1DA1F2">
                                                    <g id="Group-12" transform="translate(869.000000, 99.000000)">
                                                        <g id="Group-14">
                                                            <path d="M345.160062,375.094953 C345.160062,378.767116 342.366572,383 337.257687,383 C335.743586,382.999201 334.263077,382.565282 333,381.752124 C333.21987,381.776094 333.440869,381.788938 333.662106,381.790605 C334.91863,381.790627 336.136864,381.369394 337.112057,380.597701 C335.911044,380.568896 334.864362,379.79329 334.515563,378.673663 C334.688074,378.706876 334.863527,378.723448 335.039377,378.723138 C335.28754,378.723995 335.534582,378.690682 335.773169,378.624188 C334.461544,378.346651 333.530366,377.210184 333.543571,375.903048 C333.929362,376.080745 334.36091,376.199428 334.80287,376.216392 C333.578221,375.379464 333.206211,373.773729 333.943205,372.505747 C335.332648,374.231666 337.419294,375.28964 339.667911,375.408296 C339.62005,375.200804 339.596007,374.988775 339.596225,374.776112 C339.576144,373.2625 340.819741,372.019608 342.37391,372 C343.145641,372.000352 343.881418,372.317722 344.400305,372.874063 C345.024794,372.754072 345.62243,372.526989 346.165357,372.203398 C345.962142,372.84205 345.52767,373.387084 344.942746,373.737131 C345.494349,373.672813 346.032532,373.526344 346.538462,373.302849 C346.167568,373.857181 345.698566,374.342981 345.153288,374.737631 C345.158368,374.858571 345.160062,374.97951 345.160062,375.094953 Z"
                                                                id="Twitter-Icon-Copy-2"></path>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                    </a>
                                </div>
                                {{Form::close()}}
                                {{-- <div class="col-md-8 col-md-offset-2">
                                    <a href="{{ route('campaign_single_convenant', [$campaign->id, $campaign->slug]) }}" class="btn btn-primary btn-lg btn-orange btn">Convenant Donation</a>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- section 1-->
        <section class="single-archive">
            <div class="container">
                <div class="single-nav-wrapper">
                    <ul class="navigate nav campaign-nav" role="menubar">
                        <li class="nav-item" role="menuitem" id="campaign">
                            <a style="cursor:pointer" class="nav-link active" aria-live="assertive" aria-label="campaigns" style="padding-left: 0rem;">
                                <span>campaign</span>
                            </a>
                        </li>
                        <li style="cursor:pointer" class="nav-item" role="menuitem" id="faq">
                            <a class="nav-link" aria-live="assertive" aria-label="FAQ" style="text-transform: uppercase">
                                <span>FAQ</span>
                            </a>
                        </li>
                        <li style="cursor:pointer" class="nav-item" role="menuitem" id="update">
                            <a class="nav-link" aria-live="assertive" aria-label="updates">
                                <span>Updates</span>
                            </a>
                        </li>
                        <li style="cursor:pointer" class="nav-item" role="menuitem" id="community">
                            <a class="nav-link" aria-live="assertive" aria-label="community">
                                <span>Community</span>
                            </a>
                        </li>
                        <li style="cursor:pointer" class="nav-item" role="menuitem" id="backer">
                            <a class="nav-link" aria-live="assertive" aria-label="community">
                                <span>backers</span>
                            </a>
                        </li>
                        <li style="cursor:pointer" class="nav-item" role="menuitem">
                            <a class="nav-link" aria-live="assertive" aria-label="community">
                                <span>Private Message</span>
                            </a>
                        </li>
                    </ul>
                    <div class="navbar navbar-light bg-transparent nav-secondary single-navbar" role="navigation">
                        <div class="container">
                            <!-- background toggle -->
                            <ul class="nav justify-content-end campaign-nav" role="menubar">
                                <li class="nav-item" role="menuitem" id="campaign">
                                    <a style="cursor:pointer" class="nav-link active" aria-live="assertive" aria-label="campaigns" style="padding-left: 0rem;">
                                        <span>campaign</span>
                                    </a>
                                </li>
                                <li style="cursor:pointer" class="nav-item" role="menuitem" id="faq">
                                    <a class="nav-link" aria-live="assertive" aria-label="FAQ" style="text-transform: uppercase">
                                        <span>FAQ</span>
                                    </a>
                                </li>
                                <li style="cursor:pointer" class="nav-item" role="menuitem" id="update">
                                    <a class="nav-link" aria-live="assertive" aria-label="updates">
                                        <span>Updates</span>
                                    </a>
                                </li>
                                <li style="cursor:pointer" class="nav-item" role="menuitem" id="community">
                                    <a class="nav-link" aria-live="assertive" aria-label="community">
                                        <span>Community</span>
                                    </a>
                                </li>
                                <li style="cursor:pointer" class="nav-item" role="menuitem" id="backer">
                                    <a class="nav-link" aria-live="assertive" aria-label="community">
                                        <span>backers</span>
                                    </a>
                                </li>
                                <li style="cursor:pointer" class="nav-item" role="menuitem">
                                    <a class="nav-link" aria-live="assertive" aria-label="community">
                                        <span>Private Message</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row pt-5">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-xs-12 col-md-12 col-lg-11">
                                <div class="pr-5 campaign-sections">
                                    <div id="campaign-campaign" style="display:none">
                                        <h4>About this</h4>
                                        
                                        @if( ! empty($campaign->video))
                                        <?php
                                            $video_url = $campaign->video;
                                            if (strpos($video_url, 'youtube') > 0) {
                                                preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $video_url, $matches);
                                                if ( ! empty($matches[1])){
                                                    echo '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/'.$matches[1].'" frameborder="0" allowfullscreen></iframe></div>';
                                                }

                                            } elseif (strpos($video_url, 'vimeo') > 0) {
                                                if (preg_match('%^https?:\/\/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|video\/|)(\d+)(?:$|\/|\?)(?:[?]?.*)$%im', $video_url, $regs)) {
                                                    if (!empty($regs[3])){
                                                        echo '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/'.$regs[3].'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
                                                    }
                                                }
                                            }
                                            ?>
                                            
                                        @endif
                                        <br />
                                            <div class="font">{!! $campaign->description !!}</div>
                                        
                                        <!-- <div class="section-btn-wrapper col-md-10 pr-5 mb-5">
                                            <a href="#" class="btn btn-primary btn-md btn-orange" role="button" aria-label="show more">donate now</a>
                                        </div> -->

                                    </div>
                                    
                                    <div id="campaign-faq" style="display:none">
                                        @include('campaign_single_faq')
                                    </div>

                                    <div id="campaign-update" style="display:none">
                                        @include('campaign_single_update')
                                    </div>

                                    <div id="campaign-community" style="display:none">
                                        @include('campaign_single_community')
                                    </div>

                                    <div id="campaign-backer" style="display:none">
                                        @include('campaign_single_backer')
                                    </div>
                                </div>


                            </div>
                            
                            
                        </div>
                    </div>


                    <div class="col-md-4 support-col">
                        <h4>Support</h4>
                        <div class="row">
                            <div class="col-xs-12 col-md-10 support-card card">
                                <div class="card-body">
                                    
                                    <div class="card-title">
                                        <h5 class="card-tag">Donate without a reward</h5>
                                    </div>
                                    {{Form::open([ 'route' => 'add_to_cart', 'class' => 'form-horizontal'])}}
                                    <div class="currency-field">
                                        <select class="select-cryptocurrency">
                                            <option value="Pound">$</option>
                                            <option value="Dollar">£</option>
                                            <option value="Naira">₦</option>
                                        </select>
                                        <input type="number" name="amount" step="1" min="1" name="amount" class="form-control" value="{{$campaign->recommended_amount}}" />
                                    </div>
                                    
                                        <input type="hidden" name="campaign_id" value="{{$campaign->id}}" />
                                        <input type="hidden" name="annuity" value="0" />
                                        <button type="submit" class="btn btn-primary btn-lg btn-light btn-block" aria-label="donate now" role="button"> Donate Now </button>
                                    {{Form::close()}}
                                </div>
                            </div>
                            @php
                                $is_ended = $campaign->is_ended();
                            @endphp
                            @if($campaign->rewards->count() > 0)

                                <div class="col-md-8 support-col">
                                    <h4 class="reward-text">@lang('app.or_choose_a_reward')</h4>
                                </div>

                                @foreach($campaign->rewards as $reward)

                                @php $claimed_count = $reward->payments->count(); @endphp
                                        
                                    <div class="col-xs-12 col-md-10 support-card card mt-4">
                                        <div class="card-body @if($reward->quantity <= $claimed_count || $is_ended ) reward-disable @endif ">

                                                <div class="support-col">
                                                    <h5 class="card-tag">Donate {{get_amount($reward->amount)}} or more</h5>
                                                </div>
                                                <div class="card-title level">
                                                    <p> </p>
                                                </div>
                                                <dl>
                                                    <dt class="card-text-secondary" aria-live="assertive">
                                                        <br>
                                                    </dt>
                                                    <dd class="card-text-secondary" aria-live="assertive">Item Description:
                                                        <br>{{$reward->description}}
                                                    </dd>
                                                </dl>
                                                <div class="campaign-details pr-5">
                                                    <div class="card-text delivery-date" aria-live="assertive">
                                                        <h6>delivery date</h6>
                                                        <p>
                                                            {{date('F Y', strtotime($reward->estimated_delivery))}}</p>
                                                    </div>
                                                    <div class="card-text shipping-location" aria-live="assertive">
                                                        <h6>quantity</h6>
                                                        <p>
                                                            <span>{{$reward->quantity}}</span> only</p>
                                                    </div>
                                                </div>
                                                
                                                @if( $reward->quantity > $claimed_count )
                                                <a href="{{route('add_to_cart', [$reward->id])}}" class="btn btn-primary btn-lg btn-light btn-block" aria-label="donate now" role="button">Donate Now</a>
                                                <div class="stock-details">
                                                    <dl>
                                                        <dt class="card-footer-text" aria-live="assertive">Limited:</dt>
                                                        <dd class="card-text-secondary" aria-live="assertive">
                                                            <span>{{$claimed_count}} </span> @lang('app.claimed_so_far')
                                                            <span>{{$reward->quantity}}</span> slots</dd>
                                                    </dl>

                                                    <dl>
                                                        <dt class="card-footer-text" aria-live="assertive">
                                                            <span>{{$claimed_count}}</span>
                                                        </dt>
                                                        <dd class="card-text-secondary" aria-live="assertive">Donors</dd>
                                                    </dl>

                                                </div>
                                                @else

                                                <button href="#" class="btn btn-primary btn-lg disabled btn-light btn-block" aria-label="donate now" role="button">Donate Now</button>
                                                <div class="stock-details">
                                                    <dl>
                                                        <dt class="card-footer-text" aria-live="assertive">Limited:</dt>
                                                        <dd class="card-text-secondary" aria-live="assertive">
                                                            <span>{{$claimed_count}} </span> @lang('app.claimed_so_far')
                                                            <span>{{$reward->quantity}}</span> slots</dd>
                                                    </dl>

                                                    <dl>
                                                        <dt class="card-footer-text" aria-live="assertive">
                                                            <span>{{$claimed_count}}</span>
                                                        </dt>
                                                        <dd class="card-text-secondary" aria-live="assertive">Donors</dd>
                                                    </dl>

                                                </div>
                                                @endif

                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            
                            @if ($campaign->end_method == 'perpetuity') 
                            <div class="col-xs-12 col-md-10 support-card card">
                                <div class="card-body">
                                    
                                    <div class="card-title">
                                        <h5 class="card-tag">Support this campaign with Annuity payment</h5>
                                    </div>
                                    {{Form::open([ 'route' => 'add_to_cart', 'class' => 'form-horizontal'])}}
                                    <div class="currency-field">
                                        <select class="select-cryptocurrency">
                                            <option value="Pound">$</option>
                                            <option value="Dollar">£</option>
                                            <option value="Naira">₦</option>
                                        </select>
                                        <input type="number" name="amount" min="{{$annuity_minimum_amount}}" name="amount" class="form-control" value="{{$annuity_minimum_amount}}" />
                                    </div>
                                    
                                        <input type="hidden" name="campaign_id" value="{{$campaign->id}}" />
                                        <input type="hidden" name="annuity" value="1" />
                                        <button type="submit" class="btn btn-primary btn-lg btn-light btn-block" aria-label="donate now" role="button"> <?php if ($campaign->end_method == 'perpetuity') {echo 'Annuity Payment';} else {echo 'Donate Now';} ?> </button>
                                    {{Form::close()}}
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section> 
    </main>


    <script>

        // $('.campaign-sections > div').hide()
        $('#campaign-campaign').show();

        $('ul.campaign-nav li').click( function(){

            element = $(this).attr('id');

            console.log("Clicked " + element);

            $('.campaign-sections > div').hide();
            $('#campaign-' + element).show();

        });


        $(".single-navbar a.nav-link").click(
          function () {
              $('.single-navbar a.nav-link.active').not(this).removeClass('active');
              $(this).addClass('active');
        });

        $(function(){
            $(document).on('click', '.donate-amount-placeholder ul li', function(e){
                $(this).closest('form').find($('[name="amount"]')).val($(this).data('value'));
            });

            $('#campaign_url_copy_btn').click(function(){
                copyToClipboard('#campaign_url_input');
            });

        });

        function copyToClipboard(element) {
            const toastr_options = {
                'closeButton': true,
                'debug': false,
                'newestOnTop': false,
                'progressBar': false,
                'positionClass': 'toast-top-right',
                'preventDuplicates': false,
                'showDuration': '1000',
                'hideDuration': '1000',
                'timeOut': '5000',
                'extendedTimeOut': '1000',
                'showEasing': 'swing',
                'hideEasing': 'linear',
                'showMethod': 'fadeIn',
                'hideMethod': 'fadeOut',
            }
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).val()).select();
            document.execCommand("copy");
            toastr.success('@lang('app.campaign_url_copied')', '@lang('app.success')', toastr_options);
            $temp.remove();
        }

        var status = 0;
        var st = 1

        function vote(newVal, user_id) {
            $.post("{{route('vote')}}",
            {
                votes: newVal,
                _token: "{{csrf_token()}}",
                campaign_id: "{{$campaign->id}}",
                user_id: user_id,
                type: "normal"
            },
            function(data, status){
                console.log(data.result)
                if( $('a.on').hasClass('btn-light') ){
                    $('a.on').removeClass('btn-light')
                    $('a.on').addClass('btn-orange');
                }
                else{
                    $('a.on').removeClass('btn-orange')
                    $('a.on').addClass('btn-light');
                }
            });
        }

        @if( $user )
            $('.btn-vote').click( (event) => {
                //if nobody has voted
                @if( $voteCount == 0)
                    var newVal = null;
                    var initialVal = null;
                    
                    if(status == 0){
                        initialVal = $('span.vote').html();
                        newVal = parseInt(initialVal) + 1;
                        user_id = "{{$user->id}}";

                        status = 1;
                    }
                    else{
                        initialVal = $('span.vote').html();
                        newVal = parseInt(initialVal) - 1;
                        user_id = "{{$user->id}}";

                        status = 0;
                    }
                    
                    $('span.vote').text(newVal);

                    vote(newVal, user_id);

                @else
                    //if the person logged in has voted
                    @if( $userHasVoted )
                        var newVal = null;
                        var initialVal = null;
                        var user_id = null;

                        if(st == 0){
                            initialVal = $('span.vote').html();
                            newVal = parseInt(initialVal) + 1;
                            user_id = "{{$user->id}}";

                            st = 1;
                        }
                        else{
                            initialVal = $('span.vote').html();
                            newVal = parseInt(initialVal) - 1;
                            user_id = "{{$user->id}}";

                            st = 0;
                        }
                        
                        $('span.vote').text(newVal);

                        vote(newVal, user_id);

                    
                    @else
                        var newVal = null;
                        var initialVal = null;

                        if(status == 0){
                            initialVal = $('span.vote').html();
                            newVal = parseInt(initialVal) + 1;
                            user_id = "{{$user->id}}";

                            status = 1;
                        }
                        else{
                            initialVal = $('span.vote').html();
                            newVal = parseInt(initialVal) - 1;
                            user_id = "{{$user->id}}";

                            status = 0;
                        }
                        
                        $('span.vote').text(newVal);

                        vote(newVal, user_id)

                    @endif
                @endif
            })
        @else
            $('.btn-vote').click( (event) => {
                window.location.href = "{{route('login')}}";
            })
        @endif

/////////////////////////////////////////// XCV ////////////////////////////////////////////
        @if($user)
            $('.btn-xcv').click( (event) => {
                @if ($numberOfVotesLeft != 0)
                    $.post("{{route('xcv_vote')}}",
                    {
                        _token: "{{csrf_token()}}",
                        campaign_id: "{{$campaign->id}}",
                        user_id: "{{ $user->id }}",
                        type: "xcv"
                    },
                    function(data, status){
                        console.log(data.result)
                        var initial = $('span.xcv').html();
                        var newVal = parseInt( initial ) + 1;
                        $('span.xcv').text(newVal);
                        if( $('a.xcvs').hasClass('btn-light') ){
                            $('a.xcvs').removeClass('btn-light')
                            $('a.xcvs').addClass('btn-orange');
                        }
                        const toastr_options = {
                            'closeButton': true,
                            'debug': false,
                            'newestOnTop': false,
                            'progressBar': false,
                            'positionClass': 'toast-top-right',
                            'preventDuplicates': false,
                            'showDuration': '1000',
                            'hideDuration': '1000',
                            'timeOut': '5000',
                            'extendedTimeOut': '1000',
                            'showEasing': 'swing',
                            'hideEasing': 'linear',
                            'showMethod': 'fadeIn',
                            'hideMethod': 'fadeOut',
                        }

                        toastr.success('Successfull', toastr_options);
                    });
                @else
                    const toastr_options = {
                        'closeButton': true,
                        'debug': false,
                        'newestOnTop': false,
                        'progressBar': false,
                        'positionClass': 'toast-top-right',
                        'preventDuplicates': false,
                        'showDuration': '1000',
                        'hideDuration': '1000',
                        'timeOut': '5000',
                        'extendedTimeOut': '1000',
                        'showEasing': 'swing',
                        'hideEasing': 'linear',
                        'showMethod': 'fadeIn',
                        'hideMethod': 'fadeOut',
                    }

                    toastr.error('You have no XCV Token', toastr_options);
                    @endif
            })
        @else
            $('.btn-xcv').click( (event) => {
                window.location.href = "{{route('login')}}";
            })
        @endif
//////////////////////////////////////////////////////////////////////////////////////////////
    </script>
@endsection

@section('page-js')
    @if($enable_discuss)
        <script id="dsq-count-scr" src="//{{get_option('disqus_shortname')}}.disqus.com/count.js" async></script>
    @endif

@endsection