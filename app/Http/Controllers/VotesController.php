<?php

namespace App\Http\Controllers;
use App\Votes;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
    }

    public function vote(Request $request) {
        $id = Votes::where('campaign_id', $request->campaign_id)->where('type', 'normal')->where('user_id', $request->user_id)->get()->toArray();

        if(!empty($id)){
            $vote = Votes::find($id[0]['id']);

            // $vote->votes = $request->votes;
            // $vote->user_id = $request->user_id;
            $vote->delete();
            
        }
        else{
            $data = [
                'user_id'       => $request->user_id,
                'campaign_id'   => $request->campaign_id,
                'votes'         => $request->votes,
                'type'          => $request->type
            ];

            Votes::create($data);
        }

        return response()->json([
            'result' => 'success'
        ]);
        
    }

    public function xcv(Request $request) {
        $data = [
            'user_id'       => $request->user_id,
            'campaign_id'   => $request->campaign_id,
            'votes'         => '1',
            'type'          => $request->type
        ];

        Votes::create($data);
        
        return response()->json([
            'result' => 'success'
        ]);
    }
}