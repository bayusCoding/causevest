<?php

namespace App\Http\Controllers;
use App\Likes;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LikesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
    }

    public function like(Request $request) {
        $id = Likes::where('campaign_id', $request->campaign_id)->where('user_id', $request->user_id)->get()->toArray();

        if(!empty($id)){
            $like = Likes::find($id[0]['id']);
            
            // $like->likes = $request->like;
            $like->user_id = $request->user_id;
            $like->delete();

            
        }
        else{
            $data = [
                'user_id'       => $request->user_id,
                'campaign_id'   => $request->campaign_id,
                'likes'         => $request->like
            ];

            Likes::create($data);                
        }

        return response()->json([
            'result' => 'success'
        ]);
    }
}