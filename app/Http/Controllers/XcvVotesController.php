<?php

namespace App\Http\Controllers;
use App\Xcv_votes;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class XcvVotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
    }

    public function vote(Request $request) {
        $data = [
            'user_id'       => $request->user_id,
            'campaign_id'   => $request->campaign_id,
        ];

        Xcv_votes::create($data);
        
        return response()->json([
            'result' => 'success'
        ]);
    }
}