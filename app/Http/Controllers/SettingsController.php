<?php

namespace App\Http\Controllers;

use App\Option;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

use App\Payment;
use App\Campaign;
use App\User;
use App\Dispute;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller
{

    public function GeneralSettings(){
        $title = trans('app.general_settings');

        $user = Auth::user();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();
        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();


        $d = Dispute::where('parent_id',  null)->get();
        

        if($user->is_admin()){
            $disputes = $d;
        }

        return view('admin.general_settings', compact('title', 'user', 'userCampaignCount', 'payments', 'disputes', 'donation'));
    }

    public function TopUpSettings() {
        $title = 'Top Up Donation';

        $user = Auth::user();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();
        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $totalAnnuityDonation = Payment::where('annuity_status', 1)->sum('amount');


        $d = Dispute::where('parent_id',  null)->get();
        

        if($user->is_admin()){
            $disputes = $d;
        }

        $meta_data = [
            'pool' => get_option('pool'),
            'amount' => get_option('amount'),
            'percent' => get_option('percent'),
            'criteria' => get_option('criteria'),
            'annuity' => get_option('annuity'),
            'annuity-rate' => get_option('annuity-rate')
        ];

        return view('admin.top_up_settings', compact('title', 'user', 'userCampaignCount', 'payments', 'meta_data', 'disputes', 'donation', 'totalAnnuityDonation'));
    }

    public function TopUpSettingsPost (Request $request) {

        $inputs = array_except($request->input(), ['_token']);
        //dd($inputs);

        foreach($inputs as $key => $value) {
            $option = Option::firstOrCreate(['option_key' => $key]);
            $option -> option_value = $value;
            $option->save();
        }

        return back()->with('success', trans('app.top_up_settings_saved'));
    }

    public function PaymentSettings(){
        $title = trans('app.payment_settings');

        $user = Auth::user();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();
        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $d = Dispute::where('parent_id',  null)->get();
        

        if($user->is_admin()){
            $disputes = $d;
        }

        $cause_campaigns = Campaign::where('cause_campaign', 1)->get();

        $meta_data = [
            'cause_campaign_commission' => get_option('cause_campaign_commission')
        ];


        return view('admin.payment_settings', compact('title','user','payments','userCampaignCount','disputes', 'donation', 'cause_campaigns','meta_data'));
    }

    public function postPaymentSettings(Request $request) {
        
        $inputs = array_except($request->input(), ['_token']);

        if( isset($inputs['apply_existing_campaign']) )
        {
            //Aplly to all Cause Campaigns
            $cause_campaigns = Campaign::where('cause_campaign', 1)->get();            
            
            if($cause_campaigns)
            {
                for($i = 0; $i < $cause_campaigns->count(); $i++){
                    $camp = Campaign::find( $cause_campaigns[$i]->id );
                    $camp->campaign_owner_commission = $inputs['cause_campaign_commission'];
                    $camp->save();
                }
            }
        }

        foreach($inputs as $key => $value) {
            $option = Option::firstOrCreate(['option_key' => $key]);
            $option -> option_value = $value;
            $option->save();
        }

        //check is request comes via ajax?

        if ($request->ajax()){
            return ['success'=>1, 'msg'=>trans('app.settings_saved_msg')];
        }

        return redirect()->back()->with('success', trans('app.settings_saved_msg'));
    }


    public function AdSettings(){
        $title = trans('app.ad_settings_and_pricing');
        return view('admin.ad_settings', compact('title'));
    }

    public function StorageSettings(){
        $title = trans('app.file_storage_settings');
        return view('admin.storage_settings', compact('title'));
    }
    
    public function SocialSettings(){
        $title = trans('app.social_settings');

        $user = Auth::user();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();
        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        return view('admin.social_settings', compact('title','user', 'donation', 'disputes', 'payments','userCampaignCount'));
    }
    public function reCaptchaSettings(){
        $title = trans('app.re_captcha_settings');

        $user = Auth::user();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        return view('admin.re_captcha_settings', compact('title', 'user', 'donation', 'disputes', 'payments', 'userCampaignCount'));
    }
    public function BlogSettings(){
        $title = trans('app.blog_settings');
        return view('admin.blog_settings', compact('title'));
    }
    public function OtherSettings(){
        $title = trans('app.other_settings');

        $user = Auth::user();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        return view('admin.other_settings', compact('title','user','payments','userCampaignCount'));
    }


    public function OtherSettingsPost(Request $request){

        if ($request->hasFile('logo')){
            $rules = ['logo'=>'mimes:jpeg,jpg,png'];
            $this->validate($request, $rules);

            $image = $request->file('logo');
            $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());

            $image_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();

            $upload_dir = './uploads/logo/';
            if ( ! file_exists($upload_dir)){
                mkdir($upload_dir, 0777, true);
            }
            $imageFileName = $upload_dir.$image_name;


            $resized = Image::make($image)->resize(null, 70, function ($constraint) {
                $constraint->aspectRatio();
            });

            try{
                //Uploading thumb
                $resized->save($imageFileName);
                $previous_photo= get_option('logo');
                update_option('logo', $image_name );

                if ($previous_photo){
                    if (file_exists($upload_dir.$previous_photo)){
                        unlink($upload_dir.$previous_photo);
                    }
                }

            } catch (\Exception $e){
                return $e->getMessage();
            }

        }

        return back();

    }



    public function ThemeSettings(){
        $title = trans('app.theme_settings');

        $user = Auth::user();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        return view('admin.theme_settings', compact('title','user','payments','userCampaignCount'));
    }
    public function modernThemeSettings(){
        $title = trans('app.modern_theme_settings');        
        return view('admin.modern_theme_settings', compact('title'));
    }

    public function SocialUrlSettings(){
        $title = trans('app.social_url_settings');
        return view('admin.social_url_settings', compact('title'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update(Request $request) {
        $inputs = array_except($request->input(), ['_token']);

        foreach($inputs as $key => $value) {
            $option = Option::firstOrCreate(['option_key' => $key]);
            $option -> option_value = $value;
            $option->save();
        }
        //check is request comes via ajax?
        if ($request->ajax()){
            return ['success'=>1, 'msg'=>trans('app.settings_saved_msg')];
        }
        return redirect()->back()->with('success', trans('app.settings_saved_msg'));
    }


    public function monetization(){
        $title = trans('app.website_monetization');
        return view('admin.website_monetization', compact('title'));
    }



    public function addAdmin() {
        $title = 'Add New Admin User';

        $user = Auth::user();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();


        return view('admin.add_admin', compact('title', 'user', 'userCampaignCount', 'payments'));
    }

    public function addAdminPost(Request $request) {
        $title = 'Add Admin Level';

        $user = Auth::user();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $validator = Validator::make($request->all(), [
            'name'  => 'required|max:255',
            'email' => 'required|email|unique:users',
          ]);

        // If validator fails, short circut and redirect with errors
        if($validator->fails()){
            $existing = User::where('email', $request->email)->get()[0];
            return redirect()->back()->with('fail', $existing->id);
        }

        $internal_operator = 'Internal Operator';

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt('123456');
        $user->user_type = $internal_operator;
        $user->active_status = 1;
        $user->save();

        
        User::sendWelcomeEmail($user, $internal_operator);

        return redirect()->back()->with('success', 'New Internal Operator Successfully Created');
    }

}
