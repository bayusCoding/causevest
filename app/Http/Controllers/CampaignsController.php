<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Category;
use App\Country;
use App\Payment;
use App\Reward;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use App\User;
use App\Faq;
use App\Option;
use App\Dispute;
use App\Comment;
use App\Update;
use App\Votes;
use App\Verify;
use App\Convenant;
use Illuminate\Support\Facades\DB;

class CampaignsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();

        $title = trans('app.start_a_campaign');
        $categories = Category::all();
        $countries = Country::all();

        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        return view('admin.start_campaign', compact('title', 'disputes', 'categories', 'countries', 'user', 'payments', 'userCampaignCount'));
    }
    
    public function campaignLeaderboard()
    {
        $user = Auth::user();

        $title = 'Leaderboard';


        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();
        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();
        $verify = Verify::selectRaw('verify')->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        $topup = get_option('topup_amount');
        if($topup){
            $xcv_topup = (75/100) * (int)$topup;
            $normal_topup = $topup - (int)$xcv_topup;

            $totalTopupEligleCampaigns = get_option('campaigns_eligible');
            $xcv = round( (75/100) * (int)$totalTopupEligleCampaigns );
            $normal = $totalTopupEligleCampaigns - $xcv;

            $xcv_amount = $xcv_topup/$xcv;
            $normal_amount = $normal_topup/$normal;

            $currentMonthYear = Carbon::now()->format('Y-m');

            $campaignNormal = Votes::selectRaw('campaign_id')
                ->where('type', 'normal')
                ->groupBy('campaign_id')
                ->orderByRaw('COUNT(*) DESC')
                ->limit($normal)
                ->get();

            $campaignXcv = Votes::selectRaw('campaign_id')
                ->where('type', 'xcv')
                ->groupBy('campaign_id')
                ->orderByRaw('COUNT(*) DESC')
                ->limit($xcv)
                ->get();

            //dd($xcv_topup + $normal_topup);

            $currentMonthYear = Carbon::now()->format('Y-m');

            // $campaigns = Campaign::where('created_at', 'like', "{$currentMonthYear}%" )->get();

            //dd($xcv_topup);
        }
        else{
            $campaignNormal = false;
            $campaignXcv = false;
            $normal_amount = false;
            $xcv_amount = false;
        }

        return view('admin.campaign_leaderboard', compact('title', 'xcv_amount', 'normal_amount', 'campaignNormal', 'campaignXcv', 'donation', 'disputes', 'user', 'payments', 'userCampaignCount', 'verify'));
    }

    public  function updateverification(Request $request)
    {
        $verify = Verify::where('id', 1)->update(['verify' => 1]);
        $request->session()->flash('success', 'Payment approved successfully');
        return back();
    }

    public  function cancelverify(Request $request)
    {
        $verify = Verify::where('id', 1)->update(['verify' => 0]);
        $request->session()->flash('success', 'Payment cancelled successfully');
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = [
            'category'      => 'required',
            'title'         => 'required',
            'description'   => 'required',
            'short_description' => 'required|max:200',
            'goal'          => 'required',
            'end_method'    => 'required',
            'country_id'    => 'required',
        ];

        //$this->validate($request, $rules);
        $u = Auth::user();
        $user_id = Auth::user()->id;

        $campaign_owner_commission = get_option('campaign_owner_commission');

        if($u->is_operator() || $u->is_admin()){
            $cause_campaign = $request->cause_campaign;

            if( $cause_campaign == 1 ){
                $campaign_owner_commission = get_option('cause_campaign_commission');
            }
            $cause_name = $request->cause_name;
            $cause_description = $request->cause_description;
            $cause_contact = $request->cause_contact;
            $cause_account = $request->cause_account;
        }
        else{
            $cause_campaign = null;
            $cause_name = null;
            $cause_description = null;
            $cause_contact = null;
            $cause_account = null;
        }

        $slug = unique_slug($request->title);

        //feature image has been moved to update
        $data = [
            'user_id'           => $user_id,
            'category_id'       => $request->category,
            'title'             => $request->title,
            'slug'              => $slug,
            'short_description' => $request->short_description,
            'description'       => $request->description,
            'campaign_owner_commission'              => $campaign_owner_commission,
            'goal'              => $request->goal,
            'min_amount'        => $request->min_amount,
            'max_amount'        => $request->max_amount,
            'recommended_amount' => $request->recommended_amount,
            'amount_prefilled'  => $request->amount_prefilled,
            'end_method'        => $request->end_method,

            'show_balance'      => $request->show_balance,

            'video'             => $request->video,
            'feature_image'     => '',
            'status'            => 0,
            'country_id'        => $request->country_id,
            'address'           => $request->address,
            'is_funded'         => 0,
            'start_date'        => $request->start_date,
            'end_date'          => $request->end_date,
            'search'            => '',
            'cause_campaign'    => $cause_campaign,
            'cause_name'        => $cause_name,
            'cause_description' => $cause_description,
            'cause_contact'     => $cause_contact,
            'cause_account'     => $request->cause_account
        ];

        //dd($data);

        $create = Campaign::create($data);

        if ($create){
            $v = Campaign::find($create->id);
            $v->search = "CV".str_pad($create->id, 6, "0" ,STR_PAD_LEFT);
            $v->save();

            return redirect(route('edit_campaign', $create->id))->with('success', trans('app.campaign_created'));
        }
        return back()->with('error', trans('app.something_went_wrong'))->withInput($request->input());
    }


    public function myCampaigns(){
        $user = Auth::user();
        $title = trans('app.my_campaigns');
        $user = request()->user();

        $my_campaigns = Campaign::whereUserId($user->id)->orderBy('id', 'desc')->get();

        $campaigns = $user->my_campaigns()->get();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();
        
        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        return view('admin.my_campaigns', compact('title', 'donation', 'disputes', 'campaigns','user','userCampaignCount','my_campaigns','payments'));
    }


    public function myPendingCampaigns(){
        $title = trans('app.pending_campaigns');
        $user = request()->user();
        $my_campaigns = Campaign::pending()->whereUserId($user->id)->orderBy('id', 'desc')->get();

        return view('admin.my_campaigns', compact('title', 'my_campaigns'));
    }


    
    public function allCampaigns(){
        $user = Auth::user();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $title = trans('app.all_campaigns');
        $campaigns = Campaign::active()->orderBy('id', 'desc')->paginate(20);

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();

        return view('admin.all_campaigns', compact('title', 'campaigns','user','payments', 'disputes', 'donation', 'userCampaignCount'));
    }


    public function staffPicksCampaigns(){
        $user = Auth::user();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $title = trans('app.staff_picks');
        $campaigns = Campaign::staff_picks()->orderBy('id', 'desc')->paginate(20);
        return view('admin.all_campaigns', compact('title', 'campaigns','user','payments'));
    }


    public function fundedCampaigns(){
        $user = Auth::user();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $title = trans('app.funded');
        $campaigns = Campaign::funded()->orderBy('id', 'desc')->paginate(20);

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();

        return view('admin.all_campaigns', compact('title', 'campaigns','user','payments', 'disputes', 'donation', 'userCampaignCount'));
    }


    public function blockedCampaigns(){
        $user = Auth::user();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $title = trans('app.blocked_campaigns');
        $campaigns = Campaign::blocked()->orderBy('id', 'desc')->paginate(20);

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();

        return view('admin.all_campaigns', compact('title', 'campaigns','user','payments', 'disputes', 'donation', 'userCampaignCount'));
    }


    public function pendingCampaigns(){
        $user = Auth::user();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $title = trans('app.pending_campaigns');
        $campaigns = Campaign::pending()->orderBy('id', 'desc')->paginate(20);

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();

        return view('admin.all_campaigns', compact('title', 'campaigns','user','payments', 'disputes', 'donation', 'userCampaignCount'));
    }


    public function expiredCampaigns(){
        $user = Auth::user();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $title = trans('app.expired_campaigns');
        $campaigns = Campaign::active()->expired()->orderBy('id', 'desc')->paginate(20);

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();

        return view('admin.all_campaigns', compact('title', 'campaigns','user','payments', 'disputes', 'donation', 'userCampaignCount'));
    }


    public function searchAdminCampaigns(Request $request){
        $user = Auth::user();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $title = trans('app.campaigns_search_results');
        $campaigns = Campaign::where('title', 'like', "%{$request->q}%")->orderBy('id', 'desc')->paginate(20);
        return view('admin.all_campaigns', compact('title', 'campaigns','user','payments'));
    }
    

    public function deleteCampaigns($id = 0){
        if(config('app.is_demo')){
            return redirect()->back()->with('error', 'This feature has been disable for demo');
        }

        if ($id){
            $campaign = Campaign::find($id);
            if ($campaign){
                $campaign->delete();
            }
        }
        return back()->with('success', trans('app.campaign_deleted'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $slug = null)
    {
        $user = Auth::user();
        $campaign = Campaign::find($id);
        $userId = Campaign::find($id)->user_id;
        $campaignCreator = User::find($userId);
        $campaignCount = Campaign::where('user_id','=',$userId)->count();

        $title = $campaign->title;
        $faqs = Faq::whereCampaignId($id)->get();
        $enable_discuss = get_option('enable_disqus_comment');
        $annuity_minimum_amount = get_option('annuity');

        $updates = Update::where('user_id', $campaignCreator->id)->get();
        $currentMonthYear = Carbon::now()->format('Y-m');

        $voteCount = Votes::where('campaign_id', $id)->where('type', 'normal')->get()->count();
        $countXcvVote = Votes::where('campaign_id', $id)->where('type', 'xcv')->where('created_at', 'like', "{$currentMonthYear}%" )->get()->count();

        if($user){
            $userHasVoted = Votes::where('campaign_id', $id)->where('type', 'normal')->where('user_id', $user->id)->get()->toArray();
            $totalXcvToken = User::where('id', $user->id)->get()[0]->xcv;
            $numberOfUserXcvVotes = Votes::where('user_id', $user->id)->where('type', 'xcv')->where('created_at', 'like', "{$currentMonthYear}%" )->get()->count();
            $numberOfUserXcvVotesByCampaignId = Votes::where('user_id', $user->id)->where('type', 'xcv')->where('campaign_id',$id)->where('created_at', 'like', "{$currentMonthYear}%" )->get()->count();
            $numberOfVotesLeft = $totalXcvToken - $numberOfUserXcvVotes;
            //$numberOfVotesLeft = 0;
        }else{
            $userHasVoted = false;
            $totalXcvToken = false;
            $numberOfUserXcvVotes = false;
            $numberOfVotesLeft = false;
            $numberOfUserXcvVotesByCampaignId = false;
        }

        if($userHasVoted){
            $userHasVoted = true;
        }
        else{
            $userHasVoted = false;
        }

        //dd($numberOfUserXcvVotes);

        return view('campaign_single', 
            compact('campaign',
                'userHasVoted', 
                'countXcvVote',
                'numberOfVotesLeft',
                'numberOfUserXcvVotesByCampaignId',
                'vote',
                'voteCount',
                'user',
                'updates',
                'annuity_minimum_amount',
                'title',
                'enable_discuss',
                'campaignCreator',
                'campaignCount',
                'faqs'
            ));
    }

    public function convenant($id, $slug = null)
    {
        $user = Auth::user();
        $campaign = Campaign::find($id);
        $userId = Campaign::find($id)->user_id;
        $campaignCreator = User::find($userId);
        $campaignCount = Campaign::where('user_id','=',$userId)->count();

        $title = $campaign->title;
        $faqs = Faq::whereCampaignId($id)->get();
        $enable_discuss = get_option('enable_disqus_comment');
        $annuity_minimum_amount = get_option('annuity');

        $updates = Update::where('user_id', $campaignCreator->id)->get();
        $currentMonthYear = Carbon::now()->format('Y-m');

        $voteCount = Votes::where('campaign_id', $id)->where('type', 'normal')->get()->count();
        $countXcvVote = Votes::where('campaign_id', $id)->where('type', 'xcv')->where('created_at', 'like', "{$currentMonthYear}%" )->get()->count();

        if($user){
            $userHasVoted = Votes::where('campaign_id', $id)->where('type', 'normal')->where('user_id', $user->id)->get()->toArray();
            $totalXcvToken = User::where('id', $user->id)->get()[0]->xcv;
            $numberOfUserXcvVotes = Votes::where('user_id', $user->id)->where('type', 'xcv')->where('created_at', 'like', "{$currentMonthYear}%" )->get()->count();
            $numberOfUserXcvVotesByCampaignId = Votes::where('user_id', $user->id)->where('type', 'xcv')->where('campaign_id',$id)->where('created_at', 'like', "{$currentMonthYear}%" )->get()->count();
            $numberOfVotesLeft = $totalXcvToken - $numberOfUserXcvVotes;
            //$numberOfVotesLeft = 0;
        }else{
            $userHasVoted = false;
            $totalXcvToken = false;
            $numberOfUserXcvVotes = false;
            $numberOfVotesLeft = false;
            $numberOfUserXcvVotesByCampaignId = false;
        }

        if($userHasVoted){
            $userHasVoted = true;
        }
        else{
            $userHasVoted = false;
        }

        //dd($numberOfUserXcvVotes);

        return view('campaign_single_convenant', 
            compact('campaign',
                'userHasVoted', 
                'countXcvVote',
                'numberOfVotesLeft',
                'numberOfUserXcvVotesByCampaignId',
                'vote',
                'voteCount',
                'user',
                'updates',
                'annuity_minimum_amount',
                'title',
                'enable_discuss',
                'campaignCreator',
                'campaignCount',
                'faqs'
            ));
    }


    public function comment(Request $request) {

        $data = [
            'user_id'  =>  $request->user_id,
            'update_id'  =>  $request->update_id,
            'comment'  =>  $request->comment
        ];

        //$create = Comment::create($data);

        return redirect(route('campaign_single', $request->campaign_id));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = request()->user()->id;
        $user = Auth::user();
        $campaign = Campaign::find($id);
        $userCampaignCount = Campaign::where('user_id','=',Auth::id())->count();
        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();
        //todo: checked if admin then he can access...
        if ($campaign->user_id != $user_id){
            exit('Unauthorized access');
        }

        $title = trans('app.edit_campaign');
        $categories = Category::all();
        $countries = Country::all();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        return view('admin.edit_campaign', compact('title', 'categories', 'userCampaignCount', 'user', 'donation', 'countries', 'campaign', 'disputes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        $rules = [
            'category'      => 'required',
            'title'         => 'required',
            'short_description' => 'required|max:200',
            'description'   => 'required',
            'goal'          => 'required',
            'country_id'    => 'required',
        ];

        $this->validate($request, $rules);

        $campaign = Campaign::find($id);

        $image_name = $campaign->feature_image;
        if ($request->hasFile('feature_image')){
            $upload_dir = './uploads/images/';
            if ( ! file_exists($upload_dir)){
                mkdir($upload_dir, 0777, true);
            }
            $thumb_dir = './uploads/images/thumb/';
            if ( ! file_exists($thumb_dir)){
                mkdir($thumb_dir, 0777, true);
            }

            //Delete old image
            if ($image_name){
                if (file_exists($upload_dir.$image_name)){
                    unlink($upload_dir.$image_name);
                }
                if (file_exists($thumb_dir.$image_name)){
                    unlink($thumb_dir.$image_name);
                }
            }

            $image = $request->file('feature_image');
            $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
            $full_image = Image::make($image)->resize(1500, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $resized = Image::make($image)->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $image_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();

            $thumbFileName = $thumb_dir.$image_name;
            $imageFileName = $upload_dir.$image_name;

            try{
                //Uploading original image
                $full_image->save($imageFileName);
                //Uploading thumb
                $resized->save($thumbFileName);
            } catch (\Exception $e){
                return $e->getMessage();
            }
        }

        $data = [
            'category_id'       => $request->category,
            'title'             => $request->title,
            'short_description' => $request->short_description,
            'description'       => $request->description,
            'goal'              => $request->goal,
            'min_amount'        => $request->min_amount,
            'max_amount'        => $request->max_amount,
            'recommended_amount' => $request->recommended_amount,
            'amount_prefilled'  => $request->amount_prefilled,
            'end_method'        => $request->end_method,
            'video'             => $request->video,
            'feature_image'     => $image_name,
            'country_id'        => $request->country_id,
            'address'           => $request->address,
            'start_date'        => $request->start_date,
            'end_date'          => $request->end_date,
        ];

        $update = Campaign::whereId($id)->update($data);

        if ($update){
            return redirect(route('edit_campaign', $id))->with('success', trans('app.campaign_created'));
        }
        return back()->with('error', trans('app.something_went_wrong'))->withInput($request->input());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showBackers($id){
        $campaign = Campaign::find($id);
        $title = trans('app.backers').' | '.$campaign->title;
        return view('campaign_backers', compact('campaign', 'title'));

    }

    public function showUpdates($id){
        $campaign = Campaign::find($id);
        $title = $campaign->title;
        return view('campaign_update', compact('campaign', 'title'));
    }

    public function showFaqs($id){
        $campaign = Campaign::find($id);
        $title = $campaign->title;
        return view('campaign_faqs', compact('campaign', 'title'));
    }

    /**
     * @param $id
     * @return mixed
     * 
     * todo: need to be moved it to reward controller
     */
    public function rewardsInCampaignEdit($id){
        $title = trans('app.campaign_rewards');
        $campaign = Campaign::find($id);
        $rewards = Reward::whereCampaignId($campaign->id)->get();
        return view('admin.campaign_rewards', compact('title', 'campaign', 'rewards'));
    }

    /**
     * @param Request $request
     * @param int $reward_id
     * @return mixed
     */
    public function addToCart(Request $request, $reward_id = 0){
        if ($reward_id){
            //If checkout request come from reward
            session( ['cart' =>  ['cart_type' => 'reward', 'reward_id' => $reward_id, 'annuity' => 0] ] );

            $reward = Reward::find($reward_id);
            if($reward->campaign->is_ended()){
                $request->session()->forget('cart');
                return redirect()->back()->with('error', trans('app.invalid_request'));
            }
        }else{
            //Or if comes from donate button
            session( ['cart' =>  ['cart_type' => 'donation', 'campaign_id' => $request->campaign_id, 'amount' => $request->amount, 'annuity' => $request->annuity ] ] );
        }


        return redirect(route('checkout'));
    }

    public function statusChange($id, $status = null){

        $campaign = Campaign::find($id);
        if ($campaign && $status){

            if ($status == 'approve'){
                $campaign->status = 1;
                $campaign->save();

            }elseif($status == 'block'){
                $campaign->status = 2;
                $campaign->save();
            }elseif($status == 'funded'){
                $campaign->is_funded = 1;
                $campaign->save();
            }elseif ($status == 'add_staff_picks'){
                $campaign->is_staff_picks = 1;
                $campaign->save();

            }elseif($status == 'remove_staff_picks'){
                $campaign->is_staff_picks = 0;
                $campaign->save();
            }

        }
        return back()->with('success', trans('app.status_updated'));
    }

    /**
     * @return mixed
     *
     * Checkout page
     */
    public function checkout(){
        $title = trans('app.checkout');

        $user = Auth::user();

        if ( ! session('cart')){
            return view('checkout_empty', compact('title'));
        }

        $reward = null;
        if(session('cart.cart_type') == 'reward'){
            $reward = Reward::find(session('cart.reward_id'));
            $campaign = Campaign::find($reward->campaign_id);
        }elseif (session('cart.cart_type') == 'donation'){
            $campaign = Campaign::find(session('cart.campaign_id'));
        }
        if (session('cart')){
            return view('checkout', compact('title', 'campaign', 'reward', 'user'));
        }
        return view('checkout_empty', compact('title'));
    }

    public function checkoutPost(Request $request){
        $title = trans('app.checkout');

        if ( ! session('cart')){
            return view('checkout_empty', compact('title'));
        }

        $cart = session('cart');
        $input = array_except($request->input(), '_token');
        session(['cart' => array_merge($cart, $input)]);

        if(session('cart.cart_type') == 'reward'){
            $reward = Reward::find(session('cart.reward_id'));
            $campaign = Campaign::find($reward->campaign_id);
        }elseif (session('cart.cart_type') == 'donation'){
            $campaign = Campaign::find(session('cart.campaign_id'));
        }
        
        //dd(session('cart'));
        return view('payment', compact('title', 'campaign'));
    }

    /**
     * @param Request $request
     * @return mixed
     *
     * Payment gateway PayPal
     */
    public function paypalRedirect(Request $request){
        if ( ! session('cart')){
            return view('checkout_empty', compact('title'));
        }
        //Find the campaign
        $cart = session('cart');

        $amount = 0;
        if(session('cart.cart_type') == 'reward'){
            $reward = Reward::find(session('cart.reward_id'));
            $amount = $reward->amount;
            $campaign = Campaign::find($reward->campaign_id);
        }elseif (session('cart.cart_type') == 'donation'){
            $campaign = Campaign::find(session('cart.campaign_id'));
            $amount = $cart['amount'];
        }
        $currency = get_option('currency_sign');
        $user_id = null;
        if (Auth::check()){
            $user_id = Auth::user()->id;
        }
        //Create payment in database


        $transaction_id = 'tran_'.time().str_random(6);
        // get unique recharge transaction id
        while( ( Payment::whereLocalTransactionId($transaction_id)->count() ) > 0) {
            $transaction_id = 'reid'.time().str_random(5);
        }
        $transaction_id = strtoupper($transaction_id);

        $payments_data = [
            'name' => session('cart.full_name'),
            'email' => session('cart.email'),

            'user_id'               => $user_id,
            'campaign_id'           => $campaign->id,
            'reward_id'             => session('cart.reward_id'),

            'amount'                => $amount,
            'payment_method'        => 'paypal',
            'status'                => 'initial',
            'currency'              => $currency,
            'local_transaction_id'  => $transaction_id,

            'contributor_name_display'  => session('cart.contributor_name_display'),
        ];
        //Create payment and clear it from session
        $created_payment = Payment::create($payments_data);
        $request->session()->forget('cart');

        // PayPal settings
        $paypal_action_url = "https://www.paypal.com/cgi-bin/webscr";
        if (get_option('enable_paypal_sandbox') == 1)
            $paypal_action_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";

        $paypal_email = get_option('paypal_receiver_email');
        $return_url = route('payment_success',$transaction_id);
        $cancel_url = route('checkout');
        $notify_url = route('paypal_notify', $transaction_id);

        $item_name = $campaign->title." [Contributing]";

        // Check if paypal request or response
        $querystring = '';

        // Firstly Append paypal account to querystring
        $querystring .= "?business=".urlencode($paypal_email)."&";

        // Append amount& currency (£) to quersytring so it cannot be edited in html
        //The item name and amount can be brought in dynamically by querying the $_POST['item_number'] variable.
        $querystring .= "item_name=".urlencode($item_name)."&";
        $querystring .= "amount=".urlencode($amount)."&";
        $querystring .= "currency_code=".urlencode($currency)."&";

        $querystring .= "first_name=".urlencode(session('cart.full_name'))."&";
        //$querystring .= "last_name=".urlencode($ad->user->last_name)."&";
        $querystring .= "payer_email=".urlencode(session('cart.email') )."&";
        $querystring .= "item_number=".urlencode($created_payment->local_transaction_id)."&";

        //loop for posted values and append to querystring
        foreach(array_except($request->input(), '_token') as $key => $value){
            $value = urlencode(stripslashes($value));
            $querystring .= "$key=$value&";
        }

        // Append paypal return addresses
        $querystring .= "return=".urlencode(stripslashes($return_url))."&";
        $querystring .= "cancel_return=".urlencode(stripslashes($cancel_url))."&";
        $querystring .= "notify_url=".urlencode($notify_url);

        // Append querystring with custom field
        //$querystring .= "&custom=".USERID;

        // Redirect to paypal IPN
        header('location:'.$paypal_action_url.$querystring);
        exit();
    }

    /**
     * @param Request $request
     * @param $transaction_id
     *
     * Check paypal notify
     */
    public function paypalNotify(Request $request, $transaction_id){
        //todo: need to  be check
        $payment = Payment::whereLocalTransactionId($transaction_id)->where('status','!=','success')->first();

        $verified = paypal_ipn_verify();
        if ($verified){
            //Payment success, we are ready approve your payment
            $payment->status = 'success';
            $payment->charge_id_or_token = $request->txn_id;
            $payment->description = $request->item_name;
            $payment->payer_email = $request->payer_email;
            $payment->payment_created = strtotime($request->payment_date);
            $payment->save();
        }else{
            $payment->status = 'declined';
            $payment->description = trans('app.payment_declined_msg');
            $payment->save();
        }
        // Reply with an empty 200 response to indicate to paypal the IPN was received correctly
        header("HTTP/1.1 200 OK");
    }


    /**
     * @return array
     * 
     * receive card payment from stripe
     */
    public function paymentStripeReceive(Request $request){

        $user_id = null;
        if (Auth::check()){
            $user_id = Auth::user()->id;
        }

        $stripeToken = $request->stripeToken;
        \Stripe\Stripe::setApiKey(get_stripe_key('secret'));
        // Create the charge on Stripe's servers - this will charge the user's card
        try {
            $cart = session('cart');

            //Find the campaign
            $amount = 0;
            if(session('cart.cart_type') == 'reward'){
                $reward = Reward::find(session('cart.reward_id'));
                $amount = $reward->amount;
                $campaign = Campaign::find($reward->campaign_id);
            }elseif (session('cart.cart_type') == 'donation'){
                $campaign = Campaign::find(session('cart.campaign_id'));
                //$amount = $cart['amount'];
                $amount = $request->amount;
            }
            $currency = get_option('currency_sign');

            //Charge from card
            $charge = \Stripe\Charge::create(array(
                "amount"        => ($amount * 100), // amount in cents, again
                "currency"      => $currency,
                "source"        => $stripeToken,
                "description"   => $campaign->title." [Contributing]"
            ));

            if ($charge->status == 'succeeded'){
                //Save payment into database

                $donation = $charge->amount / 100;
                $topUp = null;
                $annuity_status = 0;

                $vote = Votes::where('campaign_id', $campaign->id)->where('type', 'normal')->get()->count();
                $xcv = Votes::where('campaign_id', $campaign->id)->where('type', 'xcv')->get()->count();
                
                if($campaign->end_method != 'perpetuity') {
                    if( $campaign->topup_eligibility == 1 ||  $vote > 500 || $xcv > 0 ){
                        if ($donation >= get_option('criteria')) {
                            if(!empty(get_option('amount'))){
                                $topUp = get_option('amount');

                                $newPoolValue = get_option('pool') - get_option('amount');
                                update_option('pool', $newPoolValue );
                            }
                            else{
                                $percentVal = get_option('percent');
                                $topUp =  ($percentVal/100) * $donation;

                                $newPoolValue = get_option('pool') - $topUp;
                                update_option('pool', $newPoolValue );
                            }
                        }
                    }
                }

                if (session('cart')['annuity'] == 1) {
                    $annuity_status = 1;
                }

                $data = [
                    'name' => $request->name,
                    'convenant' => $request->convenant,
                    'email' => session('cart.email'),
                    'amount' => ($charge->amount / 100 ),
                    'top_up' => $topUp,
                    'annuity_status' => $annuity_status,

                    'user_id'               => $user_id,
                    'campaign_id'           => $campaign->id,
                    'reward_id'             => session('cart.reward_id'),
                    'payment_method'        => 'stripe',
                    'currency'              => $currency,
                    'charge_id_or_token'    => $charge->id,
                    'description'           => $charge->description,
                    'payment_created'       => $charge->created,

                    //Card Info
                    'card_last4'        => $charge->source->last4,
                    'card_id'           => $charge->source->id,
                    'card_brand'        => $charge->source->brand,
                    'card_country'      => $charge->source->US,
                    'card_exp_month'    => $charge->source->exp_month,
                    'card_exp_year'     => $charge->source->exp_year,

                    'contributor_name_display'  => session('cart.contributor_name_display'),
                    'status'                    => 'success',
                ];

                //save convenant donation into db

                Payment::create($data);

                // if ($request->convenant != '') {

                //     $convenant_data =[
                //     'user_id' => $user_id,
                //     'campaign_id' => $campaign->id,
                //     'payment_id' => $payment_id->id,
                //     'convenant' => $request->convenant,
                // ];

                //    Convenant::create($convenant_data);
                // }

                $request->session()->forget('cart');
                return ['success'=>1, 'msg'=> trans('app.payment_received_msg'), 'response' => $this->payment_success_html()];
                
            }
        } catch(\Stripe\Error\Card $e) {
            // The card has been declined
            return ['success'=>0, 'msg'=> trans('app.payment_declined_msg'), 'response' => $e];
        }
        return view('payment_success', compact('title'));
    }
    
    public function payment_success_html(){
        //session()->forget('cart');
        $html =     '<div class="col offset-md-6">
                        <div style="align-items: center; margin-top: 100px; margin-bottom:200px" class="payment-received">
                            <h1> <i class="fa fa-check-circle-o"></i> '.trans('app.payment_thank_you').'</h1>
                            <p>'.trans('app.payment_receive_successfully').'</p>
                            <a style="text-align: center" href="'.route('home').'" class="btn btn-filled">'.trans('app.home').'</a>
                        </div>
                    </div>';
        return $html;
    }
    
    public function paymentSuccess(Request $request, $transaction_id = null){
        if ($transaction_id){
            $payment = Payment::whereLocalTransactionId($transaction_id)->whereStatus('initial')->first();
            if ($payment){
                $payment->status = 'pending';
                $payment->save();
            }
        }

        $title = trans('app.payment_success');
        return view('payment_success', compact('title'));
    }

    /**
     * @date April 29, 2017
     * @since v.1.1
     */
    public function paymentBankTransferReceive(Request $request){
        $rules = [
            'bank_swift_code'   => 'required',
            'account_number'    => 'required',
            'branch_name'       => 'required',
            'branch_address'    => 'required',
            'account_name'      => 'required',
        ];
        $this->validate($request, $rules);

        //get Cart Item
        if ( ! session('cart')){
            return view('checkout_empty', compact('title'));
        }
        //Find the campaign
        $cart = session('cart');

        $amount = 0;
        if(session('cart.cart_type') == 'reward'){
            $reward = Reward::find(session('cart.reward_id'));
            $amount = $reward->amount;
            $campaign = Campaign::find($reward->campaign_id);
        }elseif (session('cart.cart_type') == 'donation'){
            $campaign = Campaign::find(session('cart.campaign_id'));
            $amount = $cart['amount'];
        }
        $currency = get_option('currency_sign');
        $user_id = null;
        if (Auth::check()){
            $user_id = Auth::user()->id;
        }
        //Create payment in database


        $transaction_id = 'tran_'.time().str_random(6);
        // get unique recharge transaction id
        while( ( Payment::whereLocalTransactionId($transaction_id)->count() ) > 0) {
            $transaction_id = 'reid'.time().str_random(5);
        }
        $transaction_id = strtoupper($transaction_id);

        $payments_data = [
            'name' => session('cart.full_name'),
            'email' => session('cart.email'),

            'user_id'               => $user_id,
            'campaign_id'           => $campaign->id,
            'reward_id'             => session('cart.reward_id'),

            'amount'                => $amount,
            'payment_method'        => 'bank_transfer',
            'status'                => 'pending',
            'currency'              => $currency,
            'local_transaction_id'  => $transaction_id,

            'contributor_name_display'  => session('cart.contributor_name_display'),

            'bank_swift_code'   => $request->bank_swift_code,
            'account_number'    => $request->account_number,
            'branch_name'       => $request->branch_name,
            'branch_address'    => $request->branch_address,
            'account_name'      => $request->account_name,
            'iban'              => $request->iban,
        ];
        //Create payment and clear it from session
        $created_payment = Payment::create($payments_data);
        $request->session()->forget('cart');

        return ['success'=>1, 'msg'=> trans('app.payment_received_msg'), 'response' => $this->payment_success_html()];

    }

    public function campaignEligibility($id){
        $campaign = Campaign::find($id);

        if( $campaign->topup_eligibility == 1 ){
            $campaign->topup_eligibility = 0;
        }
        else{
            $campaign->topup_eligibility = 1;
        }
        
        $campaign->save();
        return redirect()->back()->with('success', 'Changes Successfully Made');
    }

    
}
