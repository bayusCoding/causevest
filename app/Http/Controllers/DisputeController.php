<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Campaign;
use App\Payment;
use App\Dispute;


class DisputeController extends Controller
{
    /**
     *
     */
    public function disputes()
    {
        $user = Auth::user();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();
        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();


        if($user->is_admin()){
            $disputes = $d;
        }
        
        return view('admin.disputes', compact('user', 'userCampaignCount', 'payments', 'disputes', 'donation'));
    }

    public function replyDispute($id)
    {
        $user = Auth::user();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();


        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();
        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();


        if($user->is_admin()){
            $disputes = $d;
        }

        $dispute = Dispute::where('id', $id)->get()[0];
        $replies = Dispute::where('parent_id', $id)->get();

        $parent_id = $id;


        return view('admin.reply_dispute', compact('user', 'userCampaignCount', 'payments', 'dispute', 'disputes', 'donation', 'replies', 'parent_id'));
    }


    public function createDispute(Request $request) {

        $data = [
            'subject'   => $request->subject,
            'body'      => $request->body,
            'user_id'   => $request->user_id,
            'parent_id' => null
        ];

        $create = Dispute::create($data);

        if ($create){

            return redirect(route('disputes'))->with('success', trans('Message sent'));
        }
        return back()->with('error', trans('app.something_went_wrong'))->withInput($request->input());

    }

    public function replyDisputePost(Request $request)
    {
        $data = [
            'subject'   => null,
            'body'      => $request->body,
            'user_id'   => $request->user_id,
            'parent_id' => $request->parent_id
        ];

        $create = Dispute::create($data);

        if ($create){

            return redirect(route('reply_dispute', $request->parent_id))->with('success', trans('Message sent'));
        }
        return back()->with('error', trans('app.something_went_wrong'))->withInput($request->input());
    }
    
}
