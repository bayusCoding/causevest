<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Category;
use App\Dispute;
use App\Payment;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Auth;


class CategoriesController extends Controller
{

    public function index(){

        $user = Auth::user();
        $title = trans('app.categories');
        $categories = Category::paginate(10);

        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        return view('admin.categories', compact('title', 'categories', 'user', 'userCampaignCount', 'disputes', 'donation'));
    }


    public function store(Request $request)
    {
        $rules = [
            'category_name' => 'required',
            'image'         => 'image'
        ];
        $this->validate($request, $rules);

        $slug = str_slug($request->category_name);
        $duplicate = Category::where('category_slug', $slug)->count();
        if ($duplicate > 0){
            return back()->with('error', trans('app.category_exists_in_db'));
        }

        /**
         * Upload image if any
         */
        $image_name = '';
        if ($request->hasFile('image')){
            $upload_dir = './uploads/images/';
            if ( ! file_exists($upload_dir)){
                mkdir($upload_dir, 0777, true);
            }

            $image = $request->file('image');
            $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
            $resized = Image::make($image)->resize(300, 200);

            $image_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();
            $imageFileName = $upload_dir.$image_name;

            try{
                $resized->save($imageFileName);
            } catch (\Exception $e){
                return $e->getMessage();
            }
        }

        $data = [
            'category_name' => $request->category_name,
            'category_slug' => $slug,
            'image' => $image_name,
        ];

        Category::create($data);
        return back()->with('success', trans('app.category_created'));
    }

    public function edit($id){
        $user = Auth::user();

        $category = Category::find($id);

        if (! $category){
            return trans('app.invalid_request');
        }

        $title = trans('app.edit_category');

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();

        return view('admin.categories_edit', compact('title', 'category', 'donation', 'disputes', 'user', 'userCampaignCount'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return string
     */
    public function update(Request $request, $id){
        $category = Category::find($id);
        if (! $category){
            return trans('app.invalid_request');
        }

        $rules = [
            'category_name' => 'required',
            'image'         => 'image'
        ];
        $this->validate($request, $rules);

        /**
         * Upload image if any
         */
        $image_name = $category->image;
        if ($request->hasFile('image')){
            $upload_dir = './uploads/images/';
            if ( ! file_exists($upload_dir)){
                mkdir($upload_dir, 0777, true);
            }

            $image = $request->file('image');
            $file_base_name = str_replace('.'.$image->getClientOriginalExtension(), '', $image->getClientOriginalName());
            $resized = Image::make($image)->resize(300, 200);

            $image_name = strtolower(time().str_random(5).'-'.str_slug($file_base_name)).'.' . $image->getClientOriginalExtension();
            $imageFileName = $upload_dir.$image_name;

            try{
                $resized->save($imageFileName);
            } catch (\Exception $e){
                return $e->getMessage();
            }
        }

        $data = [
            'category_name' => $request->category_name,
            'image' => $image_name,
        ];

        $category->update($data);
        return back()->with('success', trans('app.category_created'));
    }

    public function destroy(Request $request){
        $category_id = $request->data_id;
        Category::find($category_id)->delete();
        return ['success' => 1];
    }
    
    public function browseCategories(){
        $title = trans('app.crowdfunding_categories');
        $categories = Category::orderBy('id', 'asc')->take(8)->get();

        $funded_campaigns = Campaign::active()->funded()->orderBy('id', 'asc')->take(3)->get();
        $popular = Campaign::active()->funded()->orderBy('id', 'desc')->take(3)->get();
        return view('categories', compact('title', 'categories', 'funded_campaigns', 'popular'));
    }

    public function singleCategory($id, $slug = null, Request $request){
        $title = trans('app.crowdfunding_categories');
        $category = Category::find($id);
        $categories = Category::orderBy('id', 'asc')->get();
        $campaignsByCategoryId = Campaign::active()->where('category_id', '=', $id)->paginate(3);

        $user = Auth::user();

        if($request->ajax()){
            return[
                'campaignsByCategoryId' => view('ajax.campaigns_by_category')->with(compact('campaignsByCategoryId'))->render(),
                'next_page' => $campaignsByCategoryId->nextPageUrl()
            ];
        }

        return view('campaigns_by_category', compact('title', 'category', 'categories', 'campaignsByCategoryId', 'id', 'user'));
    }



    public function getAllCampaigns(Request $request){
        $title = trans('app.crowdfunding_categories');
        $categories = Category::orderBy('id', 'asc')->get();
        $allCampaigns = Campaign::orderBy('id', 'asc')->paginate(9);
        
        $user = Auth::user();

        
        //$allCampaigns = Campaign::paginate(3);

        if($request->ajax()){
            return[
                'allCampaigns' => view('ajax.campaigns_all')->with(compact('allCampaigns', 'user'))->render(),
                'next_page' => $allCampaigns->nextPageUrl()
            ];
        }
        
        return view('campaigns_all', compact('title', 'categories', 'allCampaigns', 'user', 'like'));
    }



    public function search(Request $request){
        if ($request->q){
            $q = $request->q;
            $title = trans('app.search_campaigns');
            $campaigns = Campaign::active()->where('title', 'like', "%{$q}%")->orWhere('short_description', 'like', "%{$request->q}%")->orWhere('description', 'like', "%{$q}%")->paginate(3);
            return view('search', compact('title', 'campaigns', 'q'));
        }else{
            return $this->browseCategories();
        }
    }

    public function serachForCampaigns(Request $request){
        $title = trans('app.crowdfunding_categories');

        $allCampaigns = Campaign::where('title', 'like', "%{$request->q}%")->orWhere('short_description', 'like', "%{$request->q}%")->orWhere('search', 'like', "%{$request->q}%")->orWhere('description', 'like', "%{$request->q}%")->orderBy('id', 'desc')->paginate(10);
        $categories = Category::orderBy('id', 'asc')->get();
        $search = 1;

        if($request->ajax()){
            return[
                'allCampaigns' => view('ajax.campaigns_all')->with(compact('allCampaigns'))->render(),
                'next_page' => $allCampaigns->nextPageUrl()
            ];
        }

        return view('campaigns_all', compact('title', 'categories', 'allCampaigns', 'search'));
    }
    
    public function serachForCampaignsWithID(Request $request) {
        $title = trans('app.crowdfunding_categories');
        $id = $request->id;
        $search = 1;

        $category = Category::find($id);
        $categories = Category::orderBy('id', 'asc')->get();
        $campaignsByCategoryId = Campaign::where('title', 'like', "%{$request->q}%")->where('category_id', '=', $id)->orWhere('short_description', 'like', "%{$request->q}%")->orWhere('search', 'like', "%{$request->q}%")->orWhere('description', 'like', "%{$request->q}%")->orderBy('id', 'desc')->paginate(10);
    
        if($request->ajax()){
            return[
                'campaignsByCategoryId' => view('ajax.campaigns_by_category')->with(compact('campaignsByCategoryId'))->render(),
                'next_page' => $campaignsByCategoryId->nextPageUrl()
            ];
        }

        return view('campaigns_by_category', compact('title', 'category', 'categories', 'campaignsByCategoryId', 'id', 'search'));
    }
}
