<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Convenant;
use App\Payment;
use App\Dispute;
use App\Withdrawal_request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    
    public function index(Request $request){
        $user = Auth::user();
        $title = trans('app.payments');

        if ($user->is_admin()){
            if ($request->q){
                $payments = Payment::success()->where('email', 'like', "%{$request->q}%")->orderBy('id', 'desc')->paginate(20);
            }else{
                $payments = Payment::success()->orderBy('id', 'desc')->paginate(20);
            }
        }else{
            $campaign_ids = $user->my_campaigns()->pluck('id')->toArray();
            if ($request->q){
                $payments = Payment::success()->whereIn('campaign_id', $campaign_ids)->where('email', 'like', "%{$request->q}%")->orderBy('id', 'desc')->paginate(20);
            }else{
                $payments = Payment::success()->whereIn('campaign_id', $campaign_ids)->orderBy('id', 'desc')->paginate(20);
            }
        }
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        return view('admin.payments', compact('title', 'payments', 'userCampaignCount', 'user', 'donation', 'disputes'));
    }
    
    public function view($id){
        $user = Auth::user();

        $title = trans('app.payment_details');
        $payment = Payment::find($id);

        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();

        $annuityCampaigns = Campaign::where('end_method', 'perpetuity')->get();

        //dd($annuityCampaign);

        return view('admin.payment_view', compact('title', 'payment', 'user', 'payments', 'userCampaignCount', 'annuityCampaigns'));
    }

    public function withdraw(){
        // $raised = $this->success_paymentss()->sum('amount');
        $user = Auth::user();
        $title = trans('app.withdraw');
        $campaigns = $user->my_campaigns;
        $withdrawal_requests = Withdrawal_request::whereUserId($user->id)->orderBy('id', 'desc')->get();
        
        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();
        
        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $annuity = Payment::where('user_id', $user->id)->where('annuity_status', 1)->sum('amount');
        $convenant = Payment::where('user_id', $user->id)->where('responsestatus', '=', 1)->sum('amount');
        
        return view('admin.withdraw', compact('title', 'campaigns', 'disputes', 'donation','withdrawal_requests', 'convenant', 'annuity', 'payments', 'userCampaignCount', 'user'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function withdrawRequest(Request $request){
        $user_id = Auth::user()->id;
        $campaign_id = $request->withdrawal_campaign_id;

        $requested_withdrawal = Withdrawal_request::whereCampaignId($campaign_id)->first();
        if ($requested_withdrawal){
            return redirect()->back()->with('error', trans('app.this_withdraw_is_processing'));
        }

        $campaign = Campaign::find($campaign_id);
        $withdraw_amount = $campaign->amount_raised()->campaign_owner_commission;
        $total_amount = $campaign->amount_raised()->amount_raised;
        $platform_owner_commission = $campaign->amount_raised()->platform_owner_commission;
        $withdrawal_preference = withdrawal_preference();

        $data = [
            'user_id'                       => $user_id,
            'campaign_id'                   => $campaign_id,
            'total_amount'                  => $total_amount,
            'platform_owner_commission'     => $platform_owner_commission,
            'withdrawal_amount'             => $withdraw_amount,
            'withdrawal_account'            => $withdrawal_preference,
            'status'                        => 'pending',
        ];

        if ($withdrawal_preference == 'paypal'){
            $data['paypal_email']               = withdrawal_preference('paypal_email');
        }elseif ($withdrawal_preference == 'bank'){

            $data['bank_account_holders_name']  = withdrawal_preference('bank_account_holders_name');
            $data['bank_account_number']        = withdrawal_preference('bank_account_number');
            $data['swift_code']                 = withdrawal_preference('swift_code');
            $data['bank_name_full']             = withdrawal_preference('bank_name_full');
            $data['bank_branch_name']           = withdrawal_preference('bank_branch_name');
            $data['bank_branch_city']           = withdrawal_preference('bank_branch_city');
            $data['bank_branch_address']        = withdrawal_preference('bank_branch_address');
            $data['country_id']                 = withdrawal_preference('country_id');
        }

        Withdrawal_request::create($data);

        return redirect()->back()->with('success', trans('app.withdraw_request_sent'));
    }

    public function withdrawRequestView($id){
        $title = trans('app.withdrawal_details');
        $withdraw_request = Withdrawal_request::find($id);
        $user = Auth::user();

        if (! $user->is_admin() ){
            if ($user->id != $withdraw_request->user_id){
                die('Unauthorize request');
            }
        }

        return view('admin.withdrawal_details', compact('title', 'withdraw_request'));
    }

    public function withdrawalRequests(){
        $title = trans('app.withdrawal_requests');
        $withdraw_requests = Withdrawal_request::orderBy('id', 'desc')->paginate(20);

        return view('admin.withdraw_requests', compact('title', 'withdraw_requests'));
    }

    public function withdrawalRequestsStatusSwitch(Request  $request, $id = 0){
        $user = Auth::user();
        if (! $user->is_admin()){
            return redirect()->back()->with('error', trans('app.unauthorised_access'));
        }

        $withdraw_request = Withdrawal_request::find($id);
        $withdraw_request->status = $request->type;
        $withdraw_request->save();
        return redirect()->back()->with('success', trans('app.withdrawal_request_status_changed'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * @date April 29, 2017
     * @since v.1.1
     */

    public function paymentsPending(Request $request){
        $user = Auth::user();
        $title = trans('app.payments');

        if ($user->is_admin()){
            if ($request->q){
                $payments = Payment::pending()->where('email', 'like', "%{$request->q}%")->orderBy('id', 'desc')->paginate(20);
            }else{
                $payments = Payment::pending()->orderBy('id', 'desc')->paginate(20);
            }
        }else{
            $campaign_ids = $user->my_campaigns()->pluck('id')->toArray();
            if ($request->q){
                $payments = Payment::pending()->whereIn('campaign_id', $campaign_ids)->where('email', 'like', "%{$request->q}%")->orderBy('id', 'desc')->paginate(20);
            }else{
                $payments = Payment::pending()->whereIn('campaign_id', $campaign_ids)->orderBy('id', 'desc')->paginate(20);
            }
        }
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();
        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        return view('admin.payments', compact('title', 'payments', 'userCampaignCount', 'user', 'donation', 'disputes'));
    }

    public function viewAllDonations(Request $request){
        $user = Auth::user();
        $title = trans('app.payments');

        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->paginate(15);
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();

        $campaigns = Campaign::get();

        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        return view('admin.view-all-donations', compact('title', 'payments', 'userCampaignCount', 'user', 'donation','disputes', 'campaigns'));
    }

    public function CovenantResp(Request $request)
    {

        $campaign_id = Payment::findOrFail($request->response_campaign_id);
        
        $campaign_id->causeresponse = $request->response;
        $campaign_id->save();

        return back();

    }

    public function CovenantReply(Request $request)
    {

        $campaign_id = Payment::findOrFail($request->response_campaign_id);
        
        $campaign_id->responsestatus = 1;
        $campaign_id->save();

        return back();

    }


      public function viewAllDonationsCovenant(Request $request){
        $user = Auth::user();
        $title = trans('app.convenant');

        $payments = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->paginate(15);
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();

        $campaigns = Campaign::get();

        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $convenant = Convenant::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        return view('admin.view-all-donations_convenant', compact('title', 'payments', 'userCampaignCount', 'user', 'donation', 'convenant', 'disputes', 'campaigns'));
    }

    public function viewAnnuityDonations(Request $request){
        $user = Auth::user();
        $title = trans('app.payments');

        $payments = Payment::where('annuity_status','=', 1)->orderBy('id', 'desc')->paginate(15);
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        return view('admin.view-all-annuity-donations', compact('title', 'payments', 'userCampaignCount', 'user', 'disputes', 'donation'));
    }

      public function viewConvenantDonations(Request $request){
        $user = Auth::user();
        $title = trans('app.payments');

        $payments = Payment::where('convenant','!=', NULL)->orderBy('id', 'desc')->paginate(15);
        $userCampaignCount = Campaign::where('user_id','=',$user->id)->count();

        $disputes = Dispute::where('user_id', $user->id)->where('subject', "!=", null)->get();

        $d = Dispute::where('parent_id',  null)->get();

        if($user->is_admin()){
            $disputes = $d;
        }

        $donation = Payment::where('user_id','=',$user->id)->orderBy('id', 'desc')->get();

        return view('admin.view-all-convenant-donations', compact('title', 'payments', 'userCampaignCount', 'user', 'disputes', 'donation'));
    }

    public function markSuccess($id, $status){
        $payment = Payment::find($id);
        $payment->status = $status;
        $payment->save();

        return back()->with('success', trans('app.payment_status_changed'));
    }

    public function updateAnnuityInfo(Request $request){
        $title = trans('app.payment_details');

        $payment = Payment::find($request->id);
        $payment->annuity_info = $request->annuity;
        $payment->save();

        return back()->with('success', 'Annuity Information Saved');
    }


    public function reassignDonationPost(Request $request){
        $payment = Payment::find($request->payment_id);
        $payment->campaign_id = $request->campaign_id;
        $payment->save();

        return back()->with('success', 'Annuity Donation Reassigned Successfully');
    }


}
