<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Campaign;
use App\Votes;

class Xcv_votes extends Model
{
    protected $guarded = [];

    public function getCampaign($id){
        return Campaign::where('id', $id)->get()[0];
    }

    public function getNormal($id){
        return Votes::selectRaw('COUNT(*) AS count')
            ->where('campaign_id', $id)
            ->groupBy('id')
            ->get();
    }
}
