<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Votes;
use App\Campaign;

class Votes extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $dates = ['deleted_at'];
    protected $table = 'votes';

    public function getXcv($id){
        return Votes::where('campaign_id', $id)->where('type', 'xcv')->get()->count();
    }

    public function getCampaignTitle($id) {
        return Campaign::where('id', $id)->get()[0]->title;
    }

    public function getNormal($id){
        return Votes::where('campaign_id', $id)->where('type', 'normal')->get()->count();
    }
}
