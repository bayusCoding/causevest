<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class UpdateUsersXCV extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateUsersXCV:updatexcv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Users XCV Token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::get();

        $guzzle = new Client;

        $response = $guzzle->post('https://causevest.io/api/login', [
            'form_params' => [
                'email' => 'ogunbayo.abayomi@yahoo.com',
                'password' => '12345677',
                
            ],
        ]);

        foreach($users as $user){

            $res = '';

            if( json_decode((string) $response->getBody(), true)['success']['token'] ){
                try{
                    $response = $guzzle->post('https://causevest.io/api/clientTokenData/'.$user->email, [
                        'headers' => [
                            'Accept' => 'application/json',
                            'Authorization' => 'Bearer '.json_decode((string) $response->getBody(), true)['success']['token'],
                        ],
                    ]);
                    $res = json_decode((string) $response->getBody(), true)['xcv'];
                }
                catch(\Exception $e){
                    $res = 'Error';
                }
            }

            if($res != 'Error'){
                $u = User::find($user->id);
                $u->xcv = $res;
                $u->save();
            }
            else{
                $u = User::find($user->id);
                $u->xcv = null;
                $u->save();
            }

        }
    }
}
