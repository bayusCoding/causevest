<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Votes;
use App\Verify;

class LeaderboardVotes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leaderboard:votes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Number of votes for both XCV and normal users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $verify = Verify::selectRaw('verify')->first();

        if ($verify == 0) {
            
        }

        $votes = Votes::all();
        foreach($votes as $vote)
        {
            $vote->delete();

        }
    }
}
