<?php

namespace App;

use App\Comment;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Update extends Model
{
    protected $guarded = [];

    public function getComment($update_id) {
        return Comment::where('update_id', $update_id)->get();
    }

    public function getUserByUpdate($userId) {
        return User::find($userId);
    }
}
