<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConvenantToPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->string('convenant')->after('amount')->nullable();
            $table->string('causeresponse')->after('convenant')->nullable();
            $table->boolean('responsestatus')->after('causeresponse')->default(0)->comment('1 - accept, 0 - reject');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('convenant');
            $table->dropColumn('causeresponse');
            $table->dropColumn('responsestatus');
        });
    }
}
