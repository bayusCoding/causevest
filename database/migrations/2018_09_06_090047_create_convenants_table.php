<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConvenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convenants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('campaign_id');
            $table->string('convenant');
            $table->string('response')->nullable();
            $table->boolean('status')->default(0)->comment('1 - accept, 0 - reject');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convenants');
    }
}
