window.onscroll = function () {

  var header = document.querySelector("#header-scroll");

  if (window.pageYOffset >= 1) {
    header.classList.add("is-sticky");
  } else {
    header.classList.remove("is-sticky");
  }
};

$(document).ready(function () {
      $('.donate-form input[type=radio]').click(function () {
          if (this.checked == true) {
            //$(this).closest('.form-group').addClass('checked');
          }
          if (this.checked == false) {
            if ($(this).closest('.form-group').hasClass('checked')) {
              $(this).removeClass('checked');
            }
          }
          });

          $(".donate-form .currency-val").bind('keyup mouseup', function () {
            var val = $(this).val();
            $.fn.digits = function(){ 
              return this.each(function(){ 
                  $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
              });
          };
            $(".donate-form span.value").html(val).digits();
            
          
        });

        $('.select-currency').on('change', function () {
          var val = $(this).find('option:selected').text();
          $("span.currency").html(val);
      });

        $('.hamburger-menu').on('click', function() {
          console.log('i see ya')
          $('.bar').toggleClass('animate');
          var mobileNav = $('.mobile-nav');
          mobileNav.toggleClass('hide show');
          $('body').toggleClass('overflow_hidden');
        });

        $("div#featured-switch-tabs ul li").click(
          function () {
              $('#featured-switch-tabs ul li.active').not(this).removeClass('active');
              $(this).addClass('active');
          });
      
      });